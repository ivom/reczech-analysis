# ReČeK analysis

## FSD documentation

Public access to current version of the FSD documentation:

https://ivom.gitlab.io/reczech-analysis/reczech-fsd.html

## Prerequisites

1. Install Node.js & `npm`.
2. Install npm dependencies:

       npm i

## Build FSD locally

Build the output:

    npm run build

The FSD is built in `output/reczech-fsd.html`.

## Write FSD

Serve the output:

    npm run serve

Open the displayed URL in your browser.

As you edit Markdown source files in `src/`
the output FSD in the browser gets updated automatically.

Refer to https://casefu.com/ to learn how to write FSD source files.
