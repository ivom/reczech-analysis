## Screen: Auditní log `/audit-logs`

### Form:

- Entita
- Akce (select: create, delete, update, patch)
- Id
- Uživatel (select: Vodička Josef, Vomáčka Ferdinand, Vopička František)
- Čas (time)

### Table:

- Entita
    - User
    - User
    - User
- Akce
    - patch
    - update
    - create
- Id
    - 123
    - 123
    - 123
- Uživatel
    - Vopička František
    - Vopička František
    - Vopička František
- Čas
    - 2022-02-22 20:20:20
    - 2022-02-22 20:20:02
    - 2022-02-22 20:02:20
- Data
    - `{"id":123,lastName":"Vodička","firstName":"Josef","archived":true,...}`
    - `{"id":123,lastName":"Vodička","firstName":"Josef","archived":false,...}`
    - `{"id":123,lastName":"Vodička","firstName":"Pepa","archived":false,...}`
