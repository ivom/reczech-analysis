## UC: `AL10` Prohlížet auditní log

- Primární aktér: `#Root`
- Obrazovka: [Auditní log](#/audit-logs)

### Main success scenario:

1. Uživatel zadá kritéria hledání:
    - Čas: hledá záznamy s [časem vytvoření](#AuditLog.createTime) menším
2. Systém najde a zobrazí odpovídající záznamy [auditního logu](#AuditLog) řazené
   podle [času vytvoření](#AuditLog.createTime) sestupně.
