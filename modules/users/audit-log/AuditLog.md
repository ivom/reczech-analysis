## Entity: Auditní log `AuditLog`

### Attributes:

- Čas vytvoření `createTime` (M timestamp): 2022-02-20 20:02:20
- Jméno entity `entityName` (M text): Publisher
- Akce na entitě `entityAction` (M text): update
- Id entity `entityId` (O text): 1234
- Uživatel `user` (n:1 `#User`)
- Data (O json): `{"name":"Albatros",...}`
