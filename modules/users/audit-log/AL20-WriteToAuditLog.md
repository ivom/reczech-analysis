## UC: `AL20` Zapsat do auditního logu

- Primární aktér: [Systém](#System)
- Úroveň: Podfunkce
- Spouštěč: Změna dat kterékoliv entity v systému

### Main success scenario:

1. Systém zapíše nový záznam [auditního logu](#AuditLog) s:
    - [Čas vytvoření](#AuditLog.createTime) = aktuální čas
    - [Jméno entity](#AuditLog.entityName) = jméno entity, jejíž data se změnila, např. `User`
    - [Akce na entitě](#AuditLog.entityAction), např. `update`
    - [Id entity](#AuditLog.entityId)
    - [Uživatel](#AuditLog.user) = přihlášený uživatel, který změnu provedl
    - [Data](#AuditLog.data) = nová data entity po změně
