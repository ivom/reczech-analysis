## Entity: Role `Role`

### Attributes:

- Kód `code` (PK text): `PublisherAdmin`
- Působnost `authority` (M enum: user, organization, root)
- Povolené role `authorizedRoles` (m:n `#Role`): `PublisherAdmin`, `PublisherGuarantor`

### Poznámky

- Působnost:
    - Uživatel `user`: lze pracovat pouze s vlastním uživatelem, role jsou pouze ke čtení.
    - Organizace `organization`: lze pracovat pouze s uživateli vlastní organizace, lze nastavit pouze povolené role.
    - Root `root`: lze pracovat s uživateli všech organizací, lze nastavit všechny role.
