## UC: `SE30` Odhlásit se

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)

### Main success scenario:

1. Uživatel se odhlásí.
2. Systém zahodí [token uživatelské relace](#UserSession.token).
