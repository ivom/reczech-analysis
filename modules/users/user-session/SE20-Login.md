## UC: `SE20` Přihlásit se

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Přihlášení](#/login)

### Main success scenario:

1. Uživatel zadá email a heslo a přihlásí se.
2. Systém najde [uživatele](#User) podle emailu.
3. Systém zobrazí [aktivní organizace](#Organization.archived), na které má
   uživatel [aktivní vazbu](#UserOrganization.archived), včetně jejich [zdroje](#Organization.source).
4. Uživatel vybere organizaci.
5. Systém vytvoří nový záznam [uživatelské relace](#UserSession) s:
    - [Token](#UserSession.token) = vygenerovaný
    - [Organizace uživatele](#UserSession.userOrganization) = vybraná organizace uživatele
    - [Čas vytvoření](#UserSession.createTime) = aktuální čas
    - [Čas vypršení](#UserSession.expireTime) = Čas vytvoření + 12 hodin

### Extensions:

- 2a. Uživatel nebyl nalezen, heslo [neodpovídá](#User.passwordHash) nebo uživatel nemá
  žádnou [aktivní vazbu](#UserOrganization.archived) na [aktivní organizaci](#Organization.archived):
    - 2a1. Systém smaže hodnotu hesla na obrazovce.
    - 2a2. Systém zobrazí chybu: "Přihlašovací údaje nejsou platné."
- 3a. Uživatel má právě jednu aktivní organizaci:
    - 3a1. Případ užití pokračuje krokem 5.
