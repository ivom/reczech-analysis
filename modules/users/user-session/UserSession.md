## Entity: Uživatelská relace `UserSession`

### Attributes:

- Token (PK uuid)
- Organizace uživatele `userOrganization` (n:1 `#UserOrganization`)
- Čas vytvoření `createTime` (M timestamp)
- Čas vypršení `expireTime` (M timestamp)
