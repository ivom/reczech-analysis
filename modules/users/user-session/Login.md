## Screen: Přihlášení `/login`

### Form:

- Email (R)
- Heslo (R password)
- [primary: Přihlásit se](#/select-organization)

Zapomněli jste své heslo? Zresetujte si ho [zde](#/request/password/reset).
