## UC: `SE25` Přihlásit se do externího systému

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Aktéři: [Externí systém](#ExternalSystem)
- Obrazovka: [Přihlášení](#/login)
- Výstupní datová struktura - Přihlášený uživatel:
    - Id `id`
    - Email `email`
    - Jméno `firstName`
    - Příjmení `lastName`
    - Vyžadována změna hesla `shouldChangePassword`
    - Role `roles` (string array) - kódy rolí
    - Organizace `organization`
        - Id `id`
        - Zdrojový identifikátor `sourceId` (nepovinné)
        - Jméno `name`

### Main success scenario:

1. Uživatel otevře externí systém.
2. Externí systém předá uživatele do modulu Uživatelé.
3. Uživatel [se přihlásí](#SE20) do modulu Uživatelé.
4. Modul Uživatelé předá uživatele zpět do původního externího systému společně s jeho údaji.
5. Externí systém najde záznam uživatele se zdrojovým identifikátorem rovným Id uživatele z modulu Uživatelé.
6. Externí systém si uloží aktuální přijaté údaje uživatele.
7. Uživatel se stává přihlášeným uživatelem v externím systému.
8. Uživatel používá externí systém.

### Extensions:

- 1a. Uživatel má platnou uživatelskou relaci v externím systému:
    - 1a1. Případ užití pokračuje krokem 8.
- 3a. Uživatel se nepřihlásil:
    - 3a1. Případ užití končí.
- 5a. Externí systém nemá záznam uživatele s daným zdrojovým identifikátorem:
    - 5a1. Externí systém si vytvoří nový záznam uživatele se zdrojovým identifikátorem.
