## UC: `SE21` Přihlásit se do jiné organizace

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Výběr organizace](#/select-organization)

### Main success scenario:

1. Systém zobrazí [aktivní organizace](#Organization.archived), na které má
   uživatel [aktivní vazbu](#UserOrganization.archived), včetně jejich [zdroje](#Organization.source).
2. Uživatel vybere organizaci.
3. Systém vytvoří nový záznam [uživatelské relace](#UserSession) s:
    - [Token](#UserSession.token) = vygenerovaný
    - [Organizace uživatele](#UserSession.userOrganization) = vybraná organizace uživatele
    - [Čas vytvoření](#UserSession.createTime) = aktuální čas
    - [Čas vypršení](#UserSession.expireTime) = Čas vytvoření + 12 hodin
