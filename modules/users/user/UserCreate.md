## Screen: Vytvořit uživatele `/users/create`

### Form:

- Organizace (R select: Albatros - Nakladatelé, Grada - Nakladatelé, Národní knihovna)
- Email (R)
- Jméno (R)
- Příjmení (R)
- Role (checkboxes: Správce nakladatele, Garant nakladatele, Pracovník agentury)
- Heslo (R password) - Heslo musí mít alespoň 8 znaků.
- Odeslat heslo emailem (checkbox)
- [primary: Uložit](#/users/detail)
- [:Zpět](#/users)
