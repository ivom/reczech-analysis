## Screen: Resetovat heslo uživatele `/users/detail/reset-password`

### Form: Resetovat heslo uživatele František Vopička

Tuto akci nelze vrátit.

<span class="h2">uAokH45V</span>
[:Kopírovat]()

- Odeslat heslo emailem (checkbox)
- [warning: Potvrdit](#/users/detail)
- [:Zpět](#/users/detail)
