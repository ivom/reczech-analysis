## Screen: Uživatelé `/users`

### Form:

- Hledat - Hledat podle emailu, příjmení nebo jména
- Organizace (select: Albatros - Nakladatelé, Grada - Nakladatelé, Národní knihovna)
- Archivováno (checkbox)

### Table:

- Příjmení
    - [Vodička](#/users/detail)
    - [Vomáčka](#/users/detail)
    - [Vondrušková](#/users/detail)
    - [Vopička](#/users/detail)
- Jméno
    - Josef
    - Ferdinand
    - Marie
    - František
- Email
    - vodicka @ albatros.cz
    - vomacka @ grada.cz
    - vondruskova @ nkp.cz
    - vopicka @ albatros.cz
- Organizace
    - Albatros
    - Grada
    - ~~Albatros~~, Grada, Národní knihovna
    - Albatros, ~~Grada~~

[:Vytvořit](#/users/create)
