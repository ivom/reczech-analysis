## UC: `US41` Resetovat heslo uživatele

- Primární aktér: `#Root`, [Správce](#Admin)
- Obrazovka: [Resetovat heslo uživatele](#/users/detail/reset-password)

### Main success scenario:

1. Uživatel vybere cílový záznam uživatele a resetuje jeho heslo.
2. Systém vygeneruje náhodné heslo o délce 8 znaků písmen bez diakritiky (velká i malá písmena) a číslic.
3. Systém zobrazí vygenerované heslo.
4. Uživatel potvrdí reset hesla na vygenerovanou hodnotu.
5. Systém nastaví na cílovém [uživateli](#User):
    - [Hash hesla](#User.passwordHash) = hash vygenerovaného hesla
    - [Vyžadována změna hesla](#User.shouldChangePassword) = `true`

### Extensions:

- 5a. Uživatel zvolil "Odeslat heslo emailem":
    - 5a1. Systém odešle emailovu zprávu na emailovou adresu cílového uživatele s informací o resetu hesla a s heslem v
      otevřené formě.
