## Screen: Změnit uživatele `/users/edit`

> Pro externího uživatele systém zobrazí následující informaci:

<p class="alert alert-warning">
Tento uživatel je externí, jeho údaje mohou být přepsány z jiného systému.
</p>

### Form: Uživatel

- Email (R): vopicka@albatros.cz
- Jméno (R): František
- Příjmení (R): Vopička

### Table: Organizace uživatele

- Organizace
    - ~~Albatros~~
    - Grada
    - Národní knihovna
- Role (R checkboxes: Správce nakladatele, Garant nakladatele, Pracovník agentury)
    - Správce nakladatele, Garant nakladatele
    -
    - Pracovník agentury
- Archivovat
    - [warning: Archivovat]()
    - [warning: Archivovat]()
    - [warning: Archivovat]()
- Obnovit
    - [warning: Obnovit]()
    - [warning: Obnovit]()
    - [warning: Obnovit]()

### Form: Přidat organizaci

- Organizace (R select: Albatros - Nakladatelé, Grada - Nakladatelé, Národní knihovna)
- [primary: Přidat]()

### Form:

- [primary: Uložit](#/users/detail)
- [:Zpět](#/users/detail)
