## Entity: Uživatel `User`

### Attributes:

- Id (PK bigserial)
- Email (U citext): vopicka@albatros.cz
- Jméno `firstName` (M citext): František
- Příjmení `lastName` (M citext): Vopička
- Hash hesla `passwordHash` (M text)
- Vyžadována změna hesla `shouldChangePassword` (M boolean)
- Token pro resetování hesla `passwordResetToken` (O uuid)
- Čas vypršení resetování hesla `passwordResetExpireTime` (O timestamp)

### Poznámky

- Hash hesla je ve formátu, který explicitně identifikuje použitý algoritmus a umožňuje tak podporovat více algoritmů
  současně, viz např.
  [Spring DelegatingPasswordEncoder](https://docs.spring.io/spring-security/site/docs/5.0.x/api/org/springframework/security/crypto/password/DelegatingPasswordEncoder.html)
- Systém požívá algoritmus, který neumožňuje ověření pomocí rainbow tables a podporuje dynamickou náročnost výpočtu.
  Požadovaný algoritmus je [bcrypt](https://en.wikipedia.org/wiki/Bcrypt) nebo lepší.
