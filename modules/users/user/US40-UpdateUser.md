## UC: `US40` Změnit uživatele

- Primární aktér: `#Root`, [Správce](#Admin)
- Obrazovka: [Změnit uživatele](#/users/edit)

### Main success scenario:

1. Uživatel vybere cílový záznam uživatele.
2. Systém najde všechny [role](#Role).
3. Systém zobrazí údaje cílového [uživatele](#User).
4. Systém najde a zobrazí všechny [organizace](#Organization), na které má cílový uživatel vazbu
   přes [organizaci uživatele](#UserOrganization).
5. Systém zvýrazní záznamy organizací, které jsou [archivované](#Organization.archived) nebo jejichž organizace
   uživatele je [archivovaná](#UserOrganization.archived).
6. Uživatel:
    - změní údaje cílového uživatele,
    - přidá cílového uživatele do jiné organizace, výběr podle [jména](#Organization.name)
      a [zdroje](#Organization.source),
    - přidá nebo odebere role cílového uživatele v rámci jeho organizace,
    - archivuje [aktivní](#UserOrganization.archived) záznam [organizace cílového uživatele](#UserOrganization),
    - obnoví [archivovaný](#UserOrganization.archived) záznam [organizace cílového uživatele](#UserOrganization).
7. Uživatel potvrdí změny.
8. Systém:
    - uloží nové hodnoty cílového [uživatele](#User)
      ([email](#User.email), [jméno](#User.firstName), [příjmení](#User.lastName)),
    - vytvoří nové záznamy [organizací cílového uživatele](#UserOrganization),
    - uloží nové hodnoty [organizací cílového uživatele](#UserOrganization)
      ([role](#UserOrganization.roles), [archivováno](#UserOrganization.archived)).

### Extensions:

- 2a. Přihlášený uživatel je Správce (tj. není Root):
    - 2a1. Systém najde [povolené role](#Role.authorizedRoles) všech rolí s [působností](#Role.authority)
      Organizace přihlášeného uživatele.
- 3a. Cílový uživatel je externí (tj. existuje pro něj záznam [zdroje uživatele](#UserSource)):
    - 3a1. Systém zobrazí informaci, že editovaný uživatel je externí.
- 3b. Cílový uživatel má alespoň dva [zdroje](#UserSource) nebo alespoň dvě [organizace](#UserOrganization):
    - 3b1. Systém neumožní změnit email, pole je pouze pro čtení.
- 4a. Přihlášený uživatel je Správce (tj. není Root):
    - 4a1. Systém zobrazí pouze organizaci přihlášeného uživatele.
- 6a. Přihlášený uživatel je Správce (tj. není Root):
    - 6a1. Uživatel nesmí přidat cílového uživatele do jiné organizace.
