## UC: `US20` Vytvořit uživatele

- Primární aktér: `#Root`, [Správce](#Admin)
- Obrazovka: [Vytvořit uživatele](#/users/create)

### Main success scenario:

1. Systém najde a zobrazí všechny [role](#Role).
2. Uživatel najde a vybere [organizaci](#Organization) podle [jména](#Organization.name)
   a [zdroje](#Organization.source).
3. Uživatel zadá údaje nového uživatele, vybere role a vytvoří nového uživatele.
4. Systém vytvoří nový záznam [uživatele](#User) s:
    - [Email](#User.email) = vyplněná hodnota
    - [Jméno](#User.firstName) = vyplněná hodnota
    - [Příjmení](#User.lastName) = vyplněná hodnota
    - [Hash hesla](#User.passwordHash) = hash vyplněného hesla
    - [Vyžadována změna hesla](#User.shouldChangePassword) = `true`
5. Systém vytvoří nový záznam [organizace uživatele](#UserOrganization) s:
    - [Uživatel](#UserOrganization.user) = nový uživatel
    - [Organizace](#UserOrganization.organization) = organizace přihlášeného uživatele
    - [Role](#UserOrganization.roles) = vyplněná hodnota

### Extensions:

- 1a. Přihlášený uživatel je Správce (tj. není Root):
    - 1a1. Systém najde a zobrazí [povolené role](#Role.authorizedRoles) všech rolí s [působností](#Role.authority)
      Organizace přihlášeného uživatele.
- 2a. Přihlášený uživatel je Správce (tj. není Root):
    - 2a1. Organizace je přdvyplněna podle přihlášeného uživatele a nelze ji změnit.
- 3a. Heslo má méně než 8 znaků:
    - 3a1. Systém zobrazí chybu.
- 4a. Uživatel se zadaným [emailem](#User.email) již existuje:
    - 4a1. Systém zobrazí informaci: "Uživatel s tímto emailem je již v systému evidován. Kontaktuje prosím agenturu
      emailem nebo telefonem."
    - 4a2. Případ užití končí. (Root agentury následně přidá požadovaného uživatele do organizace správce.)
- 5a. Uživatel zvolil "Odeslat heslo emailem":
    - 5a1. Systém odešle emailovu zprávu na zadanou emailovou adresu nového uživatele s informací o založení
      uživatelského přístupu do systému a přihlašovacími údaji, tj. email a heslo v otevřené formě.
