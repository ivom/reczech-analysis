## Screen: Uživatel `/users/detail`

### Form: Uživatel

- Email (RO): vopicka@albatros.cz
- Jméno (RO): František
- Příjmení (RO): Vopička
- Externí (RO checkbox): true

### Table: Organizace uživatele

- Organizace
    - ~~Albatros~~
    - Grada
    - Národní knihovna
- Role
    - ~~Správce nakladatele, Garant nakladatele~~
    -
    - Pracovník agentury

### Form:

- [: Změnit](#/users/edit)
- [:Resetovat heslo](#/users/detail/reset-password)
- [:Zpět](#/users)
