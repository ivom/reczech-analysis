## UC: `US10` Prohlížet uživatele

- Primární aktér: `#Root`, [Správce](#Admin)
- Obrazovka: [Uživatelé](#/users), [Detail uživatelé](#/users/detail)

### Main success scenario:

1. Uživatel zadá kritéria hledání:
    - "Hledat": podle emailu, příjmení, jména
    - Organizace: existuje záznam [organizace uživatele](#UserOrganization) pro vybranou organizaci a cílového
      uživatele. Systém zobrazí pro každou organizaci její [jméno](#Organization.name) a [zdroj](#Organization.source).
    - Archivováno: zahrne i záznamy, které mají zobrazeny pouze organizace, které jsou
      buď [archivované](#Organization.archived), nebo jejichž organizace uživatele
      je [archivovaná](#UserOrganization.archived).
2. Systém najde a zobrazí odpovídající záznamy [uživatele](#User) řazené podle:
    - [Příjmení](#User.lastName)
    - [Jméno](#User.firstName)
    - [Email](#User.email)
3. Systém pro každého uživatele najde a zobrazí všechny [organizace](#Organization), na které má
   uživatel vazbu přes [organizaci uživatele](#UserOrganization).
4. Systém zvýrazní ty organizace, které jsou [archivované](#Organization.archived), nebo jejichž organizace uživatele
   je [archivovaná](#UserOrganization.archived).
5. Uživatel vybere záznam.
6. Systém zobrazí detail vybraného [uživatele](#User).
7. Systém zobrazí uživatele jako externího, pokud pro něj existuje záznam [zdroje uživatele](#UserSource).
8. Systém najde a zobrazí všechny [organizace](#Organization), na které má vybraný uživatel vazbu
   přes [organizaci uživatele](#UserOrganization).
9. Systém zvýrazní záznamy organizací, které jsou [archivované](#Organization.archived), nebo jejichž organizace
   uživatele je [archivovaná](#UserOrganization.archived).

### Extensions:

- 1a. Přihlášený uživatel je Správce (tj. není Root):
    - 1a1. Systém předvyplní výběrové kritérium Organizace podle přihlášeného uživatele a neumožní hodnotu změnit.
- 3a. Přihlášený uživatel je Správce (tj. není Root):
    - 3a1. Systém zobrazí pouze organizaci přihlášeného uživatele.
- 8a. Přihlášený uživatel je Správce (tj. není Root):
    - 8a1. Systém zobrazí pouze organizaci přihlášeného uživatele.
