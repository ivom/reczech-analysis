## Entity: Organizace uživatele `UserOrganization`

### Attributes:

- Uživatel `user` (n:1 `#User`)
- Organizace `organization` (n:1 `#Organization`)
- Role `roles` (m:n `#Role`): `PublisherAdmin`, `PublisherGuarantor`
- Archivováno `archived` (M boolean)

### Unique constraints

- `user`, `organization`
