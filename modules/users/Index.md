# ReČeK FSD, modul Uživatelé

FSD systému ReČeK, včetně přehledu všech modulů, [zde](reczech-fsd.html).

## Obrazovky

- [Přihlášení](#/login)
- [Můj profil](#/my-profile)
- [Organizace](#/organizations)
- [Uživatelé](#/users)

## Aktéři

- [Registrovaný uživatel](#RegisteredUser)
    - [Správce](#Admin)
    - `#Root`
- [Externí systém](#ExternalSystem)
    - [Modul Nakladatelé](#ModulePublishers)
- [Systém](#System)

## Datový model

- `#Role`
- [Organizace](#Organization)
- [Uživatel](#User)
    - [Organizace uživatele](#UserOrganization)
    - [Zdroj uživatele](#UserSource)
- [Uživatelská relace](#UserSession)
- [Auditní log](#AuditLog)

## Klíčové požadavky

Specifikace modulu Uživatelé vychází z těchto klíčových požadavků:

1. Poskytovat jednotné přihlášení přes všechny moduly (SSO) jako službu pro všechny ostatní moduly.
2. Jednotně spravovat organizace, uživatele a jejich přístupová práva tak, aby ostatní moduly to nemusely řešit.
3. Umožnit zařazení uživatele do více organizací.

## Generické funkčnosti

V rámci modulu Uživatelé jsou popsány generické funkčnosti, které jsou totožně požadovány ve všech modulech.
Z důvodu zamezení duplicitní specifikace jsou popsány pouze jednou, zde.

Jde o:

- Jednotné přihlášení přes všechny moduly (SSO), viz [Přihlásit se do externího systému](#SE25)
- [Vedení auditního logu](#AL20) všech změn v systému a jeho [prohlížení](#AL10)

## Technické požadavky

Z funkční analýzy vyplynuly tyto technické požadavky na implementaci modulu:

- Ukládání hesel v systému: viz [entita Uživatel](#User).
- Všechna pole pro zadávání hesla musí umožnit zobrazit heslo i v odkryté podobě na žádost uživatele (tlačítko).

## Procesní / metodické požadavky

Z funkční analýzy vyplynuly tyto požadavky na procesní / metodické zajištění modulu:

- Součástí podmínek používání systému musí být závazek každého uživatele, že uživatelský účet je veden na konkrétní
  fyzickou osobu a že bude adekvátním způsobem chránit své přístupové údaje (email a heslo) proti jejich zneužití,
  zejména je nebude sdílet s nikým jiným.

## TODO

- Anonymizace
