## Screen: Žádost o reset hesla `/request/password/reset`

### Form:

- Email (R)
- [primary: Resetovat heslo](#/reset/password)

> Po stisknutí tl. Zresetovat heslo se zobrazí:

Pokračujte prosím podle instrukcí ve zprávě na Vašem registrovaném emailovém účtu.
