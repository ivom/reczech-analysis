## UC: `MU40` Změnit můj profil

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Můj profil](#/my-profile)

### Main success scenario:

1. Systém zobrazí údaje přihlášeného uživatele.
2. Uživatel změní údaje.
3. Systém uloží údaje na přihlášeném [uživateli](#User).
