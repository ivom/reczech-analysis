## UC: `MU41` Změnit si heslo

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Moje heslo](#/my-profile/password)

### Main success scenario:

1. Uživatel zadá nové heslo.
2. Systém na přihlášeném [uživateli](#User) nastaví:
    - [Hash hesla](#User.passwordHash) = hash hesla
    - [Vyžadována změna hesla](#User.shouldChangePassword) = `false`

### Extensions:

- 1a. Heslo má méně než 8 znaků:
    - 1a1. Systém zobrazí chybu.
