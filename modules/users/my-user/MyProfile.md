## Screen: Můj profil `/my-profile`

### Form: Můj uživatel

- Organizace (RO): Albatros
- Email (R): vopicka@albatros.cz
- Jméno (R): František
- Příjmení (R): Vopička
- Role (RO): Správce nakladatele, Garant nakladatele
- [primary: Uložit]()
- [:Přihlásit se do jiné organizace](#/select-organization)
- [:Změnit heslo](#/my-profile/password)
- [:Odhlásit se](#/login)
