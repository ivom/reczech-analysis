## UC: `MU45` Zresetovat si heslo

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Přihlášení](#/login)

### Main success scenario:

1. Uživatel zadá email a požádá o reset hesla.
2. Systém zobrazí instrukci pro pokračování podle emailové zprávy.
3. Systém najde [uživatele](#User) podle emailu.
4. Systém na [uživateli](#User) nastaví:
    - [Token pro resetování hesla](#User.passwordResetToken) = vygenerovaný
    - [Čas vypršení resetování hesla](#User.passwordResetExpireTime) = aktuální čas + 10 minut
5. Systém pošle emailovou zprárvu s odkazem pro reset hesla na [email uživatele](#User.email).
6. Uživatel otevře odkaz z přijatého emailu.
7. Uživatel zadá nové heslo.
8. Systém najde [uživatele](#User) podle [tokenu pro resetování hesla](#User.passwordResetToken).
9. Systém na [uživateli](#User) nastaví:
    - [Hash hesla](#User.passwordHash) = hash nového hesla
    - [Čas vypršení resetování hesla](#User.passwordResetExpireTime) = aktuální čas
    - [Vyžadována změna hesla](#User.shouldChangePassword) = `false`

### Extensions:

- 3a. Uživatel nebyl nalezen nebo uživatel nemá žádnou [aktivní vazbu](#UserOrganization.archived)
  na [aktivní organizaci](#Organization.archived):
    - 3a1. Případ užití končí.
- 7a. Heslo má méně než 8 znaků:
    - 7a1. Systém zobrazí chybu.
- 8a. Uživatel nebyl nalezen nebo uživatel nemá žádnou [aktivní vazbu](#UserOrganization.archived)
  na [aktivní organizaci](#Organization.archived):
    - 8a1. Případ užití končí.
- 8b. [Čas vypršení resetování hesla](#User.passwordResetExpireTime) uživatele již proběhl:
    - 8b1. Systém zobrazí chybu:
      "Váš požadavek na reset hesla již z bezpečnostních důvodů vypršel. Požádejte si prosím o reset hesla znovu."
