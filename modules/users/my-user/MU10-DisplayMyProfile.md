## UC: `MU10` Zobrazit můj profil

- Primární aktér: [Registrovaný uživatel](#RegisteredUser)
- Obrazovka: [Můj profil](#/my-profile)

### Main success scenario:

1. Systém zobrazí údaje přihlášeného uživatele.
