## Entity: Zdroj uživatele `UserSource`

### Attributes:

- Uživatel `user` (n:1 `#User`)
- Zdroj `source` (M enum: publishers)
- Zdrojový identifikátor `sourceId` (M text): 12345
