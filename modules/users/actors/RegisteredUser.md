## Actor: Registrovaný uživatel `RegisteredUser`

### Goals

- Uživatelská relace
    - [Přihlásit se](#SE20)
    - [Přihlásit se do jiné organizace](#SE21)
    - [Přihlásit se do externího systému](#SE25)
    - [Odhlásit se](#SE30)
- Můj profil
    - [Zobrazit můj profil](#MU10)
    - [Změnit můj profil](#MU40)
    - [Změnit si heslo](#MU41)
    - [Zresetovat si heslo](#MU45)
