## Actor: Správce `Admin`

- Rozšiřuje: [Registrovaný uživatel](#RegisteredUser)
- Správcem se míní přihlášený uživatel, který má v [přihlášené organizaci](#UserOrganization)
  alespooň jednu [roli](#Role) s [působností](#Role.authority) Organizace.

### Goals

- Uživatel
    - [Prohlížet uživatele](#US10)
    - [Vytvořit uživatele](#US20)
    - [Změnit uživatele](#US40)
    - [Resetovat heslo uživatele](#US41)
