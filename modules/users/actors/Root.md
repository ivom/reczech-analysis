## Actor: Root

- Rozšiřuje: [Registrovaný uživatel](#RegisteredUser)
- Rootem se míní přihlášený uživatel, který má v [přihlášené organizaci](#UserOrganization)
  alespooň jednu [roli](#Role) s [působností](#Role.authority) Root.

### Goals

- Organizace
    - [Prohlížet organizace](#OR10)
    - [Vytvořit organizaci](#OR20)
    - [Archivovat organizaci](#OR30)
    - [Obnovit organizaci](#OR31)
    - [Změnit organizaci](#OR40)
- Uživatel
    - [Prohlížet uživatele](#US10)
    - [Vytvořit uživatele](#US20)
    - [Změnit uživatele](#US40)
    - [Resetovat heslo uživatele](#US41)
- Auditní log
    - [Prohlížet auditní log](#AL10)
