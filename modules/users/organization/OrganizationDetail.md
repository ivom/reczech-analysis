## Screen: Organizace `/organizations/detail`

### Form:

- Jméno (RO): Albatros
- Zdroj (RO): Nakladatelé
- Archivováno (RO checkbox): false
- [: Změnit](#/organizations/edit)
- [warning: Archivovat]()
- [warning: Obnovit]()
- [:Uživatelé](#/users)
- [:Zpět](#/organizations)

> Tl. Změnit, Archivovat a Obnovit povolena pouze pro interní organizace (tj. [zdroj](#Organization.source) není
> vyplněn).
