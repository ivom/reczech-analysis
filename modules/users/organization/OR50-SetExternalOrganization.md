## UC: `OR50` Nastavit externí organizaci

- Primární aktér: [Modul Nakladatelé](#ModulePublishers)
- Vstupní datová struktura:
    - Zdroj `source` (enum: `publishers`)
    - Zdrojový identifikátor organizace `sourceId`
    - Jméno `name`
    - Archivováno `archived` (boolean)
    - Uživatelé `users`
        - Zdrojový identifikátor uživatele `sourceId`
        - Email `email`
        - Jméno `firstName`
        - Příjmení `lastName`
        - Archivováno `archived` (boolean)
        - Role `roles` (string array) - kódy rolí

### Main success scenario:

1. Systém najde [organizaci](#Organization) podle zdroje a zdrojového identifikátoru organizace.
2. Systém uloží nové hodnoty [organizace](#Organization):
    - [Jméno](#Organization.name)
    - [Archivováno](#Organization.archived)
3. Systém pro každý vstupní záznam uživatele najde [zdroj uživatele](#UserSource)
   podle [zdroje](#UserSource.source) a [zdrojového identifikátoru uživatele](#UserSource.sourceId) a
   vezme [uživatele](#UserSource.user) z nalezeného záznamu.
4. Systém uloží nové hodnoty [uživatele](#User):
    - [Jméno](#User.firstName)
    - [Příjmení](#User.lastName)
5. Systém najde záznam [organizace uživatele](#UserOrganization) pro danou organizaci a uživatele.
6. Systém uloží nové hodnoty [organizace uživatele](#UserOrganization):
    - [Archivováno](#UserOrganization.archived)

### Extensions:

- 1a. Organizace s danou kombinací zdroje a zdrojového identifikátoru neexistuje:
    - 1a1. Systém založí nový záznam [organizace](#Organization) s hodnotami [zdroje](#Organization.source)
      a [zdrojového identifikátoru](#Organization.sourceId).
- 3a. Zdroj uživatele s danou kombinací zdroje a zdrojového identifikátoru neexistuje:
    - 3a1. Systém najde [uživatele](#User) podle vstupního emailu.
        - 3a1a. [Uživatel](#User) s daným emailem neexistuje:
            - 3a1a1. Systém založí nový záznam [uživatele](#User).
            - 3a1a2. Systém vygeneruje náhodné heslo.
            - 3a1a3. Systém odešle zprávu na email uživatele o založení nového uživatelského účtu včetně hesla.
            - 3a1a4. Systém zapíše do uživatele:
                - [Hash hesla](#User.passwordHash) = hash vstupního hesla
                - [Vyžadována změna hesla](#User.shouldChangePassword) = `true`
    - 3a2. Systém založí nový záznam [zdroje uživatele](#UserSource) a nastaví na něm:
      [uživatel](#UserSource.user), [zdroj](#UserSource.source) a [zdrojový identifikátor](#UserSource.sourceId).
- 4a. Uživatel má nejvýše jeden [zdroj](#UserSource) a zároveň nejvýše jednu [organizaci](#UserOrganization):
    - 4a1. Systém uloží také [email](#User.email) uživatele.
- 5a. Organizace uživatele pro danou kombinaci organizace a uživatele neexistuje:
    - 5a1. Systém založí nový záznam [organizace uživatele](#UserOrganization) s
      hodnotami [organizace](#UserOrganization.organization), [uživatele](#UserOrganization.user),
      [role](#UserOrganization.roles) a [archivováno](#UserOrganization.archived).
