## UC: `OR40` Změnit organizaci

- Primární aktér: `#Root`
- Obrazovka: [Změnit organizaci](#/organizations/edit)

### Main success scenario:

1. Uživatel vybere organizaci.
2. Uživatel změní údaje organizace.
3. Systém uloží nové hodnoty [organizace](#Organization).

### Extensions:

- 1a. Organizace je externí (tj. má vyplněn [zdroj](#Organization.source)):
    - 1a1. Případ užití končí.
- 3a. Organizace se zadaným [jménem](#Organization.name) a prázným [zdrojem](#Organization.source) již existuje:
    - 3a1. Systém zobrazí chybu "Tato organizace již existuje."
    - 3a2. Případ užití končí.
