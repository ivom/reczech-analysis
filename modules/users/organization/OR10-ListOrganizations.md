## UC: `OR10` Prohlížet organizace

- Primární aktér: `#Root`
- Obrazovka: [Organizace](#/organizations)

### Main success scenario:

1. Uživatel zadá kritéria hledání.
2. Systém najde a zobrazí odpovídající záznamy [organizace](#Organization) řazené podle:
    - [Jméno](#Organization.name)
    - [Zdroj](#Organization.source)
3. Uživatel vybere záznam.
4. Systém zobrazí detail záznamu.
