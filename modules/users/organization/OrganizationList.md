## Screen: Organizace `/organizations`

### Form:

- Jméno
- Archivováno (checkbox)

### Table:

- Jméno
    - [Albatros](#/organizations/detail)
    - [Grada](#/organizations/detail)
    - [Národní knihovna](#/organizations/detail)
    - [Pstroužek](#/organizations/detail)
- Zdroj
    - Nakladatelé
    - Nakladatelé
    -
    - Nakladatelé
- Archivováno (checkbox)
    - false
    - false
    - false
    - true

[:Vytvořit](#/organizations/create)
