## Entity: Organizace `Organization`

### Attributes:

- Id (PK bigserial)
- Jméno `name` (M text): Grada
- Zdroj `source` (O enum: publishers)
- Zdrojový identifikátor `sourceId` (O text): 1234
- Archivováno `archived` (M boolean)

### Unique constraints

- `name`, `source`
