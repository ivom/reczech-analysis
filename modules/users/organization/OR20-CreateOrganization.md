## UC: `OR20` Vytvořit organizaci

- Primární aktér: `#Root`
- Obrazovka: [Vytvořit organizaci](#/organizations/create)

### Main success scenario:

1. Uživatel zadá údaje organizace a vytvoří organizaci.
2. Systém vytvoří nový záznam [organizace](#Organization) s:
    - [Archivováno](#Organization.archived) = `false`

### Extensions:

- 2a. Organizace se zadaným [jménem](#Organization.name) a prázným [zdrojem](#Organization.source) již existuje:
    - 2a1. Systém zobrazí chybu "Tato organizace již existuje."
    - 2a2. Případ užití končí.
