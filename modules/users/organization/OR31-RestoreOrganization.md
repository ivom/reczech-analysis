## UC: `OR31` Obnovit organizaci

- Primární aktér: `#Root`
- Obrazovka: [Organizace](#/organizations/detail)

### Main success scenario:

1. Uživatel najde organizaci.
2. Uživatel obnoví organizaci.
3. Systém nastaví na [organizaci](#Organization)
    - [Archivováno](#Organization.archived) = `false`

### Extensions:

- 1a. Organizace je externí (tj. má vyplněn [zdroj](#Organization.source)):
    - 1a1. Případ užití končí.
