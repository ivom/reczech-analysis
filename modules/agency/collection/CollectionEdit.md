## Screen: Změnit řadu `/collections/edit`

### Národní obrození

### Form:

- Typ (R select: Edice, Série): Edice
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA): Albatros Media
- ISSN: 1802-7091
- Hlavní název (R): Národní obrození

### Table: Názvy v jiném jazyce

- Název (R)
    - National Revival
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/collections/detail)
- [:Zpět](#/collections/detail)
