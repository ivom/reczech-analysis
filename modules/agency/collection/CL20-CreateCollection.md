## UC: `CL20` Vytvořit řadu

- Primární aktér: [Správce řad](#CollectionsAdministrator), [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Vytvořit řadu](#/collections/create)

### Main success scenario:

1. Uživatel zadá údaje řady a vytvoří ji.
2. Systém provede formální validace dat.
3. Systém zapíše data z formuláře do nových záznamů:
    - [řady](#Collection),
    - jejích [názvů v jiném jazyce](#CollectionName).

### Extensions:

- 1a. Uživatel má pouze roli Garant nakladatele:
    - 1a1. Systém znepřístupní pole Nakladatel a vyplní ho nakladatelem přihlášeného uživatele.
- 1b. Uživatel zadá ISSN a načte podle něj údaje řady:
    - 1b1. Systém načte údaje řady podle ISSN a vyplní pole Hlavní název.
- 2a. Není vyplněno povinné pole:
    - 2a1. Systém zobrazí chybu.
- 2b. Typ je Edice a Nakladatel není vyplněn:
    - 2b1. Systém zobrazí chybu "Nakladatel je pro edice povinný."

### TODO

- Získat API, které umožní načíst název série podle ISSN.
