## UC: `CL25` Přijmout řadu

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Řada
- Předpoklady:
    - [Nakladatel](#Publisher), který zprávu odeslal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Položky zprávy Řada se shodují s položkami [entity Řada](#Collection) a podřízených entit (viz krok 3.) s těmito
  výjimkami:
    - Tyto položky zpráva neobsahuje:
        - Nakladatel
    - Tato položka je ve zprávě povinná:
        - Zdrojový identifikátor

### Main success scenario:

1. Systém přijme zprávu o řadě.
2. Systém najde [řadu](#Collection) podle [nakladatele](#Collection.publisher) (= nakladatel, který zprávu odeslal)
   a [zdrojového identifikátoru](#Collection.sourceId).
3. Systém zapíše údaje ze zprávy do:
    - [řady](#Collection),
    - jejích [názvů v jiném jazyce](#CollectionName).

### Extensions:

- 2a. Řada s daným nakladatelem a zdrojovým identifikátorem neexistuje:
    - 2a1. Systém založí novou [řadu](#Collection) s:
        - [Zdrojový identifikátor](#Collection.sourceId) = příchozí
        - [Nakladatel](#Collection.publisher) = [Nakladatel](#Publisher), který zprávu odeslal
