## Screen: Řady `/collections`

### Form:

- Typ (select: Edice, Série)
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- ISSN
- Název - Hledat podle hlavního názvu a všech názvů v jiném jazyce.

### Table:

- Typ
    - Edice
    - Série
    - Série
- Nakladatel
    - Albatros Media
    -
    - Grada Publishing
- ISSN
    - 1802-7091
    - 2049-3630
- Hlavní název
    - [Národní obrození](#/collections/detail)
    - [Harry Hole](#/collections/detail)
    - [Harry Potter](#/collections/detail)

[:Vytvořit](#/collections/create)
