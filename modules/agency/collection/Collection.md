## Entity: Řada `Collection`

### Attributes:

- Zdrojový identifikátor `sourceId` (O text): 1234 - Identifikátor v systému nakladatele
- Typ `type` (M enum: publisherSeries, series)
- Nakladatel `publisher` (n:0..1 `#Publisher`)
- ISSN (O text)
- Hlavní název `mainName` (M text)
- Názvy v jiném jazyce `otherLanguageNames` (1:n `#CollectionName`)

### Typ

V závorce kód Onix číselníku https://onix-codelists.io/codelist/148.

- `publisherSeries`: Edice (10)
- `series`: Série (20?, 10?)
