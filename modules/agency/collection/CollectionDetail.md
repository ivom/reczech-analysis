## Screen: Řada `/collections/detail`

### Národní obrození

### Form:

- Typ (RO): Edice
- Nakladatel (RO): [Albatros Media](#/publishers/detail)
- ISSN (RO): [1802-7091](https://portal.issn.org/resource/ISSN/1802-7091)
- Hlavní název (RO): Národní obrození

### Table: Názvy v jiném jazyce

- Název
    - National Revival

### Form:

- [:Změnit](#/collections/edit)
- [:Zpět](#/collections)
