## UC: `CL40` Změnit řadu

- Primární aktér: [Správce řad](#CollectionsAdministrator), [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Změnit řadu](#/collections/edit)

### Main success scenario:

1. Uživatel najde a vybere řadu k editaci.
2. Systém zobrazí údaje [řady](#Collection).
3. Uživatel změní údaje řady.
4. Systém provede formální validace dat.
5. Systém zapíše data z formuláře do:
    - [řady](#Collection),
    - jejích [názvů v jiném jazyce](#CollectionName).

### Extensions:

- 2a. Uživatel má pouze roli Garant nakladatele a vybraná řada [nepatří](#Collection.publisher) nakladateli uživatele:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 3a. Uživatel má pouze roli Garant nakladatele:
    - 3a1. Systém znepřístupní pole Nakladatel.
- 4a. Není vyplněno povinné pole:
    - 4a1. Systém zobrazí chybu.
- 4b. Typ je Edice a Nakladatel není vyplněn:
    - 4b1. Systém zobrazí chybu "Nakladatel je pro edice povinný."
