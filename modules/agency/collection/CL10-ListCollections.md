## UC: `CL10` Prohlížet řady

- Primární aktér: [Správce řad](#CollectionsAdministrator), [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Řady](#/collections)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [řad](#Collection), které odpovídají zadaným kritériím, a seřadí je podle:
    - [Hlavní název](#Collection.mainName)
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybrané [řady](#Collection).
5. Systém najde a zobrazí pro danou řadu:
    - všechny její [názvy v jiném jazyce](#Collection.otherLanguageNames).

### Extensions:

- 1a. Uživatel má roli Garant nakladatele:
    - 1a1. Systém předvyplní výběrové kritérium na nakladatele přihlášeného uživatele.
