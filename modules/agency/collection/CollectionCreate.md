## Screen: Vytvořit řadu `/collections/create`

### Form:

- Typ (R select: Edice, Série)
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- ISSN
- [:Načíst]()
- Hlavní název (R)

### Table: Názvy v jiném jazyce

- Název (R text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/collections/detail)
- [:Zpět](#/collections)
