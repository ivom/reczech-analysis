# ReČeK FSD, modul Agentura

FSD systému ReČeK, včetně přehledu všech modulů, [zde](reczech-fsd.html).

## Obrazovky

- Správce číselníků Onix
    - [Číselníky Onix](#/onix-codetables)
    - [Položky číselníku Onix](#/onix-codetable-items)
- Pracovník agentury
    - [Mezinárodní prefixy](#/international-prefixes)
    - [Prefixy agentury](#/agency-prefixes)
    - [Rozsahy prefixů nakladatele](#/publisher-prefix-ranges)
    - [Nakladatelé](#/publishers)
    - [Prefixy nakladatele (pracovník agentury)](#/agent/publisher-prefixes)
- Garant nakladatele
    - [Prefixy nakladatele (garant)](#/publisher/publisher-prefixes)
- Garant nakladatele, Distributor
    - [Fyzické osoby](#/natural-persons)
    - [Právnické osoby](#/legal-persons)
- Správce řad, Garant nakladatele, Distributor
    - [Řady](#/collections)
- Pracovník agentury, Garant nakladatele, Distributor
    - [Tituly](#/titles)
    - [Produkty](#/products)
    - [Čísla produktů](#/product-numbers)

## Aktéři

- [Správce číselníků Onix](#OnixCodetablesAdministrator)
- [Pracovník agentury](#Agent)
- [Správce řad](#CollectionsAdministrator)
- [Garant nakladatele](#PublisherGuarantor)
- [Systém nakladatele](#PublisherSystem)
- `#Distributor`
- [Systém](#System)

## Datový model

- [Číselník Onix](#OnixCodetable)
    - [Položka číselníku Onix](#OnixCodetableItem)
- [Mezinárodní prefix](#InternationalPrefix)
- [Prefix agentury](#AgencyPrefix)
- [Rozsah prefixů nakladatele](#PublisherPrefixRange)
- [Nakladatel](#Publisher)
    - [Členství](#Membership)
- [Prefix nakladatele](#PublisherPrefix)
- [Osoba](#Person)
- [Řada](#Collection)
- [Titul](#Title)
    - [Řada titulu](#TitleCollection)
    - [Příspěvek k titulu](#TitleContribution)
    - [Nakladatel titulu](#TitlePublisher)
- [Produkt](#Product)
    - [Vztah produktů](#ProductsRelation)
- [Číslo produktu](#ProductNumber)
- [Uživatel](#User)

## Klíčové požadavky

Specifikace modulu Agentura vychází z těchto klíčových požadavků:

1. Vést evidenci čísel produktů přidělených mezinárodní agenturou České národní agentuře ISBN a ISMN.
2. Vést evidenci přidělení čísel produktů nakladatelům.
3. Vést evidenci všech titulů a produktů s českým ISBN a ISMN a přidělovat jim tato čísla produktů.
4. Přebírat údaje titulů a produktů od nakladatelů a předávat je do knihovního systému.
5. Poskytovat data produktů distributorům a veřejnosti.

## Generické funkčnosti

Modul musí implementovat následující generické funkčnosti, které jsou popsány
v [modulu Uživatelé](reczech-users-fsd.html):

- Jednotné přihlášení přes všechny moduly (SSO)
- Vedení auditního logu všech změn v systému a jeho prohlížení

## TODO

### Úkoly

- Místo vydání titulu: Geografická autorita? Rozhraní?
- Co s Onix číselníkovými hodnotami typu Nedefinováno, Neuvedeno, a pod.? Na povinné položce, na nepovinné položce?
- Garant koeditora neuvidí tituly, produkty ani čísla produktů, dokud je hlavní nakladatel nezveřejní.
- Osoby, případy užití
    - Najít osobu (API Systém nakladatele, nebo obrazovky) - pro tituly
    - (Změnit titul) - mazat sirotky ve stavu Návrh měsíc po osiření
