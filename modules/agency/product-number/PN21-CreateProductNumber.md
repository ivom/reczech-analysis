## UC: `PN21` Vytvořit číslo produktu

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Vytvořit číslo produktu](#/product-numbers/create)

### Main success scenario:

1. Systém zobrazí agentury (viz [mezinárodní prefix](#InternationalPrefix)).
2. Uživatel vybere agenturu.
3. Systém načte záznamy [mezinárodních prefixů](#InternationalPrefix) pro danou agenturu,
   zařadí [výchozí](#InternationalPrefix.default) jako první a zobrazí jejich [formáty](#InternationalPrefix.format).
4. Uživatel vybere formát.
5. Systém najde [mezinárodní prefix](#InternationalPrefix) podle vybrané agentury a formátu.
6. Uživatel zadá číslo produktu.
7. Systém provede formální validace čísla produktu.
8. Systém oddělí ze začátku čísla produktu [hodnotu](#InternationalPrefix.value) mezinárodního prefixu.
9. Systém najde [prefix agentury](#AgencyPrefix) pro danou agenturu, jehož [hodnota](#AgencyPrefix.value) se shoduje se
   začátkem zbytku čísla produktu a oddělí tuto hodnotu ze začátku zbytku čísla produktu.
10. Systém najde [prefix nakladatele](#PublisherPrefix) pro daný prefix agentury,
    jehož [hodnota](#PublisherPrefix.value) je prázdná nebo se shoduje se začátkem zbytku čísla produktu.
11. Systém zobrazí [prefix nakladatele](#PublisherPrefix) a jeho [nakladatele](#Publisher).
12. Uživatel vybere Důvod blokace, zadá Poznámku blokace a vytvoří číslo produktu.
13. Systém provede formální validace dat.
14. Systém vytvoří nový záznam [čísla produktu](#ProductNumber) s:
    - [Prefix nakladatele](#ProductNumber.publisherPrefix) = nalezený nebo prázdný,
    - [Nakladatel](#ProductNumber.publisher) = nakladatel nalezeného prefixu nakladatele nebo prázdný,
    - [Produkt](#ProductNumber.product) = prázdný,
    - [Formát](#ProductNumber.format) = vybraný,
    - [Hodnota](#ProductNumber.value) = zadaná,
    - [Blokováno](#ProductNumber.blocked) = `true`,
    - [Důvod blokace](#ProductNumber.blockReason) = vybraný,
    - [Poznámka blokace](#ProductNumber.blockComment) = zadaná.
15. Systém [odešle titul](#TT90) produktu.

### Extensions:

- 7a. Počet číslic čísla produktu neodpovídá formátu:
    - 7a1. Systém zobrazí chybu.
- 7b. Kontrolní číslice čísla produktu neodpovídá formátu:
    - 7b1. Systém zobrazí chybu.
- 13a. Není vyplněno povinné pole:
    - 13a1. Systém zobrazí chybu.
- 9a. Hodnota žádného takového prefixu agentury se neshoduje se začátkem zbytku čísla produktu:
    - 9a1. Případ užití pokračuje krokem 12.
- 10a. Hodnota žádného takového prefixu nakladatele se neshoduje se začátkem zbytku čísla produktu:
    - 10a1. Případ užití pokračuje krokem 12.
