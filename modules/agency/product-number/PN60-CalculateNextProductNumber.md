## UC: `PN60` Vypočítat následující číslo produktu

- Úroveň: Podfunkce
- Předpoklady
    - Aktivní [prefix nakladatelele](#PublisherPrefix)
    - Formát (viz [Číslo produktu](#ProductNumber))
- Obrazovka: [Přidělit číslo produktu](#/product-numbers/assign)

### Main success scenario:

1. Systém načte:
    - [Nakladatele](#Publisher) [prefixu nakladatele](#PublisherPrefix.publisher)
    - [Hodnotu](#PublisherPrefix.value) prefixu nakladatele
    - [Následující sekvenci](#PublisherPrefix.nextSequence) prefixu nakladatele
    - [Rozsah prefixů nakladatele](#PublisherPrefixRange) [prefixu nakladatele](#PublisherPrefix.publisherPrefixRange)
    - [Prefix agentury](#AgencyPrefix) [rozsahu](#PublisherPrefixRange.agencyPrefix),
      jeho [hodnotu](#AgencyPrefix.value) a [agenturu](#AgencyPrefix.agency)
2. Systém najde záznam [mezinárodního prefixu](#InternationalPrefix) podle [agentury](#InternationalPrefix.agency) a
   [formátu](#InternationalPrefix.format) a načte jeho jeho [hodnotu](#InternationalPrefix.value).
3. Systém vypočítá kontrolní číslici podle formátu (viz [Číslo produktu](#ProductNumber)).
4. Systém sestaví nové číslo produktu takto:
    - (Mezinárodní prefix) `-`
      (Prefix agentury) `-`
      (Prefix nakladatelele) `-`
      (Následující sekvence prefixu nakladatele) `-`
      (Kontrolní číslice)
5. Systém zobrazí číslo produktu.
6. Uživatel potvrdí přidělení čísla produktu.
7. Systém navýší [následující sekvenci](#PublisherPrefix.nextSequence) prefixu nakladatele o jedničku.

### Extensions:

- 4a. Mezinárodní prefix má prázdnou [hodnotu](#InternationalPrefix.value):
    - 4a1. Systém sestaví číslo produktu počínaje prefixem agentury.
- 4b. Číslo produktu již existuje a je [zablokované](#ProductNumber.blocked) nebo má
  vyplněn [produkt](#ProductNumber.product):
    - 4b1. Systém navýší [následující sekvenci](#PublisherPrefix.nextSequence) prefixu nakladatele o jedničku a
      pokračuje krokem 3.
        - 4b1a. Prefix se vyčerpal, tj. [následující sekvence](#PublisherPrefix.nextSequence) po navýšení
          překročila [řád](#PublisherPrefixRange.magnitude) rozsahu prefixů nakladatele:
            - 4b1a1. Systém zavře prefix nakladatele, nastaví [stav](#PublisherPrefix.status) = Zavřený.
            - 4b1a2. Systém [vytvoří nový prefix nakladatele](#PP20) pro daného nakladatele a agenturu.
            - 4b1a3. Případ užití pokračuje krokem 1 pro nový prefix nakladatele.
- 6a. Uživatel zruší přidělení čísla produktu:
    - 6a1. Případ užití končí bez výsledného čísla produktu.
- 7a. Prefix se vyčerpal, tj. [následující sekvence](#PublisherPrefix.nextSequence) po navýšení
  překročila [řád](#PublisherPrefixRange.magnitude) rozsahu prefixů nakladatele:
    - 7a1. Systém zavře prefix nakladatele, nastaví [stav](#PublisherPrefix.status) = Zavřený.

### Poznámky

- Pokud číslo produktu již existuje, není zablokované a nemá vyplněn produkt, bude použito, jako by bylo nové.
