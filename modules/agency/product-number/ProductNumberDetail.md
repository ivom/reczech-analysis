## Screen: Číslo produktu `/product-numbers/detail`

### Form:

- Nakladatel (RO): [Albatros Media](#/publishers/detail)
- Titul (RO): [Božena Němcová: Babička](#/titles/detail)
- Produkt (RO): [Text, Pevná vazba](#/products/detail)
- Prefix nakladatele (RO): [80-12](#/publisher-prefixes/detail)
- Agentura (RO): ISBN (knihy)
- Formát (RO): EAN13
- Číslo produktu (RO): ISBN 978-80-12-12345-1
- Blokováno (RO checkbox): true
- Důvod blokace (RO): Pravděpodobně vydáno
- Poznámka k blokaci (RO): Přiděleno na Slovensku.
- [:Změnit](#/product-numbers/edit)
- [:Blokovat](#/product-numbers/block)
- [warning: Odblokovat]()
- [:Zpět](#/product-numbers)
