## UC: `PN25` Požádat o číslo produktu

- Primární aktér: [Systém nakladatele](#PublisherSystem)
- Předpoklady:
    - [Nakladatel](#Publisher), který službu zavolal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Vstupní položky:
    - Identifikátor vlastníka produktu `productOwnerId` (povinné)
    - Prefix nakladatele `publisherPrefix` (nepovinné), identifikovaný agenturou a plnou hodnotou, tj.:
        - (Agentura) (Mezinárodní prefix)-(Prefix agentury)-(Prefix nakladatele)
        - př.: `ISBN 978-80-243`
- Výstupní položky:
    - Číslo produktu, v plném formátu, př.: `ISBN 978-80-243-1234-1`

### Main success scenario:

1. Systém najde [produkt](#Product) podle [vlastníka titulu](#Title.owner) (= nakladatel, který službu zavolal)
   a [identifikátoru vlastníka produktu](#Product.ownerId).
2. Systém najde:
    - [titul](#Title) produktu,
    - [nakladatele](#Publisher) - [vlastníka titulu](#Title.owner),
    - agenturu podle [primárního typu obsahu](#Product.primaryContentType) produktu:
        - `11` Musical notation: ISMN,
        - jiný: ISBN,
3. Systém načte [aktivní](#PublisherPrefix.status) [prefixy nakladatele](#PublisherPrefix) pro daného nakladatele a
   agenturu (podle jejich [rozsahu](#PublisherPrefixRange) a [prefixu agentury](#AgencyPrefix)).
4. Systém vybere prefix nakladatele s nejmenším počtem zbývajících čísel.
5. Systém najde [výchozí](#InternationalPrefix.default) záznam [mezinárodního prefixu](#InternationalPrefix) pro danou
   agenturu a vybere jeho [formát](#InternationalPrefix.format).
6. Systém [vypočte následující číslo produktu](#PN60) pro vybraný prefix nakladatele a formát.
7. Systém vytvoří nový záznam [čísla produktu](#ProductNumber) s:
    - [Prefix nakladatele](#ProductNumber.publisherPrefix) = vybraný,
    - [Nakladatel](#ProductNumber.publisher) = vlastník titulu produktu,
    - [Produkt](#ProductNumber.product) = nalezený,
    - [Formát](#ProductNumber.format) = vybraný,
    - [Hodnota](#ProductNumber.value) = vypočtená,
    - [Blokováno](#ProductNumber.blocked) = `false`,
    - [Důvod blokace](#ProductNumber.blockReason) = prázdné,
    - [Poznámka blokace](#ProductNumber.blockComment) = prázdné.
8. Systém vrátí přidělené číslo produktu.

### Extensions:

- 1a. Nakladatel je ukončený ([closed](#Publisher.status)):
    - 1a1. Systém zapíše chybu a případ užití končí.
- 1b. Nakladatel nepotvrdil údaje déle než 1 rok ([čas potvrzení údajů](#Publisher.dataConfirmationTime)):
    - 1b1. Systém zapíše chybu a případ užití končí.
- 2a. Titul [patří](#Title.owner) jinému nakladateli:
    - 2a1. Systém zapíše chybu a případ užití končí.
- 3a. Nakladatel vyplnil na vstupu prefix nakladatele a ten se neshoduje s žádným načteným aktivním prefixem
  nakladatele:
    - 3a1. Systém zapíše chybu a případ užití končí.
- 4a. Nakladatel vyplnil na vstupu prefix nakladatele:
    - 4a1. Systém vybere vstupní prefix nakladatele.
- 4b. Nakladatel nemá v dané agentuře žádný aktivní prefix nakladatele:
    - 4b1. Systém [vytvoří nový prefix nakladatele](#PP20) a použije ho.
        - 4b1a. Nakladatel je v likvidaci ([closing](#Publisher.status)):
            - 4b1a1. Systém zapíše chybu a případ užití končí.

### Poznámky

- Volané případy užití nahradí interakci s uživatelem prostřednictvím obrazovek za výběr základní volby.
