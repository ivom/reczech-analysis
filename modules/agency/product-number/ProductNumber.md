## Entity: Číslo produktu `ProductNumber`

Číslo produktu reprezentuje ISBN nebo ISMN,
je přidělováno [agenturou](#AgencyPrefix.agency) nadřazeného prefixu agentury.

### Attributes:

- Prefix nakladatele `publisherPrefix` (n:0..1 `#PublisherPrefix`)
- Nakladatel `publisher` (n:0..1 `#Publisher`)
- Produkt `product` (n:0..1 `#Product`)
- Formát `format` (M enum: ISBN10, ISMN10, EAN13): EAN13
- Hodnota `value` (NK text): 978-80-243-0001-6
- Blokováno `blocked` (M boolean)
- Důvod blokace `blockReason` (O enum: unauthorizedUse, presumedPublished)
- Poznámka k blokaci `blockComment` (O text): Přiděleno na Slovensku.

### Formát

Výčet, který řídí formát čísla produktu a jeho zpracování. Udává:

- délku čísla (počet číslic)
- algoritmus výpočtu kontrolní číslice

Hodnoty:

- `ISBN10`:
    - pouze pro agenturu `ISBN`
    - 10-ti místný ISBN kód dle ISO 2108, používaný 1970 - 2006
    - algoritmus kontrolní číslice: modulo 11
    - příklad: 80-86000-00-1
- `ISMN10`:
    - pouze pro agenturu `ISMN`
    - 10-ti místný ISMN kód, používaný 1993 - 2007
    - příklad: M-2306-7118-7
- `EAN13`:
    - pro agentury `ISBN` i `ISMN`
    - 13-ti místný EAN-13 kód zboží
    - pro ISBN používaný od r. 2007, pro ISMN používaný od r. 2008
    - algoritmus kontrolní číslice: EAN

### Důvody blokace

- `unauthorizedUse`: Neoprávněně použito
- `presumedPublished` Pravděpodobně vydáno

### Poznámky

- Bez nakladatele, prefixu a produktu je to v případě neoprávněně použitého čísla produktu.
