## UC: `PN40` Změnit číslo produktu

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Změnit číslo produktu](#/product-numbers/edit)

### Main success scenario:

1. Uživatel najde a vybere číslo produktu.
2. Systém zobrazí údaje [čísla produktu](#ProductNumber).
3. Systém najde a zobrazí pro dané číslo produktu:
    - jeho [prefix nakladatele](#PublisherPrefix),
    - jeho [nakladatele](#Publisher),
    - jeho [produkt](#Product),
    - [titul](#Title) jeho produktu,
    - [agenturu](#AgencyPrefix.agency) podle prefixu nakladatele, jeho rozsahu a prefixu agentury.
4. Uživatel změní údaje čísla produktu.
5. Systém provede formální validace dat.
6. Systém zapíše data z formuláře do [čísla produktu](#ProductNumber) a přepočítá jeho [hodnotu](#ProductNumber.value)
   podle nového [formátu](#ProductNumber.format).
7. Systém [odešle titul](#TT90) produktu.

### Extensions:

- 5a. Není vyplněno povinné pole:
    - 5a1. Systém zobrazí chybu.
