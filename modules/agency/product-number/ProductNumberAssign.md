## Screen: Přidělit číslo produktu `/product-numbers/assign`

### Form:

- Nakladatel (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA): Albatros Media
- Titul (RO): Božena Němcová: Babička
- Produkt (RO): Text, Pevná vazba
- Agentura (RO): ISBN (knihy)
- Velikost nového prefixu (R radios: pro 1 000 titulů, pro 100 titulů, pro 10 titulů): pro 1 000 titulů
- Prefix nakladatele (R select: 80-6543, 80-243, 80-12): 80-12
- Formát (R select: EAN13, ISBN10): EAN13
- Hodnota (RO): ISBN 978-80-12-12345-1
- [primary: Uložit](#/products/detail)
- [:Zpět](#/products/detail)
