## Screen: Blokace čísla produktu `/product-numbers/block`

### Form:

- Nakladatel (RO): Albatros Media
- Titul (RO): Božena Němcová: Babička
- Produkt (RO): Text, Pevná vazba
- Prefix nakladatele (RO): 80-12
- Agentura (RO): ISBN (knihy)
- Formát (RO): EAN13
- Číslo produktu (RO): 978-80-12-12345-1
- Důvod blokace (R select: Neoprávněně použito, Pravděpodobně vydáno)
- Poznámka k blokaci (multiLine)
- [primary: Uložit](#/product-numbers/detail)
- [:Zpět](#/product-numbers/detail)
