## UC: `PN20` Přidělit číslo produktu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovky: [Detail produktu](#/products/detail), [Přidělit číslo produktu](#/product-numbers/assign)

### Main success scenario:

1. Uživatel [vybere produkt](#PD10) a rozhodne se přidělit mu číslo produktu.
2. Systém zobrazí vybraný [produkt](#Product).
3. Systém najde a zobrazí:
    - [titul](#Title) produktu,
    - [nakladatele](#Publisher) - [vlastníka titulu](#Title.owner),
    - agenturu podle [primárního typu obsahu](#Product.primaryContentType) produktu:
        - `11` Musical notation: ISMN,
        - jiný: ISBN,
4. Systém načte [aktivní](#PublisherPrefix.status) [prefixy nakladatele](#PublisherPrefix) pro daného nakladatele a
   agenturu (podle jejich [rozsahu](#PublisherPrefixRange) a [prefixu agentury](#AgencyPrefix)).
5. Systém seřadí prefixy nakladatele podle počtu zbývajících čísel produktů od nejmenšího a první předvybere.
6. Uživatel vybere prefix nakladatele.
7. Systém najde [výchozí](#InternationalPrefix.default) záznam [mezinárodního prefixu](#InternationalPrefix) pro danou
   agenturu a vybere a zobrazí jeho [formát](#InternationalPrefix.format).
8. Systém [vypočte následující číslo produktu](#PN60) pro vybraný prefix nakladatele a formát.
9. Systém vytvoří nový záznam [čísla produktu](#ProductNumber) s:
    - [Prefix nakladatele](#ProductNumber.publisherPrefix) = vybraný,
    - [Nakladatel](#ProductNumber.publisher) = vlastník titulu vybraného produktu,
    - [Produkt](#ProductNumber.product) = vybraný,
    - [Formát](#ProductNumber.format) = vybraný,
    - [Hodnota](#ProductNumber.value) = vypočtená,
    - [Blokováno](#ProductNumber.blocked) = `false`,
    - [Důvod blokace](#ProductNumber.blockReason) = prázdné,
    - [Poznámka blokace](#ProductNumber.blockComment) = prázdné.
10. Systém [odešle titul](#TT90) produktu.

### Extensions:

- 1a. Nakladatel je ukončený ([closed](#Publisher.status)):
    - 1a1. Systém zobrazí chybu a případ užití končí.
- 1b. Uživatel má pouze roli Garant nakladatele a nakladatel nepotvrdil údaje déle než 1
  rok ([čas potvrzení údajů](#Publisher.dataConfirmationTime)):
    - 1b1. Systém zobrazí chybu "Potvrďte prosím nejprve Vaše údaje." a případ užití končí.
- 3a. Uživatel má pouze roli Garant nakladatele a titul [patří](#Title.owner) jinému nakladateli:
    - 3a1. Systém zobrazí chybu a případ užití končí.
- 3b. Uživatel je Pracovník agentury:
    - 3b1. Systém umožní uživateli vybrat jiného nakladatele.
    - 3b2. Uživatel vybere nakladatele.
- 4a. Nakladatel má v dané agentuře právě jeden aktivní prefix nakladatele:
    - 4a1. Systém vybere tento prefix nakladatele.
    - 4a2. Případ užití pokračuje krokem 7.
- 4b. Nakladatel nemá v dané agentuře žádný aktivní prefix nakladatele:
    - 4b1. Systém skryje pole Prefix nakladatele, [vytvoří nový prefix nakladatele](#PP20) a použije ho.
        - 4b1a. Nakladatel je v likvidaci ([closing](#Publisher.status)):
            - 4b1a1. Systém zobrazí chybu "Nakladatelům v likvidaci nelze přidělit nový prefix." a případ užití končí.
- 7a. Uživatel je Pracovník agentury:
    - 7a1. Systém načte záznamy [mezinárodního prefixu](#InternationalPrefix) pro danou agenturu, zobrazí
      jejich [formáty](#InternationalPrefix.format), zařadí [výchozí](#InternationalPrefix.default) jako první a
      předvybere ho.
    - 7a2. Uživatel vybere formát.
- 8a. Uživatel nepotvrdil přidělení nového čísla produktu:
    - 8a1. Případ užití končí.

### Poznámky

- Pracovník agentury změní nakladatele (3b.) v případě koedice při přidělení druhého čísla produktu pro "vedlejšího"
  nakladatele.
