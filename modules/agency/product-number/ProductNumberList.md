## Screen: Čísla produktů `/product-numbers`

### Form:

- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- Prefix nakladatele (select: 80-12, 80-17, 80-243, 80-88808)
- Číslo produktu - S pomlčkami nebo bez. S agenturou nebo bez.
- Pouze blokováno (checkbox)

### Table:

- Číslo produktu
    - [ISBN 978-80-12-12345-1](#/product-numbers/detail)
    - [ISBN 978-80-12-12346-2](#/product-numbers/detail)
    - [ISBN 978-80-17-98765-3](#/product-numbers/detail)
    - [ISBN 978-80-243-2345-4](#/product-numbers/detail)
    - [ISBN 978-80-88808-56-5](#/product-numbers/detail)
- Nakladatel
    - Albatros Media
    - Albatros Media
    - Euromedia Group
    - Grada Publishing
    - Moravská Bastei MOBA
- Blokováno (checkbox)
    - false
    - false
    - true

[:Vytvořit](#/product-numbers/create)
