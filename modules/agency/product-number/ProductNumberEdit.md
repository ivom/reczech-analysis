## Screen: Změnit číslo produktu `/product-numbers/edit`

### Form:

- Nakladatel (RO): Albatros Media
- Titul (RO): Božena Němcová: Babička
- Produkt (RO): Text, Pevná vazba
- Prefix nakladatele (RO): 80-12
- Agentura (RO): ISBN (knihy)
- Formát (R select: ISBN10, EAN13): EAN13
- Číslo produktu (RO): 978-80-12-12345-1
- [primary: Uložit](#/product-numbers/detail)
- [:Zpět](#/product-numbers/detail)
