## UC: `PN46` Odblokovat číslo produktu

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Číslo produktu](#/product-numbers/detail)

### Main success scenario:

1. Uživatel najde a vybere zablokované číslo produktu a odblokuje ho.
2. Systém na [čísle produktu](#ProductNumber) nastaví:
    - [Blokováno](#ProductNumber.blocked) = `false`,
    - [Důvod blokace](#ProductNumber.blockReason) = prázdné,
    - [Poznámka blokace](#ProductNumber.blockComment) = prázdné.
