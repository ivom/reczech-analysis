## UC: `PN10` Prohlížet čísla produktů

- Primární aktér: [Pracovník agentury](#Agent), [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Čísla produktů](#/product-numbers)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
    - Číslo produktu: hledá v poli [hodnota](#ProductNumber.value)
2. Systém najde záznamy [čísel produktu](#ProductNumber), které odpovídají zadaným kritériím, a zobrazí:
    - Číslo produktu: [hodnota](#ProductNumber.value) čísla produktu s předřazenou [agenturou](#AgencyPrefix.agency)
      získanou z [prefixu nakladatele čísla produktu](#ProductNumber.publisherPrefix),
      [jeho rozsahu](#PublisherPrefix.publisherPrefixRange) a prefixu agentury.
3. Systém seřadí záznamy podle zobrazené hodnoty Číslo produktu.
4. Uživatel vybere záznam.
5. Systém zobrazí detail vybraného [čísla produktu](#ProductNumber).
6. Systém najde a zobrazí pro dané číslo produktu:
    - jeho [prefix nakladatele](#PublisherPrefix),
    - jeho [nakladatele](#Publisher),
    - jeho [produkt](#Product),
    - [titul](#Title) jeho produktu,
    - [agenturu](#AgencyPrefix.agency) podle prefixu nakladatele, jeho rozsahu a prefixu agentury.

### Extensions:

- 1a. Uživatel má roli Garant nakladatele:
    - 1a1. Systém předvyplní výběrové kritérium na nakladatele přihlášeného uživatele.
- 2a. Uživatel má roli Distributor:
    - 2a1. Systém vynechá čísla produktů, které patří [neveřejným](#Product.accessibility) produktům.
- 2b. Uživatel má roli Garant nakladatele:
    - 2b1. Systém vynechá čísla produktů, které patří [neveřejným](#Product.accessibility) produktům,
      jejichž [titul je vlastněný](#Title.owner) jiným nakladatelem.
