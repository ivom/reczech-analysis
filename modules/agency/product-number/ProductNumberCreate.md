## Screen: Vytvořit číslo produktu `/product-numbers/create`

### Form:

- Agentura (R select: ISBN, ISMN)
- Formát (R select: EAN13, ISBN10)
- Číslo produktu (R) - S nebo bez pomlček.
- Prefix nakladatele (RO): 80-234
- Nakladatel (RO): Albatros Media
- Důvod blokace (R select: Neoprávněně použito, Pravděpodobně vydáno)
- Poznámka k blokaci (multiLine)
- [primary: Uložit](#/product-numbers/detail)
- [:Zpět](#/product-numbers)
