## UC: `PR21` Najít obsahující rozsah prefixů nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Prefix agentury](#AgencyPrefix)
    - Hodnota prefixu nakladatele

### Main success scenario:

1. Systém [sestaví plný prefix nakladatele](#PP60).
2. Systém načte [agenturu](#AgencyPrefix.agency) prefixu agentury.
3. Systém najde všechny [záznamy rozsahů prefixů nakladatele](#PublisherPrefixRange) pro tuto agenturu.
4. Systém pro každý záznam [sestaví plný začátek a plný konec](#PP60).
5. Systém najde první záznam, pro který platí:
    - (Plný začátek) <= (Plný prefix nakladatele) <= (Plný konec)
