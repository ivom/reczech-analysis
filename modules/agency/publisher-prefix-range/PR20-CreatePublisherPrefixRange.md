## UC: `PR20` Vytvořit rozsah prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Vytvořit rozsah prefixů nakladatele](#/publisher-prefix-ranges/create)

### Main success scenario:

1. Uživatel vybere prefix agentury.
2. Systém vypočítá možný rozsah řádu a vyplní podle toho hodnoty pro výběr počtu čísel.
3. Uživatel vybere počet čísel.
4. Uživatel zadá začátek rozsahu.
5. Uživatel zadá konec rozsahu.
6. Uživatel vytvoří rozsah prefixů nakladatele.
7. Systém vytvoří nový záznam [rozsahu prefixů nakladatele](#PublisherPrefixRange) s:
    - [Stav](#PublisherPrefixRange.status) = Aktivní
    - [Následující sekvence](#PublisherPrefixRange.nextSequence) = začátek

### Extensions:

- 4a. Počet znaků začátku neodpovídá řádu rozsahu:
    - 4a1. Systém zobrazí chybu "Počet znaků neodpovídá řádu rozsahu."
- 4b. Začátek [leží uvnitř](#PR21) jiného [rozsahu prefixů nakladatele](#PublisherPrefixRange):
    - 4b1. Systém zobrazí chybu "Leží uvnitř jiného rozsahu: (plný začátek) - (plný konec)."
- 5a. Počet znaků konce neodpovídá řádu rozsahu:
    - 5a1. Systém zobrazí chybu "Počet znaků konce neodpovídá řádu rozsahu."
- 5b. Konec [leží uvnitř](#PR21) jiného [rozsahu prefixů nakladatele](#PublisherPrefixRange):
    - 5b1. Systém zobrazí chybu "Leží uvnitř jiného rozsahu: (plný začátek) - (plný konec)."
- 5c. Konec je menší než začátek:
    - 5c1. Systém zobrazí chybu "Konec nesmí být menší než začátek."
- 5d. Systém najde jiný rozsah prefixů nakladatele, který je [obsažen v novém rozsahu](#PR22):
    - 5d1. Systém zobrazí chybu "Rozsah se překrývá s jiným rozsahem: (plný začátek) - (plný konec)."
