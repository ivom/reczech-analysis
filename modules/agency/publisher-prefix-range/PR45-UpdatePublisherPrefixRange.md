## UC: `PR45` Změnit rozsah prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Změnit rozsah prefixů nakladatele](#/publisher-prefix-ranges/edit)

### Main success scenario:

1. Uživatel vybere rozsah prefixů nakladatele a změní jeho [konec](#PublisherPrefixRange.end).
2. Systém uloží změny do [rozsahu prefixů nakladatele](#PublisherPrefixRange).

### Extensions:

- 2a. Počet znaků konce neodpovídá řádu rozsahu:
    - 2a1. Systém zobrazí chybu "Počet znaků konce neodpovídá řádu rozsahu."
- 2b. Konec [leží uvnitř](#PR21) jiného [rozsahu prefixů nakladatele](#PublisherPrefixRange):
    - 2b1. Systém zobrazí chybu "Leží uvnitř jiného rozsahu: (plný začátek) - (plný konec)."
- 2c. Konec je menší než začátek:
    - 2c1. Systém zobrazí chybu "Konec nesmí být menší než začátek."
- 2d. Systém najde jiný rozsah prefixů nakladatele, který je [obsažen ve změněném rozsahu](#PR22):
    - 2d1. Systém zobrazí chybu "Rozsah se překrývá s jiným rozsahem: (plný začátek) - (plný konec)."
- 2e. Konec < [Následující sekvence](#PublisherPrefixRange.nextSequence) rozsahu - 1:
    - 2e1. Systém zobrazí chybu "Vyčleněné prefixy nakladatele již byly přiděleny."
- 2f. Konec = [Následující sekvence](#PublisherPrefixRange.nextSequence) rozsahu - 1:
    - 2f1. Systém zavře rozsah prefixů nakladatele, nastaví [stav](#PublisherPrefixRange.status) = Zavřený.
