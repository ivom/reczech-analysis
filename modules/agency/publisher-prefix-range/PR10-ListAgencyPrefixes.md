## UC: `PR10` Prohlížet rozsahy prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Rozsahy prefixů nakladatele](#/publisher-prefix-ranges)

### Main success scenario:

1. Systém zobrazí [aktivní](#PublisherPrefixRange.status) záznamy [rozsahů prefixů nakladatele](#PublisherPrefixRange)
   řazené podle:
    - [stavu](#PublisherPrefixRange.status) (aktivní, pak zavřené),
    - [agentury](#AgencyPrefix.agency) a [hodnoty](#AgencyPrefix.value) prefixu agentury,
    - [začátku](#PublisherPrefixRange.start).

### Extensions:

- 1a. Uživatel zadá výběrová kritéria:
    - 1a1. Systém zobrazí odpovídající záznamy.
- 1b. Uživatel zadá zobrazení i zavřených rozsahů prefixů nakladatele:
    - 1b1. Systém zobrazí všechny záznamy bez ohledu na [stav](#PublisherPrefixRange.status).
