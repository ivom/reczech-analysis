## Screen: Vytvořit rozsah prefixů nakladatele `/publisher-prefix-ranges/create`

### Form:

- Prefix agentury (R select: ISBN 80, ISMN)
- Počet čísel (R select: 10, 100, 1 000, 10 000, 100 000)
- Začátek (R)
- Konec (R)

- [primary: Uložit](#/publisher-prefix-ranges/detail)
- [:Zpět](#/publisher-prefix-ranges)
