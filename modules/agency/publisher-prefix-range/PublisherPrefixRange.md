## Entity: Rozsah prefixů nakladatele `PublisherPrefixRange`

### Attributes:

- Prefix agentury `agencyPrefix` (n:1 `#AgencyPrefix`)
- Řád `magnitude` (M integer): 2 - Řád počtu čísel produktů v prefixech nakladatele v rozsahu, viz níže.
- Začátek `start` (M integer): 85 000 - První prefix nakladatele v rozsahu
- Konec `end` (M integer): 89 999 - Poslední prefix nakladatele v rozsahu
- Stav `status` (M enum: active, closed)
- Následující sekvence `nextSequence` (M integer): 85 123 - Následující sekvence k přidělení.

### Stavový diagram

```plant
[*] --> Active: Create (Vytvořit)
Active: Aktivní
Active --> Closed: Close (Zavřít)
Closed: Zavřený
Closed --> Active: Open (Otevřít)
Active --> [*]: Delete (Smazat)
Closed --> [*]: Delete (Smazat)
```

### Poznámky

- Řád:
    - řád 1 -> každý prefix nakladatele obsahuje 10 čísel produktů
    - řád 2 -> každý prefix nakladatele obsahuje 100 čísel produktů
    - řád 3 -> každý prefix nakladatele obsahuje 1 000 čísel produktů
    - řád 4 -> každý prefix nakladatele obsahuje 10 000 čísel produktů
    - řád 5 -> každý prefix nakladatele obsahuje 100 000 čísel produktů
