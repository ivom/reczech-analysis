## UC: `PR41` Otevřít rozsah prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Rozsah prefixů nakladatele](#/publisher-prefix-ranges/detail)

### Main success scenario:

1. Uživatel vybere zavřený rozsah prefixů nakladatele a otevře ho.
2. Systém nastaví:
    - [Stav](#PublisherPrefixRange.status) = Aktivní

### Extensions:

- 2a. [Rozsah prefixů nakladatele](#PublisherPrefixRange) je vyčerpaný
  ([následující sekvence](#PublisherPrefixRange.nextSequence) = [konec](#PublisherPrefixRange.end)):
    - 2a1. Systém zobrazí chybu "Rozsah prefixů nakladatele je vyčerpaný."
