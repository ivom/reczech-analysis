## UC: `PR30` Smazat rozsah prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Rozsah prefixů nakladatele](#/publisher-prefix-ranges/detail)

### Main success scenario:

1. Uživatel vybere rozsah prefixů nakladatele a smaže ho.
2. Systém smaže vybraný záznam [rozsahu prefixů nakladatele](#PublisherPrefixRange).

### Extensions:

- 2a. Pro prefix agentury existuje záznam [prefixu nakladatele](#PublisherPrefix):
    - 2a1. Systém zobrazí chybu "Existují podřízené záznamy."
