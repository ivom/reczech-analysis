## UC: `PR22` Najít obsažený rozsah prefixů nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Rozsah prefixů nakladatele](#PublisherPrefixRange)

### Main success scenario:

1. Systém [sestaví plný začátek a plný konec](#PP60) prefixu nakladatele.
2. Systém načte [prefix agentury](#PublisherPrefixRange.agencyPrefix) a jeho [agenturu](#AgencyPrefix.agency).
3. Systém najde všechny ostatní [záznamy rozsahů prefixů nakladatele](#PublisherPrefixRange) pro tuto agenturu.
4. Systém pro každý záznam [sestaví plný začátek a plný konec](#PP60).
5. Systém najde první záznam, pro který platí zároveň:
    - (Plný začátek vstupního rozsahu) <= (Plný začátek jiného rozsahu)
    - (Plný konec jiného rozsahu) <= (Plný konec vstupního rozsahu)
