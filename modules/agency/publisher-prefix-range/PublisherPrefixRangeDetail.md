## Screen: Rozsah prefixů nakladatele `/publisher-prefix-ranges/detail`

### Form:

- Prefix agentury (RO): [ISBN 80](#/agency-prefixes/detail)
- Začátek (RO): 200
- Konec (RO): 699
- Následující sekvence (RO): 234
- Stav (RO): Aktivní

- [warning: Smazat](#/publisher-prefix-ranges)
- [warning: Zavřít]()
- [warning: Otevřít]()
- [:Změnit](#/publisher-prefix-ranges/edit)
- [:Prefixy nakladatele](#/agent/publisher-prefixes)
- [:Zpět](#/publisher-prefix-ranges)
