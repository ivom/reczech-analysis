## Screen: Rozsahy prefixů nakladatele `/publisher-prefix-ranges`

### Form:

- Prefix agentury (select: ISBN 80, ISMN)
- Zobrazit zavřené (checkbox)

### Table:

- Prefix agentury
    - [ISBN 80](#/publisher-prefix-ranges/detail)
    - [ISBN 80](#/publisher-prefix-ranges/detail)
    - [ISBN 80](#/publisher-prefix-ranges/detail)
    - [ISBN 80](#/publisher-prefix-ranges/detail)
    - [ISBN 80](#/publisher-prefix-ranges/detail)
- Počet čísel
    - 100 000
    - 10 000
    - 1 000
    - 100
    - 10
- Začátek
    - 00
    - 200
    - 7 000
    - 85 000
    - 900 000
- Konec
    - 19
    - 699
    - 8 499
    - 89 999
    - 999 999
- Stav
    - Aktivní
    - Aktivní
    - Aktivní
    - Aktivní
    - Zavřený
- Následující sekvence
    - 06
    - 678
    - 7 012
    - 88 008
    - 923 456

[:Vytvořit](#/publisher-prefix-ranges/create)
