## Screen: Změnit rozsah prefixů nakladatele `/publisher-prefix-ranges/edit`

### Form:

- Prefix agentury (RO): [ISBN 80](#/agency-prefixes/detail)
- Začátek (RO): 200
- Konec (R): 699

- [primary: Uložit](#/publisher-prefix-ranges/detail)
- [:Zpět](#/publisher-prefix-ranges/detail)
