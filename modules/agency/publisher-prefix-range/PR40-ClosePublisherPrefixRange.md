## UC: `PR40` Zavřít rozsah prefixů nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Rozsah prefixů nakladatele](#/publisher-prefix-ranges/detail)

### Main success scenario:

1. Uživatel vybere aktivní rozsah prefixů nakladatele a zavře ho.
2. Systém nastaví:
    - [Stav](#PublisherPrefixRange.status) = Zavřený
