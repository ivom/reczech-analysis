## UC: `PE20` Vytvořit fyzickou osobu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Vytvořit fyzickou osobu](#/natural-persons/create)

### Main success scenario:

1. Uživatel zadá údaje fyzické osoby a vytvoří ji.
2. Systém provede formální validace dat.
3. Systém zapíše data z formuláře do nových záznamů:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních jmen](#NaturalPersonAlternativeName).
4. Systém na [osobě](#Person) nastaví:
    - [Zdrojový identifikátor](#Person.sourceId) = `rck` + interní id záznamu
    - [Právní forma](#Person.legalForm) = Fyzická osoba
    - [Stav](#Person.status) = Návrh
    - [Navrhovatel](#Person.proposer) = [Nakladatel](#Publisher) přihlášeného uživatele
      (identifikovaný zdrojovým identifikátorem jeho organizace)
    - [Identifikátor navrhovatele](#Person.proposer) = prázdný
5. Systém [odešle osobu](#PE90).

### Extensions:

- 1a. Uživatel zadá / smaže Datum úmrtí:
    - 1a1. Systém skryje / zobrazí Souhlas se zveřejněním.
- 2a. Není vyplněno Příjmení ani Jméno:
    - 2a1. Systém zobrazí chybu.
