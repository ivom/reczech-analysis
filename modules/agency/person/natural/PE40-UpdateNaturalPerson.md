## UC: `PE40` Změnit fyzickou osobu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Změnit fyzickou osobu](#/natural-persons/edit)

### Main success scenario:

1. Uživatel najde a vybere fyzickou osobu k editaci.
2. Systém zobrazí údaje [osoby](#Person).
3. Uživatel změní údaje osoby.
4. Systém provede formální validace dat.
5. Systém zapíše data z formuláře do:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních jmen](#NaturalPersonAlternativeName).
6. Systém [odešle osobu](#PE90).

### Extensions:

- 2a. Osoba nemá [stav](#Person.status) Návrh:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 2b. Nakladatel přihlášeného uživatele se neshoduje s [navrhovatelem](#Person.proposer) osoby:
    - 2b1. Systém zobrazí chybu a případ užití končí.
- 3a. Uživatel zadá / smaže Datum úmrtí:
    - 3a1. Systém skryje / zobrazí Souhlas se zveřejněním.
- 4a. Není vyplněno Příjmení ani Jméno:
    - 4a1. Systém zobrazí chybu.
