## Screen: Fyzické osoby `/natural-persons`

### Form:

- Příjmení
- Jméno
- Identifikátor - ISNI, ORCID nebo NK ČR AUT
- Stav (select: Potvrzeno, Návrh)
- Navrhovatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)

### Table:

- Příjmení
    - [Jirásek](#/natural-persons/detail)
    - [Kolínský](#/natural-persons/detail)
    - [Kutnohorská](#/natural-persons/detail)
    - [Mrštík](#/natural-persons/detail)
    - [Mrštík](#/natural-persons/detail)
    - [Němcová](#/natural-persons/detail)
- Jméno
    - Alois
    - Adalbert
    - Eliška
    - Alois
    - Vilém
    - Božena
- Datum narození
    - 23.8.1851
    -
    -
    - 1861
    - 1863
    - 4.2.1820
- Datum úmrtí
    - 12.3.1930
    -
    -
    - 1925
    - 1912
    - 21.1.1862
- Stav
    - Potvrzeno
    - Návrh
    - Návrh
    - Potvrzeno
    - Potvrzeno
    - Potvrzeno

[:Vytvořit](#/natural-persons/create)
