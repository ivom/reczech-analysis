## Screen: Vytvořit fyzickou osobu `/natural-persons/create`

### Form:

- Jméno
- Příjmení
- Datum narození (date)
- Datum úmrtí (date)
- Biografická poznámka (multiLine)
- Osoba souhlasí se zveřejněním osobních údajů (checkbox)

> Data jsou buď konkrétní den nebo pouze rok.

### Table: Identifikátory

- Typ (R select: ISNI, ORCID)
    - .
- Identifikátor (R)
    - .
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Alternativní jména

- Jméno (O)
    - .
- Příjmení (O)
    - .
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/natural-persons/detail)
- [:Zpět](#/natural-persons)
