## Screen: Změnit fyzickou osobu `/natural-persons/edit`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Jméno: Božena
- Příjmení: Němcová
- Datum narození (date): 4. 2. 1820
- Datum úmrtí (date): 21. 1. 1862
- Biografická poznámka (multiLine): Božena Němcová byla česká spisovatelka národního obrození.
- Osoba souhlasí se zveřejněním osobních údajů (checkbox)

> Data jsou buď konkrétní den nebo pouze rok.

### Table: Identifikátory

- Typ (R select: ISNI, ORCID)
    - ISNI
    - ORCID
- Identifikátor (R)
    - 0000000121343289
    - 0000-0002-1825-0097
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Alternativní jména

- Jméno (O)
    - Barbora
    - Barbora
- Příjmení (O)
    - Novotná
    - Panklová
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/natural-persons/detail)
- [:Zpět](#/natural-persons/detail)
