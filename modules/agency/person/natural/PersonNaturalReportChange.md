## Screen: Nahlásit změnu fyzické osoby `/natural-persons/report-change`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Jméno (RO): Božena
- Příjmení (RO): Němcová
- Návrh změn (R multiLine)
- [primary: Odeslat](#/natural-persons/detail)
- [:Zpět](#/natural-persons/detail)
