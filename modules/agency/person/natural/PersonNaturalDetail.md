## Screen: Fyzická osoba `/natural-persons/detail`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Jméno (RO): Božena
- Příjmení (RO): Němcová
- Datum narození (RO): 4. 2. 1820
- Datum úmrtí (RO): 21. 1. 1862
- Biografická poznámka (RO): Božena Němcová byla česká spisovatelka národního obrození.
- Stav (RO): Návrh
- Navrhovatel (RO): Albatros Media

### Table: Identifikátory

- Typ
    - ISNI
    - ORCID
- Identifikátor
    - [0000000121343289](https://isni.org/isni/0000000121343289)
    - [0000-0002-1825-0097](https://orcid.org/0000-0002-1825-0097)

### Table: Alternativní jména

- Jméno
    - Barbora
    - Barbora
- Příjmení
    - Novotná
    - Panklová

### Form:

- [:Změnit](#/natural-persons/edit)
- [:Nahlásit změnu](#/natural-persons/report-change)
- [:Smazat](#/natural-persons)
- [:Tituly](#/titles)
- [:Zpět](#/natural-persons)
