## UC: `PE30` Smazat fyzickou osobu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Fyzická osoba](#/natural-persons/detail)

### Main success scenario:

1. Uživatel najde a vybere fyzickou osobu a smaže ji.
2. Systém smaže:
   - vybraný záznam [osoby](#Person),
   - její [identifikátory](#PersonIdentifier),
   - její [alternativní jména](#NaturalPersonAlternativeName).

### Extensions:

- 2a. Osoba nemá [stav](#Person.status) Návrh:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 2b. Nakladatel přihlášeného uživatele se neshoduje s [navrhovatelem](#Person.proposer) osoby:
    - 2b1. Systém zobrazí chybu a případ užití končí.
- 2c. Pro osobu existuje záznam [příspěvku k titulu](#TitleContribution):
    - 2c1. Systém zobrazí chybu "Existují podřízené záznamy."
