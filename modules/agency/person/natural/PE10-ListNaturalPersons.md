## UC: `PE10` Prohlížet fyzické osoby

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Fyzické osoby](#/natural-persons)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [osoby](#Person), které mají [právní formu](#Person.legalForm) Fyzická osoba a
   odpovídají zadaným kritériím, a seřadí je podle:
    - [Příjmení](#Person.lastName)
    - [Jméno](#Person.firstName)
    - [Datum narození](#Person.birthDate) sestupně
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybrané [osoby](#Person).
5. Systém najde a zobrazí pro danou osobu:
    - všechny její [identifikátory](#Person.identifiers),
    - všechny její [alternativní jména](#Person.naturalAlternativeNames).
