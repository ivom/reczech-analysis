## Entity: Alternativní jméno fyzické osoby `NaturalPersonAlternativeName`

### Attributes:

- Jméno `firstName` (C text)
- Příjmení `lastName` (C text)

### Poznámky

- Musí být vyplněno alespoň jedno z polí Jméno, Příjmení.
