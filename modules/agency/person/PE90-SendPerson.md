## UC: `PE90` Odeslat osobu

- Úroveň: Podfunkce
- Předpoklady
    - [Osoba](#Person)

### Main success scenario:

1. Systém odešle zprávu s údaji těchto entit:
    - [osoba](#Person)
    - její obsažené entity:
        - [identifikátory](#PersonIdentifier),
        - [alternativní jména fyzické osoby](#NaturalPersonAlternativeName),
        - [alternativní názvy právnické osoby](#LegalPersonAlternativeName),
    - jeho nadřazené entity:
        - [nakladatelé](#TitlePublisher), kde nakladatel je identifikován ??? TODO.

### Poznámky

- Odeslaná zpráva o osobě ve [stavu](#Person.status) Návrh bude směrována do systému Autority.
