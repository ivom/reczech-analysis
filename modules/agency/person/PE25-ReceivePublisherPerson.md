## UC: `PE25` Přijmout osobu nakladatele

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Osoba nakladatele
- Předpoklady:
    - [Nakladatel](#Publisher), který zprávu odeslal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Položky zprávy Osoba nakladatele se shodují s položkami [entity Osoba](#Person) s těmito výjimkami:
    - Tyto položky zpráva neobsahuje:
        - Zdrojový identifikátor
        - Stav
        - Navrhovatel
    - Tato položka je ve zprávě povinná:
        - Identifikátor navrhovatele

### Main success scenario:

1. Systém přijme zprávu o osobě nakladatele.
2. Systém najde [osobu](#Person) podle [navrhovatele](#Person.proposer) (= nakladatel, který zprávu odeslal)
   a [identifikátoru navrhovatele](#Person.proposerId).
3. Systém zapíše údaje ze zprávy do:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních jmen fyzické osoby](#NaturalPersonAlternativeName).
    - jejích [alternativních názvů právnické osoby](#LegalPersonAlternativeName).

### Extensions:

- 2a. Osoba s daným navrhovatelem a identifikátorem navrhovatele neexistuje:
    - 2a1. Systém založí novou [osobu](#Person) s:
        - [Zdrojový identifikátor](#Person.sourceId) = `rck` + interní id záznamu
        - [Stav](#Person.status) = Návrh
        - [Navrhovatel](#Person.proposer) = [Nakladatel](#Publisher), který zprávu odeslal
