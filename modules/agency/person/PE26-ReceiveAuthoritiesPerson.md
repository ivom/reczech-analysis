## UC: `PE26` Přijmout osobu Autorit

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Osoba Autorit
- Položky zprávy Osoba Autorit se shodují s položkami [entity Osoba](#Person) s těmito výjimkami:
    - Tyto položky zpráva neobsahuje:
        - Stav
        - Navrhovatel
        - Identifikátor navrhovatele

### Main success scenario:

1. Systém přijme zprávu o osobě Autorit.
2. Systém najde [osobu](#Person) podle [zdrojového identifikátoru](#Person.sourceId).
3. Systém zapíše údaje ze zprávy do:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních jmen fyzické osoby](#NaturalPersonAlternativeName).
    - jejích [alternativních názvů právnické osoby](#LegalPersonAlternativeName).

### Extensions:

- 2a. Osoba s daným zdrojovým identifikátorem neexistuje:
    - 2a1. Systém založí novou [osobu](#Person) s:
        - [Stav](#Person.status) = Potvrzeno
        - [Navrhovatel](#Person.proposer) = prázdný
        - [Identifikátor navrhovatele](#Person.proposerId) = prázdný
