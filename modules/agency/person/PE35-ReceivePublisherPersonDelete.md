## UC: `PE35` Přijmout smazání osoby nakladatele

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Smazání osoby nakladatele
- Předpoklady:
    - [Nakladatel](#Publisher), který zprávu odeslal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Položky zprávy Smazání osoby nakladatele:
    - Identifikátor navrhovatele `proposerId` (povinné)

### Main success scenario:

1. Systém přijme zprávu o smazání osoby nakladatele.
2. Systém najde [osobu](#Person) podle [navrhovatele](#Person.proposer) (= nakladatel, který zprávu odeslal)
   a [identifikátoru navrhovatele](#Person.proposerId).
3. Systém smaže:
    - záznam [osoby](#Person),
    - její [identifikátory](#PersonIdentifier),
    - její [alternativní jména](#NaturalPersonAlternativeName).

### Extensions:

- 2a. Osoba s daným navrhovatelem a identifikátorem navrhovatele neexistuje:
    - 2a1. Systém zapíše chybu a případ užití končí.
- 2b. Osoba nemá [stav](#Person.status) Návrh:
    - 2b1. Systém zapíše chybu a případ užití končí.
- 2c. Nakladatel přihlášeného uživatele se neshoduje s [navrhovatelem](#Person.proposer) osoby:
    - 2c1. Systém zapíše chybu a případ užití končí.
- 2d. Pro osobu existuje záznam [příspěvku k titulu](#TitleContribution):
    - 2d1. Systém zapíše chybu a případ užití končí.
