## Entity: Osoba `Person`

### Attributes:

- Zdrojový identifikátor `sourceId` (NK text) - Identifikátor v systému Autority.
- Identifikátory `identifiers` (1:n `#PersonIdentifier`)
- Právní forma `legalForm` (M enum: natural, legal)

### Attributes: Fyzická osoba

- Jméno `firstName` (C text)
- Příjmení `lastName` (C text)
- Datum narození `birthDate` (O date) - Přesnost rok nebo den.
- Datum úmrtí `deathDate` (O date) - Přesnost rok nebo den.
- Biografická poznámka `biographicalNote` (O text)
- Souhlas se zveřejněním `disclosureConsent` (O boolean)
- Alternativní jména fyzické osoby `naturalAlternativeNames` (1:n `#NaturalPersonAlternativeName`)

### Attributes: Právnická osoba

- Název `name` (C text)
- Sídlo `residence` (O text)
- Alternativní názvy právnické osoby `legalAlternativeNames` (1:n `#LegalPersonAlternativeName`)

### Attributes:

- Stav `status` (M enum: proposal, confirmed)
- Navrhovatel `proposer` (n:0..1 `#Publisher`)
- Identifikátor navrhovatele `proposerId` (O text): 1234 - Id v systému navrhovatele.

### Stavový diagram

```plant
[*] --> Proposal: Vytvořit osobu, Zapsat osobu (Nakladatel)
Proposal: Návrh
[*] --> Confirmed: Zapsat osobu (Autority)
Confirmed: Potvrzeno
Proposal --> Confirmed: Zapsat osobu (Autority)
Proposal --> [*]: Smazat osobu (Nakladatel, Systém)
```

### Poznámky

- Navrhovatel vyplněn ve stavu Návrh.
- Identifikátor navrhovatele vyplněn ve stavu Návrh, pokud byl záznam vytvořen nakladatelem prostřednictvím API.
- Pro právní formu Fyzická osoba musí být vyplněno alespoň jedno z polí Jméno, Příjmení.
- Pro právní formu Právnická osoba musí být vyplněno pole Název.
