## UC: `PL40` Změnit právnickou osobu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Změnit právnickou osobu](#/legal-persons/edit)

### Main success scenario:

1. Uživatel najde a vybere právnickou osobu k editaci.
2. Systém zobrazí údaje [osoby](#Person).
3. Uživatel změní údaje osoby.
4. Systém provede formální validace dat.
5. Systém zapíše data z formuláře do:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních názvů](#LegalPersonAlternativeName).
6. Systém [odešle osobu](#PE90).

### Extensions:

- 2a. Osoba nemá [stav](#Person.status) Návrh:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 2b. Nakladatel přihlášeného uživatele se neshoduje s [navrhovatelem](#Person.proposer) osoby:
    - 2b1. Systém zobrazí chybu a případ užití končí.
- 4a. Není vyplněno povinné pole:
    - 4a1. Systém zobrazí chybu.
