## UC: `PL10` Prohlížet právnické osoby

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Právnické osoby](#/legal-persons)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [osoby](#Person), které mají [právní formu](#Person.legalForm) Právnická osoba a
   odpovídají zadaným kritériím, a seřadí je podle:
    - [Název](#Person.name)
    - [Sídlo](#Person.residence)
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybrané [osoby](#Person).
5. Systém najde a zobrazí pro danou osobu:
    - všechny její [identifikátory](#Person.identifiers),
    - všechny její [alternativní názvy](#Person.legalAlternativeNames).
