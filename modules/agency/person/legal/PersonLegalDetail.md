## Screen: Právnická osoba `/legal-persons/detail`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Název (RO): První pražská, sro.
- Sídlo (RO): Praha
- Stav (RO): Návrh
- Navrhovatel (RO): Albatros Media

### Table: Identifikátory

- Typ
    - ISNI
    - ORCID
- Identifikátor
    - [0000000121343289](https://isni.org/isni/0000000121343289)
    - [0000-0002-1825-0097](https://orcid.org/0000-0002-1825-0097)

### Table: Alternativní názvy

- Název
    - Kapřík, sro.

### Form:

- [:Změnit](#/legal-persons/edit)
- [:Nahlásit změnu](#/legal-persons/report-change)
- [:Smazat](#/legal-persons)
- [:Tituly](#/titles)
- [:Zpět](#/legal-persons)
