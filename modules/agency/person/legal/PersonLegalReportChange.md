## Screen: Nahlásit změnu právnické osoby `/legal-persons/report-change`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Název (RO): První pražská, sro.
- Sídlo (RO): Praha
- Návrh změn (R multiLine)
- [primary: Odeslat](#/legal-persons/detail)
- [:Zpět](#/legal-persons/detail)
