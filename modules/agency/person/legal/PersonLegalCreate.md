## Screen: Vytvořit právnickou osobu `/legal-persons/create`

### Form:

- Název (R)
- Sídlo

### Table: Identifikátory

- Typ (R select: ISNI, ORCID)
    - .
- Identifikátor (R)
    - .
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Alternativní názvy

- Název (R)
    - .
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/legal-persons/detail)
- [:Zpět](#/legal-persons)
