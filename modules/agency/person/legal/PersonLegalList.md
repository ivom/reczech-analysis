## Screen: Právnické osoby `/legal-persons`

### Form:

- Hledat - Hledat podle názvu, alternativního názvu nebo sídla.
- Identifikátor - ISNI, ORCID nebo NK ČR AUT
- Stav (select: Potvrzeno, Návrh)
- Navrhovatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)

### Table:

- Název
    - [První pražská, sro.](#/legal-persons/detail)
    - [Druhá pražská, sro.](#/legal-persons/detail)
- Sídlo
    - Praha
- Stav
    - Potvrzeno
    - Návrh

[:Vytvořit](#/legal-persons/create)
