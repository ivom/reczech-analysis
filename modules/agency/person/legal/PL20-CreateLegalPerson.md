## UC: `PL20` Vytvořit právnickou osobu

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Vytvořit právnickou osobu](#/legal-persons/create)

### Main success scenario:

1. Uživatel zadá údaje právnické osoby a vytvoří ji.
2. Systém provede formální validace dat.
3. Systém zapíše data z formuláře do nových záznamů:
    - [osoby](#Person),
    - jejích [identifikátorů](#PersonIdentifier),
    - jejích [alternativních názvů](#LegalPersonAlternativeName).
4. Systém na [osobě](#Person) nastaví:
    - [Zdrojový identifikátor](#Person.sourceId) = `rck` + interní id záznamu
    - [Právní forma](#Person.legalForm) = Právnická osoba
    - [Stav](#Person.status) = Návrh
    - [Navrhovatel](#Person.proposer) = [Nakladatel](#Publisher) přihlášeného uživatele
      (identifikovaný zdrojovým identifikátorem jeho organizace)
    - [Identifikátor navrhovatele](#Person.proposer) = prázdný
5. Systém [odešle osobu](#PE90).

### Extensions:

- 2a. Není vyplněno povinné pole:
    - 2a1. Systém zobrazí chybu.
