## Screen: Změnit právnickou osobu `/legal-persons/edit`

### Form:

- Zdrojový identifikátor (RO): rck23456
- Název (R): První pražská, sro.
- Sídlo: Praha

### Table: Identifikátory

- Typ (R select: ISNI, ORCID)
    - ISNI
    - ORCID
- Identifikátor (R)
    - 0000000121343289
    - 0000-0002-1825-0097
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Alternativní názvy

- Název (R)
    - Kapřík, sro.
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/legal-persons/detail)
- [:Zpět](#/legal-persons/detail)
