## UC: `PL45` Nahlásit změnu právnické osoby

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Nahlásit změnu právnické osoby](#/legal-persons/report-change)

### Main success scenario:

1. Uživatel najde a vybere právnickou osobu.
2. Uživatel zadá údaje hlášení změn právnické osoby.
3. Systém provede formální validace dat.
4. Systém odešle email s:
    - Odesílatel = `recek@nkp.cz`
    - Komu = `autority@nkp.cz`
    - Předmět = "Návrh na změnu osoby " + [Zdrojový identifikátor](#Person.sourceId)
    - Tělo = obsah pole Návrh změn

### Extensions:

- 3a. Není vyplněno povinné pole:
    - 3a1. Systém zobrazí chybu.
