## Entity: Identifikátor osoby `PersonIdentifier`

### Attributes:

- Typ `type` (M enum: isni, orcid)
- Identifikátor `identifier` (M text)

### Constraints

- Unique: `type`, `identifier`
