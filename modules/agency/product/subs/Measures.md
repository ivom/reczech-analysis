## Entity: Rozměry `Measures`

### Attributes:

- Výška `height` (O number) - [mm]
- Šířka `width` (O number) - [mm]
- Tloušťka `thickness` (O number) - [mm]
- Hmotnost `weight` (O number) - [g]
