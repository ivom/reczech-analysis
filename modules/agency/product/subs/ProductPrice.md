## Entity: Cena produktu `ProductPrice`

### Attributes:

- Typ `type` (M `#OnixCodetableItem`): 02 - https://onix-codelists.io/codelist/58
- Částka `amount` (M number): 259.9
- Měna `currency` (M `#OnixCodetableItem`): CZK - https://onix-codelists.io/codelist/96

### Poznámky

- Typ 02 je doporučená cena vč. DPH.
