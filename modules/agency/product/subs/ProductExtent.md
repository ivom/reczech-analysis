## Entity: Rozsah produktu `ProductExtent`

### Attributes:

- Typ `type` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/23
- Hodnota `value` (M number)
- Jednotka `unit` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/24
