## UC: `PD41` Zpřístupnit produkt

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovka: [Produkt](#/products/detail)

### Main success scenario:

1. Uživatel vybere [soukromý](#Product.accessibility) produkt a zpřístupní ho.
2. Systém na produktu nastaví:
    - [Přístupnost](#Product.accessibility) = Veřejný
3. Systém [odešle titul](#TT90) produktu.

### Extensions:

- 2a. Uživatel má pouze roli Garant nakladatele a [titul](#Product.title) vybraného produktu [patří](#Title.owner)
  jinému nakladateli:
    - 2a1. Systém zobrazí chybu a případ užití končí.
