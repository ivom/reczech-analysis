## Entity: Vztah produktů `ProductsRelation`

### Attributes:

- Produkt `product` (n:1 `#Product`)
- Související produkt `releatedProduct` (n:1 `#Product`)
- Typ vztahu `relationType` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/51
