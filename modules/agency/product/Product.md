## Entity: Produkt `Product`

### Attributes:

- Titul `title` (n:1 `#Title`)
- Identifikátor vlastníka `ownerId` (O text): 1234
- Primární typ obsahu `primaryContentType` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/81
- Forma `form` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/150
- Detaily formy `formDetails` (1:n `#ProductFormDetail`)
- Rozměry jednotky `unitMeasures` (1:0..1 `#Measures`)
- Rozměry kartónu `cartonMeasures` (1:0..1 `#Measures`)
- Rozměry palety `palletMeasures` (1:0..1 `#Measures`)
- Rozsahy `extents` (1:n `#ProductExtent`)
- Složení `composition` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/2
- Stav vydání `publishingStatus` (O `#OnixCodetableItem`) - https://onix-codelists.io/codelist/64
- Datum vydání `publishingDate` (O date) - Přesnost rok, měsíc nebo den.
- Cena `price` (1:0..1 `#ProductPrice`)
- Přístupnost `accessibility` (M enum: private, public)
- Čas zpřístupnění `publicTime` (O datetime) - Datum a čas (prvního) dosažení přístupnosti Veřejný

### Poznámky

- Přístupnost
    - Soukromý `private`: přístupný pouze pro pracovníka agentury a garanta vlastníka titulu
    - Veřejný `public`: přístupný pro pracovníka agentury, garanty všech nakladatelů a distributory
- Migrace Času vydání: rok vydání z 008 a datum 1. 1., u seriálů rok zahájení vydávání

### TODO

- Je potřeba definovat kontrolu na duplicitu produktů. Bohužel v této fázi nejsme schopni definovat položky, které
  jednoznačně určují produkt. Cílem je zamezit nakladateli ve vytvoření násobného produktu a následnému násobnému
  přidělení ISBN.
