## Screen: Produkty `/products`

### Form:

- Číslo produktu - S pomlčkami nebo bez.
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- Přispěvatel
- Název
- Přístupnost (select: Soukromý, Veřejný)

### Table:

- Nakladatel
    - Albatros Media
    - Albatros Media
    - Euromedia Group
    - Grada Publishing
- První přispěvatel
    - Němcová Božena
    - Kolínský Adalbert
    - Jirásek Alois
    - Mrštík Vilém
- Hlavní název
    - Babička
    - Čechové na Řípu
    - Psohlavci
    - Rok na vsi
- Datum vydání
    - 2\. 2020
    -
    - 22\. 2. 2022
    - 1990
- Číslo produktu
    - 978-80-12-12345-1
    - 978-80-12-12346-7
    - 978-80-13-12345-4
    - 978-80-14-12345-9
- Primární typ obsahu
    - Text
    - Text
    - Audiokniha
    - Text
- Forma
    - [Pevná vazba](#/products/detail)
    - [Brožováno](#/products/detail)
    - [CD-Audio](#/products/detail)
    - [Knížka do vany](#/products/detail)
- Přístupnost
    - Veřejný
    - Soukromý
    - Veřejný
    - Veřejný
