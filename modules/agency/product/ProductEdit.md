## Screen: Změnit produkt `/products/edit`

### Form:

- Nakladatel (RO): Albatros Media
- První přispěvatel (RO): Božena Němcová
- Titul (RO): Babička

### Form: Produkt

- Primární typ obsahu (R select: Text, Hudební notace, Mapy, Audiokniha): Text
- Forma (R select: Nedefinováno, Audio, Kniha, Pevná vazba): Pevná vazba

### Table: Detaily formy

- Hodnota (R select: A5, Vázáno v kůži, EPUB, PDF, PDF/A, Amazon Kindle, Loutka)
    - A5
    - Vázáno v kůži
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Rozměry

- Balení
    - Jednotka
    - Kartón
    - Paleta
- Výška [mm] (O text)
    - 234
- Šířka [mm] (O text)
    - 168,6
- Tloušťka [mm] (O text)
    - 19,3
- Hmotnost [g] (O text)
    - 197,2

### Table: Rozsahy

- Typ (R select: Počet stran hlavního obsahu, Délka textu, Celkový počet stran, Velikost souboru)
    - Počet stran hlavního obsahu
    - Délka textu
- Hodnota (R text)
    - 286
    - 12 345
- Jednotka (R select: Znak, Slovo, Strana, Byte)
    - Strana
    - Slovo
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Form: Produkt

- Složení (R select: Jednosložkový, Neprodejný, Vícesložkový): Jednosložkový
- Stav vydání (O select: Neuvedeno, Zrušeno, Připravováno, Aktivní, Vyprodáno, Staženo): Připravováno
- Datum vydání (O date): 20. 2. 2020 - S přesností na rok, měsíc nebo den.

> Pokud cena není zadaná:

- FieldSet: Cena
- [:Zadat cenu]()

> Pokud cena je zadaná:

- FieldSet: Cena
    - Typ ceny (R select: Doporučená maloobchodní cena bez daně): Doporučená maloobchodní cena bez daně
    - Cena (R): 459
    - Měna (R select: CZK, EUR, USD, GBP): CZK
    - [:Smazat cenu]()

### Table: Související produkty

- Typ vztahu (R select: Obsahuje, Je součástí, Nahrazuje)
    - Nahrazuje
- Nakladatel
    - Albatros Media
- Titul
    - Babička
- Produkt
    - Pevná vazba
- Číslo produktu (R select: 978-80-12-12345-1, 978-80-12-12346-7)
    - 978-80-12-12345-1
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/products/detail)
- [:Zpět](#/products/detail)
