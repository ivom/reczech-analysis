## UC: `PD25` Přijmout produkt

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Produkt
- Předpoklady:
    - [Nakladatel](#Publisher), který zprávu odeslal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Položky zprávy Produkt se shodují s položkami [entity Produkt](#Product) a podřízených entit (viz krok 3.) s těmito
  výjimkami:
    - [Titul](#Product.title) je identifikován [identifikátorem titulu vlastníka](#Title.ownerId)
    - Tyto položky zpráva neobsahuje:
        - Čas zpřístupnění
    - Tato položka je ve zprávě povinná:
        - Identifikátor vlastníka

### Main success scenario:

1. Systém přijme zprávu o produktu.
2. Systém najde [titul](#Title) podle [vlastníka](#Title.owner) (= nakladatel, který zprávu odeslal)
   a [identifikátoru titulu vlastníka](#Title.ownerId).
3. Systém najde [produkt](#Product) podle [titulu](#Product.title) a [identifikátoru vlastníka](#Product.ownerId).
4. Systém zapíše údaje ze zprávy do:
    - [produktu](#Product),
    - jeho [detailů formy](#ProductFormDetail),
    - jeho [rozměrů jednotky](#Measures),
    - jeho [rozměrů kartónu](#Measures),
    - jeho [rozměrů palety](#Measures),
    - jeho [rozsahů](#ProductExtent),
    - jeho [ceny](#ProductPrice).
5. Systém [přiřadí](#Product.title) produkt nalezenému titulu.

### Extensions:

- 2a. Titul s daným vlastníkem a identifikátorem vlastníka neexistuje:
    - 2a1. Systém zapíše chybu a případ užití končí.
- 3a. Produkt s daným [identifikátorem vlastníka](#Product.ownerId) pro daného [vlastníka titulu](#Title.owner)
  neexistuje:
    - 3a1. Systém založí nový [produkt](#Product) s:
        - [Identifikátor vlastníka](#Product.ownerId) = příchozí
- 4a. [Přístupnost](#Product.accessibility) produktu je veřejná a čas zpřístupnění není nastaven:
    - 4a1. Systém nastaví [Čas zpřístupnění](#Product.publicTime) = aktuální čas.
