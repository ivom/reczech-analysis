## UC: `PD20` Vytvořit produkt

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovka: [Detail titulu](#/titles/detail), [Vytvořit produkt](#/products/create)

### Main success scenario:

1. Uživatel [vybere titul](#TT10).
2. Uživatel zadá údaje produktu a vytvoří ho.
3. Systém provede formální validace dat.
4. Systém zapíše data z formuláře do nových záznamů:
    - [produktu](#Product),
        - jeho [detailů formy](#ProductFormDetail),
        - jeho [rozměrů](#Measures) pro každé vyplněné balení,
        - jeho [rozsahů](#ProductExtent),
        - jeho [ceny](#ProductPrice), pokud je vyplněná,
        - jeho [souvisejících produktů](#ProductsRelation).
5. Systém na produktu nastaví:
    - [Titul](#Product.title) = vybraný titul
    - [Přístupnost](#Product.accessibility) = Soukromý
6. Systém [odešle titul](#TT90).

### Extensions:

- 2a. Uživatel má pouze roli Garant nakladatele a vybraný titul [patří](#Title.owner) jinému nakladateli:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 2b. Uživatel přidá / smaže detail formy / rozsah / související produkt:
    - 2b1. Systém přidá / smaže detail formy / rozsah / související produkt.
- 2c. Uživatel vytvoří / smaže cenu:
    - 2c1. Systém otevře / zavře formulář ceny.
- 2d. Uživatel zadá text pro výběr čísla souvisejícího produktu:
    - 2d1. Systém hledá [produkty](#Product) přístupné uživateli (tj. [veřejné](#Product.accessibility)
      nebo [vlastněné](#Title.owner) nakladatelem uživatele pro roli Garant nakladatele nebo bez omezení pro roli
      Pracovník agentury) podle [hodnoty čísla produktu](#ProductNumber.value).
    - 2d2. Uživatel vybere produkt.
    - 2d3. Systém zobrazí:
        - Nakladatel: [název](#Publisher.name) [vlastníka](#Title.owner) [titulu produktu](#Product.title)
        - Titul: [název](#TitleName.name) [titulu produktu](#Product.title) s typem `01` (hlavní název), pokud existuje,
          nebo první (jakýkoliv)
        - Produkt: [forma](#Product.form) vybraného produktu
- 3a. Není vyplněno povinné pole:
    - 3a1. Systém zobrazí chybu.
