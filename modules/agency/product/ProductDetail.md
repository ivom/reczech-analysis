## Screen: Produkt `/products/detail`

### Form:

- Nakladatel (RO): [Albatros Media](#/publishers/detail)
- Titul (RO): [Božena Němcová: Babička](#/titles/detail)
- Primární typ obsahu (RO): Text
- Forma (RO): Pevná vazba
- Detaily formy (RO): A5, Vázáno v kůži

### Table: Rozměry

- Balení
    - Jednotka
    - Kartón
    - Paleta
- Výška [mm]
    - 234
- Šířka [mm]
    - 168,6
- Tloušťka [mm]
    - 19,3
- Hmotnost [g]
    - 197,2

### Table: Rozsahy

- Typ
    - Počet stran hlavního obsahu
    - Délka textu
- Hodnota
    - 286
    - 12 345
- Jednotka
    - Strana
    - Slovo

### Form:

- Složení (RO): Jednosložkový
- Stav vydání (RO): Připravováno
- Datum vydání (RO): 20. 2. 2020
- Cena (RO): 459 CZK (doporučená maloobchodní cena bez daně)
- Přístupnost (RO): Veřejný

### Table: Související produkty

- Typ vztahu
    - Nahrazuje
- Nakladatel
    - [Albatros Media](#/publishers/detail)
- Titul
    - [Babička](#/titles/detail)
- Produkt
    - [Pevná vazba](#/products/detail)
- Číslo produktu
    - [978-80-12-12346-7](#/product-numbers/detail)

### Form:

- [:Změnit](#/products/edit)
- [warning: Zpřístupnit]()
- [warning: Znepřístupnit]()
- [:Zpět](#/products)

### Table: Čísla produktů

- Číslo produktu
    - [ISBN 978-80-12-12345-1](#/product-numbers/detail)

[:Přidělit](#/product-numbers/assign)
