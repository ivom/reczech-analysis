## Screen: Vytvořit produkt `/products/create`

### Form:

- Nakladatel (RO): Albatros Media
- První přispěvatel (RO): Božena Němcová
- Titul (RO): Babička

### Form: Produkt

- Primární typ obsahu (R select: Text, Hudební notace, Mapy, Audiokniha)
- Forma (R select: Nedefinováno, Audio, Kniha, Pevná vazba)

### Table: Detaily formy

- Hodnota (R select: A5, Vázáno v kůži, EPUB, PDF, PDF/A, Amazon Kindle, Loutka)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Rozměry

- Balení
    - Jednotka
    - Kartón
    - Paleta
- Výška [mm] (O text)
- Šířka [mm] (O text)
- Tloušťka [mm] (O text)
- Hmotnost [g] (O text)

### Table: Rozsahy

- Typ (R select: Počet stran hlavního obsahu, Délka textu, Celkový počet stran, Velikost souboru)
- Hodnota (R text)
- Jednotka (R select: Znak, Slovo, Strana, Byte)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form: Produkt

- Složení (R select: Jednosložkový, Neprodejný, Vícesložkový)
- Stav vydání (O select: Neuvedeno, Zrušeno, Připravováno, Aktivní, Vyprodáno, Staženo)
- Datum vydání (O date) - S přesností na rok, měsíc nebo den.

- FieldSet: Cena
- [:Zadat cenu]()

> Pro zadání ceny:

- FieldSet: Cena
    - Typ ceny (R select: Doporučená maloobchodní cena bez daně)
    - Cena (R)
    - Měna (R select: CZK, EUR, USD, GBP)
    - [:Smazat cenu]()

### Table: Související produkty

- Typ vztahu (R select: Obsahuje, Je součástí, Nahrazuje)
- Nakladatel
- Titul
- Produkt
- Číslo produktu (R select: 978-80-12-12345-1, 978-80-12-12346-7)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- [primary: Uložit](#/products/detail)
- [:Zpět](#/titles/detail)
