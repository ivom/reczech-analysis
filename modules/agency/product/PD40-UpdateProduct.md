## UC: `PD40` Změnit produkt

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovka: [Změnit produkt](#/products/edit)

### Main success scenario:

1. Uživatel najde a vybere produkt k editaci.
2. Systém zobrazí údaje [produktu](#Product).
3. Uživatel změní údaje produktu.
4. Systém provede formální validace dat.
5. Systém zapíše data z formuláře do:
    - [produktu](#Product),
        - jeho [detailů formy](#ProductFormDetail),
        - jeho [rozměrů](#Measures),
        - jeho [rozsahů](#ProductExtent),
        - jeho [ceny](#ProductPrice),
        - jeho [souvisejících produktů](#ProductsRelation).
6. Systém [odešle titul](#TT90).

### Extensions:

- 2a. Uživatel má pouze roli Garant nakladatele a [titul](#Product.title) vybraného produktu [patří](#Title.owner)
  jinému nakladateli:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 3a. Primární typ obsahu je `11` Hudební notace:
    - 3a1. Systém zakáže editovat pole Primární typ obsahu.
- 3b. Primární typ obsahu není `11` Hudební notace:
    - 3b1. Systém povolí editovat pole Primární typ obsahu a vyřadí z něho hodnotu `11` Hudební notace.
- 3c. Uživatel přidá / smaže detail formy / rozsah / související produkt:
    - 3c1. Systém přidá / smaže detail formy / rozsah / související produkt.
- 3d. Uživatel vytvoří / smaže cenu:
    - 3d1. Systém otevře / zavře formulář ceny.
- 3e. Uživatel zadá text pro výběr čísla souvisejícího produktu:
    - 3e1. Systém hledá [produkty](#Product) přístupné uživateli (tj. [veřejné](#Product.accessibility)
      nebo [vlastněné](#Title.owner) nakladatelem uživatele pro roli Garant nakladatele nebo bez omezení pro roli
      Pracovník agentury) podle [hodnoty čísla produktu](#ProductNumber.value).
    - 3e2. Uživatel vybere produkt.
    - 3e3. Systém zobrazí:
        - Nakladatel: [název](#Publisher.name) [vlastníka](#Title.owner) [titulu produktu](#Product.title)
        - Titul: [název](#TitleName.name) [titulu produktu](#Product.title) s typem `01` (hlavní název), pokud existuje,
          nebo první (jakýkoliv)
        - Produkt: [forma](#Product.form) vybraného produktu
- 4a. Není vyplněno povinné pole:
    - 4a1. Systém zobrazí chybu.
