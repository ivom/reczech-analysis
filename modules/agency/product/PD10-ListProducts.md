## UC: `PD10` Prohlížet produkty

- Primární aktér: [Pracovník agentury](#Agent), [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Produkty](#/products)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
    - Číslo produktu: hledá v poli [hodnota](#ProductNumber.value) podřízených záznamů [čísla produktu](#ProductNumber)
    - Nakladatel: hledá v [názvu nakladatele](#Publisher.name) propojeného přes [titul produktu](#Product.title)
      a [nakladatele titulu](#TitlePublisher)
    - Přispěvatel: hledá v polích [Příjmení](#TitleContribution.lastName) a [Název organizace](#TitleContribution.name)
      nadřízeného [titulu produktu](#Product.title)
    - Název: hledá v polích [Název](#TitleName.name), [Název části](#TitleName.partName)
      a [Podnázev](#TitleName.subtitle) nadřízeného [titulu produktu](#Product.title)
2. Systém najde záznamy [produktu](#Product), které odpovídají zadaným kritériím, a zobrazí:
    - Nakladatel: [název](#Publisher.name) [vlastníka](#Title.owner) [titulu produktu](#Product.title)
    - První přispěvatel: [Příjmení](#TitleContribution.lastName) a [Jméno](#TitleContribution.firstName)
      nebo [Název organizace](#TitleContribution.name) nebo [Nespecifikovaná osoba](#TitleContribution.unnamedPerson)
      příspěvku k [titulu produktu](#Product.title) s [pořadím](#TitleContribution.sequenceNumber) 1
    - Hlavní název: [název](#TitleName.name) [titulu produktu](#Product.title) s typem `01` (hlavní název), pokud
      existuje, nebo první (jakýkoliv)
    - Číslo produktu: [hodnota](#ProductNumber.value) prvního (jakéhokoliv) [čísla produktu](#ProductNumber)
3. Systém seřadí záznamy podle
    - Nakladatel
    - První přispěvatel
    - Hlavní název
    - Datum vydání
    - Číslo produktu
4. Uživatel vybere záznam.
5. Systém zobrazí detail vybraného [produktu](#Product).
6. Systém najde a zobrazí pro daný produkt:
    - všechny jeho [detaily formy](#ProductFormDetail),
    - všechny jeho [rozměr](#Measures),
    - všechny jeho [rozsahy](#ProductExtent),
    - všechny jeho [cenu](#ProductPrice),
    - všechny jeho [související produkty](#ProductsRelation).
7. Systém najde a zobrazí pro daný titul všechny jeho [čísla produktů](#ProductNumber).

### Extensions:

- 1a. Uživatel má roli Garant nakladatele:
    - 1a1. Systém předvyplní výběrové kritérium na nakladatele přihlášeného uživatele.
- 2a. Uživatel má roli Distributor:
    - 2a1. Systém vynechá [neveřejné](#Product.accessibility) produkty.
- 2b. Uživatel má roli Garant nakladatele:
    - 2b1. Systém vynechá [neveřejné](#Product.accessibility) produkty, jejichž [titul je vlastněný](#Title.owner) jiným
      nakladatelem.
