## UC: `AP10` Prohlížet prefixy agentury

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Prefixy agentury](#/agency-prefixes)

### Main success scenario:

1. Systém zobrazí všechny záznamy prefixů agentury řazené
   podle [agentury](#AgencyPrefix.agency), [hodnoty](#AgencyPrefix.value).

### Extensions:

- 1a. Uživatel zadá výběrová kritéria:
    - 1a1. Systém zobrazí odpovídající záznamy.
