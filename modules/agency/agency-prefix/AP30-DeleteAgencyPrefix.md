## UC: `AP30` Smazat prefix agentury

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Prefix agentury](#/agency-prefixes/detail)

### Main success scenario:

1. Uživatel vybere prefix agentury a smaže ho.
2. Systém smaže vybraný záznam [prefixu agentury](#AgencyPrefix).

### Extensions:

- 2a. Pro prefix agentury existuje záznam [rozsahu prefixů nakladatele](#PublisherPrefixRange):
    - 2a1. Systém zobrazí chybu "Existují podřízené záznamy."
