## UC: `AP20` Vytvořit prefix agentury

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Vytvořit prefix agentury](#/agency-prefixes/create)

### Main success scenario:

1. Uživatel zadá údaje prefixu agentury a vytvoří prefix agentury.
2. Systém vytvoří nový záznam [prefixu agentury](#AgencyPrefix).

### Extensions:

- 2a. Záznam s identickými hodnotami již existuje:
    - 2a1. Systém zobrazí chybu "Duplicitní záznam."
