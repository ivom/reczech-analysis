## Screen: Prefixy agentury `/agency-prefixes`

### Form:

- Agentura (select: ISBN, ISMN)

### Table:

- Agentura
    - [ISBN](#/agency-prefixes/detail)
    - [ISMN](#/agency-prefixes/detail)
- Hodnota
    - 80

[:Vytvořit](#/agency-prefixes/create)
