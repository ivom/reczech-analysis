## Entity: Prefix agentury `AgencyPrefix`

### Attributes:

- Agentura `agency` (M enum: ISBN, ISMN): ISBN
- Hodnota `value` (O text): 80

### Constraints

- Unique: `agency`, `value`

### Záznamy

- Agentura ISBN, hodnota: 80
- Agentura ISMN, bez hodnoty
