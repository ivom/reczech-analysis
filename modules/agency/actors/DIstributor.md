## Actor: Distributor

### Cíle

- Osoba
    - Fyzická osoba
        - [Prohlížet fyzické osoby](#PE10)
    - Právnická osoba
        - [Prohlížet právnické osoby](#PL10)
- Řada
    - [Prohlížet řady](#CL10)
- Titul
    - [Prohlížet tituly](#TT10)
- Produkt
    - [Prohlížet produkty](#PD10)
- Číslo produktu
    - [Prohlížet čísla produktů](#PN10)
