## Actor: Garant nakladatele `PublisherGuarantor`

### Cíle

- Prefix nakladatele
    - [Prohlížet prefixy nakladatele (garant)](#PP11)
- Osoba
    - Fyzická osoba
        - [Prohlížet fyzické osoby](#PE10)
        - [Vytvořit fyzickou osobu](#PE20)
        - [Smazat fyzickou osobu](#PE30)
        - [Změnit fyzickou osobu](#PE40)
        - [Nahlásit změnu fyzické osoby](#PE45)
    - Právnická osoba
        - [Prohlížet právnické osoby](#PL10)
        - [Vytvořit právnickou osobu](#PL20)
        - [Smazat právnickou osobu](#PL30)
        - [Změnit právnickou osobu](#PL40)
        - [Nahlásit změnu právnické osoby](#PL45)
- Řada
    - [Prohlížet řady](#CL10)
    - [Vytvořit řadu](#CL20)
    - [Změnit řadu](#CL40)
- Titul
    - [Prohlížet tituly](#TT10)
    - [Vytvořit titul](#TT20)
    - [Změnit titul](#TT40)
- Produkt
    - [Prohlížet produkty](#PD10)
    - [Vytvořit produkt](#PD20)
    - [Změnit produkt](#PD40)
    - [Zpřístupnit produkt](#PD41)
    - [Znepřístupnit produkt](#PD42)
- Číslo produktu
    - [Prohlížet čísla produktů](#PN10)
    - [Přidělit číslo produktu](#PN20)
