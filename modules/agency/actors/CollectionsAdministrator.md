## Actor: Správce řad `CollectionsAdministrator`

### Cíle

- Řada
    - [Prohlížet řady](#CL10)
    - [Vytvořit řadu](#CL20)
    - [Změnit řadu](#CL40)
