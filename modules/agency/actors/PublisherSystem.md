## Actor: Systém nakladatele `PublisherSystem`

### Cíle

- Osoba
    - Odeslat osobu nakladatele - [Přijmout osobu nakladatele](#PE25)
    - Odeslat smazání osoby nakladatele - [Přijmout smazání osoby nakladatele](#PE35)
- Řada
    - Odeslat řadu - [Přijmout řadu](#CL25)
- Titul
    - Odeslat titul - [Přijmout titul](#TT25)
- Produkt
    - Odeslat produkt - [Přijmout produkt](#PD25)
- Číslo produktu
    - [Požádat o číslo produktu](#PN25)
