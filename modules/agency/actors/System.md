## Actor: Systém `System`

### Cíle

- Nakladatel
    - [Přijmout nakladatele](#APB20)
- Osoba
    - [Přijmout osobu nakladatele](#PE25)
    - [Přijmout smazání osoby nakladatele](#PE35)
    - [Přijmout osobu Autorit](#PE26)
- Řada
    - [Přijmout řadu](#CL25)
- Titul
    - [Přijmout titul](#TT25)
- Produkt
    - [Přijmout produkt](#PD25)
- Uživatel
    - Přijmout uživatele
