## Actor: Správce číselníků Onix `OnixCodetablesAdministrator`

### Cíle

- Číselník Onix
    - [Prohlížet číselníky Onix](#OC10)
    - [Vytvořit číselník Onix](#OC20)
    - [Změnit číselník Onix](#OC40)
- Položka číselníku Onix
    - [Prohlížet položky číselníku Onix](#OI10)
    - [Vytvořit položku číselníku Onix](#OI20)
    - [Změnit položku číselníku Onix](#OI40)
