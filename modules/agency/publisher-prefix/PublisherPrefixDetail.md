## Screen: Prefix nakladatele `/publisher-prefixes/detail`

### Form:

- Prefix agentury (RO): [ISBN 80](#/agency-prefixes/detail)
- Rozsah prefixů nakladatele (RO): [ISBN 80 00-19](#/publisher-prefix-ranges/detail)
- Nakladatel (RO): [Albatros Media](#/publishers/detail)
- Hodnota (RO): 80-12
- Stav (RO): Aktivní
- Následující sekvence (RO): 123 456
- [:Změnit](#/publisher-prefixes/edit)
- [:Zpět](#/agent/publisher-prefixes)
