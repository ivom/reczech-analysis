## UC: `PP10` Prohlížet prefixy nakladatele (pracovník agentury)

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Prefixy nakladatele (pracovník agentury)](#/agent/publisher-prefixes)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [prefixů nakladatele](#PublisherPrefix), které odpovídají zadaným kritériím, a seřadí
   je podle:
    - [Stav](#PublisherPrefix.status) v pořadí: aktivní, zavřené.
    - [Rozsah prefixů nakladatele](#PublisherPrefix.publisherPrefixRange)
    - [Hodnota](#PublisherPrefix.value)
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybraného [prefixu nakladatele](#PublisherPrefix).
