## UC: `PP40` Změnit prefix nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Změnit prefix nakladatele](#/publisher-prefixes/edit)

### Main success scenario:

1. Uživatel vybere prefix nakladatele a změní ho.
2. Systém uloží změny do [prefixu nakladatele](#PublisherPrefix):
    - [Nakladatel](#PublisherPrefix.publisher)
