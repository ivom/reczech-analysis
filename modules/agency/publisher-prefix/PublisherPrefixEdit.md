## Screen: Změnit prefix nakladatele `/publisher-prefixes/edit`

### Form:

- Hodnota (RO): 80-12
- Nakladatel (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA): Albatros Media
- [primary: Uložit](#/publisher-prefixes/detail)
- [:Zpět](#/publisher-prefixes/detail)
