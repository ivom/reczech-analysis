## UC: `PP60` Sestavit plný prefix nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Prefix agentury](#AgencyPrefix)
    - Hodnota prefixu nakladatele

### Main success scenario:

1. Systém načte [hodnotu prefixu agentury](#AgencyPrefix.value).
2. Systém sestaví plný prefix nakladatele takto:
    - (Prefix agentury)(Prefix nakladatele)
