## UC: `PP11` Prohlížet prefixy nakladatele (garant)

- Primární aktér: [Garant nakladatele](#PublisherGuarantor)
- Obrazovka: [Prefixy nakladatele (garant)](#/publisher/publisher-prefixes)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [prefixů nakladatele](#PublisherPrefix), které odpovídají zadaným kritériím, a seřadí
   je podle:
    - [Stav](#PublisherPrefix.status) v pořadí: aktivní, zavřené.
    - [Rozsah prefixů nakladatele](#PublisherPrefix.publisherPrefixRange)
    - [Hodnota](#PublisherPrefix.value)
