## Entity: Prefix nakladatele `PublisherPrefix`

### Attributes:

- Rozsah prefixů nakladatele `publisherPrefixRange` (n:1 `#PublisherPrefixRange`)
- Nakladatel `publisher` (n:1 `#Publisher`)
- Hodnota `value` (M text): 85 678
- Stav `status` (M enum: active, closed)
- Následující sekvence `nextSequence` (M integer): 23 - Následující sekvence k přidělení.

### Stavový diagram

```plant
[*] --> Active: Create (Vytvořit)
Active: Aktivní
Active --> Closed: Close (Zavřít)
Closed: Zavřený
```
