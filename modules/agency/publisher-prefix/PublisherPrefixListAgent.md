## Screen: Prefixy nakladatele (pracovník agentury) `/agent/publisher-prefixes`

### Form:

- Prefix agentury (select: ISBN 80)
- Rozsah prefixů nakl. (select: ISBN 80 00-19, ISBN 80 200-699, ISBN 80 85 000-89 000)
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- Hodnota
- Zobrazit zavřené (checkbox)

### Table:

- Prefix agentury
    - ISBN 80
    - ISBN 80
    - ISBN 80
    - ISBN 80
    - ISBN 80
- Rozsah prefixů nakladatele
    - 00 - 19
    - 00 - 19
    - 200 - 699
    - 200 - 699
    - 85 000 - 89 999
- Hodnota
    - [12](#/publisher-prefixes/detail)
    - [17](#/publisher-prefixes/detail)
    - [243](#/publisher-prefixes/detail)
    - [567](#/publisher-prefixes/detail)
    - [88 808](#/publisher-prefixes/detail)
- Nakladatel
    - Albatros Media
    - Albatros Media
    - Euromedia Group
    - Grada Publishing
    - Moravská Bastei MOBA
- Stav
    - Aktivní
    - Aktivní
    - Aktivní
    - Aktivní
    - Zavřený
- Následující sekvence
    - 000 001
    - 123 456
    - 98 765
    - 00 001
    - 34
