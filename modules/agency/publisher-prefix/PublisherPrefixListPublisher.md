## Screen: Prefixy nakladatele (garant) `/publisher/publisher-prefixes`

### Form:

- Hodnota
- Zobrazit zavřené (checkbox)

### Table:

- Prefix agentury
    - ISBN 80
    - ISBN 80
    - ISBN 80
    - ISBN 80
    - ISBN 80
- Rozsah prefixů nakladatele
    - 00 - 19
    - 00 - 19
    - 200 - 699
    - 200 - 699
    - 85 000 - 89 999
- Hodnota
    - 12
    - 17
    - 243
    - 567
    - 88 808
- Stav
    - Aktivní
    - Aktivní
    - Aktivní
    - Aktivní
    - Zavřený
- Následující sekvence
    - 000 001
    - 123 456
    - 98 765
    - 00 001
    - 34
