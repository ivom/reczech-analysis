## UC: `PP20` Vytvořit prefix nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Nakladatel](#Publisher)
    - Agentura: ISBN, ISMN
- Obrazovka: [Přidělit číslo produktu](#/product-numbers/assign)

### Main success scenario:

1. Systém zjistí nejvyšší řád prefixu, viz poznámka.
2. Systém najde [aktivní](#PublisherPrefixRange.status) [rozsahy prefixů nakladatele](#PublisherPrefixRange), které
   spadají pod nějaký [prefix agentury](#AgencyPrefix) dané agentury a mají
   zvolený [řád](#PublisherPrefixRange.magnitude).
3. Systém vybere ten rozsah prefixů nakladatele, ve kterém zbývá nejméně prefixů
   (tj. [konec](#PublisherPrefixRange.end) - [následující sekvence](#PublisherPrefixRange.nextSequence) je nejmenší).
4. Systém vytvoří nový záznam [prefixu nakladatele](#PublisherPrefix) a nastaví na něm:
    - [Rozsah prefixů nakladatele](#PublisherPrefix.publisherPrefixRange) = vybraný
    - [Nakladatel](#PublisherPrefix.publisher) = obdržený
    - [Hodnota](#PublisherPrefix.value) = [Následující sekvence](#PublisherPrefixRange.nextSequence) z vybraného rozsahu
      prefixů nakladatele
    - [Stav](#PublisherPrefix.status) = Aktivní
    - [Následující sekvence](#PublisherPrefix.nextSequence) = 0
5. Systém povýší o jedničku [následující sekvenci](#PublisherPrefixRange.nextSequence) vybraného rozsahu prefixů
   nakladatele.

### Extensions:

- 1a. Nejvyšší řád prefixu je alespoň 2:
    - 1a1. Systém zobrazí pole Velikost nového prefixu.
    - 1a2. Uživatel vybere řád prefixu od 1 do nejvyššího řádu prefixu.
- 5a. Rozsah prefixů nakladatele se vyčerpal, tj. [následující sekvence](#PublisherPrefixRange.nextSequence) po navýšení
  překročila [konec](#PublisherPrefixRange.end):
    - 5a1. Systém zavře rozsah prefixů nakladatele, nastaví [stav](#PublisherPrefixRange.status) = Zavřený.

### Poznámky

- Nejvyšší řád prefixu podle počtu vydaných produktů za poslední rok i za rok předchozí:
    - 20 000 produktů: řád 5 (100 000 produktů)
    - 2 000 produktů: řád 4 (10 000 produktů)
    - 200 produktů: řád 3 (1 000 produktů)
    - 20 produktů: řád 2 (100 produktů)
    - méně: řád 1 (10 produktů)
- Posledním rokem je míněno období, které končí včerejším dnem a trvalo 365 dní.
- Předchozím rokem je míněno období o délce 365 dní, které přímo předchází poslednímu roku.
- Produkt se započítá do daného roku, pokud tam spadá jeho [čas zpřístupnění](#Product.publicTime) a
  je [veřejně přístupný](#Product.accessibility).
