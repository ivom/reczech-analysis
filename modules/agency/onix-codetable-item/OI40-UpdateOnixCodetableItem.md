## UC: `OI40` Změnit položku číselníku Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Položky číselníku Onix](#/onix-codetable-items)

### Main success scenario:

1. Uživatel [zobrazí položky číselníku Onix](#OI10).
2. Uživatel najde položku číselníku Onix.
3. Uživatel změní údaje položky.
4. Systém zapíše nové údaje do záznamu [položky číselníku Onix](#OnixCodetableItem).

### Extensions:

- 4a. Záznam s novou [hodnotou](#OnixCodetableItem.value) pro daný číselník Onix již existuje:
    - 4a1. Systém zobrazí chybu "Duplicitní záznam."
