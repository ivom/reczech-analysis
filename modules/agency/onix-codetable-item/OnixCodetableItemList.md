## Screen: Položky číselníku Onix `/onix-codetable-items`

### Form:

- Číselník Onix (R select: 2 - Složení produktu, 15 - Typ názvu, 17 - Role přispěvatele, 19 - Nespecifikovaná osoba)

### Table:

- Hodnota (R)
    - 00
    - 01
    - 03
    - 05
- Popis (R)
    - Nedefinováno
    - Hlavní název
    - Název v původním jazyce
    - Zkrácený název
- Poznámky (O text)
    - .
    - Plný text hlavního názvu, bez zkratek a zkracování. U knih obvykle z titulní strany...
    - Pro překlady
    - Zkrácená forma názvu 01

[:Přidat]()
