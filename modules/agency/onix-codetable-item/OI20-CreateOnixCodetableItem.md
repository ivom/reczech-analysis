## UC: `OI20` Vytvořit položku číselníku Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Položky číselníku Onix](#/onix-codetable-items)

### Main success scenario:

1. Uživatel [zobrazí položky číselníku Onix](#OI10).
2. Uživatel přidá novou položku.
3. Uživatel zadá údaje položky.
4. Systém vytvoří nový záznam [položky číselníku Onix](#OnixCodetableItem).

### Extensions:

- 4a. Záznam s danou [hodnotou](#OnixCodetableItem.value) pro daný číselník Onix již existuje:
    - 4a1. Systém zobrazí chybu "Duplicitní záznam."
