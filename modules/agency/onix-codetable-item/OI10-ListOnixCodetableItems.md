## UC: `OI10` Prohlížet položky číselníku Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Položky číselníku Onix](#/onix-codetable-items)

### Main success scenario:

1. Uživatel vybere číselník Onix.
2. Systém najde [číselník Onix](#OnixCodetable) a načte a zobrazí všechny [položky číselníku Onix](#OnixCodetableItem)
   řazené podle [hodnoty](#OnixCodetableItem.value).
