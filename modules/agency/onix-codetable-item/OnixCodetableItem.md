## Entity: Položka číselníku Onix `OnixCodetableItem`

### Attributes:

- Číselník Onix `onixCodetable` (n:1 `#OnixCodetable`)
- Hodnota `value` (M text): 05
- Popis `description` (M text): Zkrácený název
- Poznámky `notes` (O text): Zkrácená forma názvu 01

### Constraints

- Unique: `onixCodetable`, `value`
