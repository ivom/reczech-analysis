## UC: `OC20` Vytvořit číselník Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Číselníky Onix](#/onix-codetables)

### Main success scenario:

1. Uživatel přidá nový číselník Onix.
2. Uživatel zadá údaje číselníku Onix.
3. Systém vytvoří nový záznam [číselníku Onix](#OnixCodetable).

### Extensions:

- 3a. Záznam s daným [číslem](#OnixCodetable.number) nebo [názvem](#OnixCodetable.name) již existuje:
    - 3a1. Systém zobrazí chybu "Duplicitní záznam."
