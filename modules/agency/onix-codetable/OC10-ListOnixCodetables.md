## UC: `OC10` Prohlížet číselníky Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Číselníky Onix](#/onix-codetables)

### Main success scenario:

1. Systém zobrazí všechny záznamy číselníků Onix řazené podle [čísla](#OnixCodetable.number).

### Extensions:

- 1a. Uživatel zadá kritéria hledání (viz obrazovka):
    - 1a1. Systém najde a zobrazí záznamy, které odpovídají zadaným kritériím.
