## Screen: Číselníky Onix `/onix-codetables`

### Form:

- Číslo (select: 2, 15, 17, 19)
- Název (select: Nespecifikovaná osoba, Role přispěvatele, Složení produktu, Typ názvu)

### Table:

- Číslo (R)
    - 2
    - 15
    - 17
    - 19
- Název (R)
    - Složení produktu
    - Typ názvu
    - Role přispěvatele
    - Nespecifikovaná osoba
- Položky
    - [:Položky](#/onix-codetable-items)
    - [:Položky](#/onix-codetable-items)
    - [:Položky](#/onix-codetable-items)
    - [:Položky](#/onix-codetable-items)

[:Přidat]()
