## UC: `OC40` Změnit číselník Onix

- Primární aktér: [Správce číselníků Onix](#OnixCodetablesAdministrator)
- Obrazovka: [Číselníky Onix](#/onix-codetables)

### Main success scenario:

1. Uživatel najde číselník Onix.
2. Uživatel změní údaje číselníku Onix.
3. Systém zapíše nové údaje do záznamu [číselníku Onix](#OnixCodetable).

### Extensions:

- 3a. Záznam s novým [číslem](#OnixCodetable.number) nebo [názvem](#OnixCodetable.name) již existuje:
    - 3a1. Systém zobrazí chybu "Duplicitní záznam."
