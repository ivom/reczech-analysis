## Entity: Členství `Membership`

### Attributes:

- Nakladatel `publisher` (n:1 `#Publisher`)
- Agentura `agency` (M enum: ISBN, ISMN): ISBN
- Čas zahájení `startTime` (M datetime)
