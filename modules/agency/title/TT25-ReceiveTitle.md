## UC: `TT25` Přijmout titul

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Titul
- Předpoklady:
    - [Nakladatel](#Publisher), který zprávu odeslal (identifikovaný zdrojovým identifikátorem jeho organizace, buď
      prostřednictvím přihlášeného uživatele nebo API klíče)
- Položky zprávy Titul se shodují s položkami [entity Titul](#Title) a podřízených entit (viz krok 3.) s těmito
  výjimkami:
    - Tyto položky zpráva neobsahuje:
        - Vlastník
    - Tato položka je ve zprávě povinná:
        - Identifikátor vlastníka
    - [Řada titulu](#TitleCollection.collection) je identifikována [zdrojovým identifikátorem](#Collection.sourceId)
    - [Osoba přispěvatele](#TitleContribution.person) je identifikována [zdrojovým identifikátorem](#Person.sourceId)
    - Záznamy nakladatele titulu neobsahují identifikaci nakladatele a všechny jsou vztaženy k nakladateli, který zprávu
      odeslal

### Main success scenario:

1. Systém přijme zprávu o titulu.
2. Systém najde [titul](#Title) podle [vlastníka](#Title.owner) (= nakladatel, který zprávu odeslal)
   a [identifikátoru vlastníka](#Title.ownerId).
3. Systém zapíše údaje ze zprávy do:
    - [titulu](#Title),
    - jeho [názvů](#TitleName),
    - jeho [jazyků](#TitleLanguage),
    - jeho [témat](#TitleSubject),
    - jeho [obsahů](#TitleTextContent),
    - jeho [zdrojů](#TitleSupportingResource),
    - jeho [míst vydání](#TitlePublicationPlace),
    - jeho [uživatelských určení](#TitleAudience),
    - jeho [řad titulu](#TitleCollection),
    - jeho [příspěvků k titulu](#TitleContribution),
    - jeho [nakladatelů titulu](#TitlePublisher).
4. Systém [odešle titul](#TT90).

### Extensions:

- 2a. Titul s daným vlastníkem a identifikátorem vlastníka neexistuje:
    - 2a1. Systém založí nový [titul](#Title) s:
        - [Identifikátor vlastníka](#Title.ownerId) = příchozí
        - [Vlastník](#Title.owner) = [Nakladatel](#Publisher), který zprávu odeslal
