## Entity: Řada titulu `TitleCollection`

### Attributes:

- Titul `title` (n:1 `#Title`)
- Řada `collection` (n:1 `#Collection`)
- Číslo části `partNumber` (O text)
