## Screen: Změnit titul `/titles/edit`

### Božena Němcová: Babička

### Form:

- Vlastník (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA): Albatros Media

### Table: Názvy

- Typ
    - Hlavní název
    - Název v jiném jazyce
- Název
    - Babička
    - The Grandmother
- Prefix
    - .
    - The
- Název bez prefixu
    - .
    - Grandmother
- Číslo části
- Název části
- Podnázev
    - Obrazy venkovského života
- Změnit
    - [:Změnit]()
    - [:Změnit]()
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Form: Název

> Formulář zobrazí k editaci jeden řádek z tabulky výše po stisku tl. Změnit, případně nový řádek po stisku tl. Přidat.

- Typ (R select: Hlavní název, Název v jiném jazyce, Název v původním jazyce): Hlavní název
- Název (R): Babička
- Prefix (O)
- Název bez prefixu (O)
- Číslo části (O)
- Název části (O)
- Podnázev (O): Obrazy venkovského života
- [:Zavřít]()

> Tl. Zavřít zapíše změny do (nového) řádku v tabulce výše.

### Table: Příspěvky

- Pořadí
    - 1
    - 2
    - 3
    - 4
- Přispěvatel
    - Božena Němcová
    - Eliška Kutnohorská
    -
    - Nadace Terezy Maxové
- Role
    - Autor
    - Námět, Spoluautor, Předmluva, Doslov
    - Ilustrátor
    - Vydavatel
- Jméno
    - Božena
    - Eliška
- Příjmení
    - Němcová
    - Kutnohorská
- Biografická poznámka
    - Božena Němcová se narodila...
- Název organizace
    - .
    -
    -
    - Nadace Terezy Maxové
- Nespecifikovaná osoba
    - .
    -
    - AI
- Změnit
    - [:Změnit]()
    - [:Změnit]()
    - [:Změnit]()
    - [:Změnit]()
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Form: Přispěvatel

> Formulář zobrazí k editaci jeden řádek z tabulky výše po stisku tl. Změnit, případně nový řádek po stisku tl. Přidat.

- Osoba (R select: Adam Robert, Burian Zdeněk, Kutnohorská Eliška, Němcová Božena): Němcová Božena
- Jméno: Božena
- Příjmení: Němcová
- Biografická poznámka (multiLine): Božena Němcová se narodila 4. února 1820 ve Vídni.
- Název organizace
- Nespecifikovaná osoba (select: Anonym, et al, Různí, AI)
- FieldSet: Role
    - FieldSet:
        - Role (R select: Autor, Námět, Spoluautor, Předmluva, Doslov, Ilustrátor, Editor): Autor
        - [:Smazat]()
    - [:Přidat]()
- [:Zavřít]()

> Tl. Zavřít zapíše změny do (nového) řádku v tabulce výše.

### Table: Nakladatelé

- Nakladatel (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
    - Albatros Media
- Role (R select: Nakladatel, Spolunakladatel)
    - Nakladatel
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Místa vydání

- Obec (R)
    - Praha
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Řady

- Název (R select: Národní obrození)
    - Národní obrození
- Typ (RO)
    - Edice
- Číslo části (O)
    - Svazek 1.
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Jazyky

- Role (R select: Jazyk textu, Původní jazyk překladu, Jazyk abstrakt, Jazyk titulků)
    - Jazyk textu
- Jazyk (R select: Čeština, Slovenština, Angličtina, Němčina)
    - Čeština
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Témata

- Hlavní téma (O checkbox)
    - true
- Schéma (R select: Předmětová kategorie Thema, MDT, Klíčová slova, BISAC téma)
    - Předmětová kategorie Thema
    - Předmětová kategorie Thema
    - Klíčová slova
- Kód (O select: FBC - Klasická beletrie, YFA - Beletrie pro děti a mládež - klasická beletrie)
    - FBC - Klasická beletrie
    - YFA - Beletrie pro děti a mládež - klasická beletrie
- Text (O text)
    - .
    -
    - Česká próza
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Obsahy

- Typ (R select: Krátký popis, Popis, Obsah, Primární popis na obálce, Doporučení)
    - Popis
    - Primární popis na obálce
- Text (R multiLine)
    - Babička je nejznámější dílo Boženy Němcové, poprvé vydané roku 1855...
    - Stěžejní dílo Boženy Němcové, které autorka psala od konce roku...
- Jazyk (O select: Čeština, Slovenština, Angličtina, Němčina)
    - Čeština
    - Čeština
- Autor textu (O)
    - Jaroslav Pospíšil
    - .
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Zdroje

- Typ (R select: Přední obálka, Fotografie přispěvatele, Ilustrace, Plný text)
    - Přední obálka
    - Fotografie přispěvatele
    - Ilustrace
    - Plný text
- Druh (R select: Aplikace, Audio, Obrázek, Text)
    - Obrázek
    - Obrázek
    - Obrázek
    - Text
- Odkaz (R)
    - https://upload.wikimedia.org/wikipedia/commons/e/e2/Babi%C4%8Dka_%281855%29.jpg
    - https://upload.wikimedia.org/wikipedia/commons/1/16/Jan_Vil%C3%ADmek_-_Bo%C5%BEena_N%C4%9Bmcov%C3%A1.jpg
    - https://upload.wikimedia.org/wikipedia/commons/c/c8/Babicka-1924-A.jpg
    - https://cs.wikisource.org/wiki/Babi%C4%8Dka
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Table: Uživatelská určení

- Typ (R select: Děti, Dospívající, Základní a střední škola, Školka, První stupeň, Druhý stupeň, Střední škola)
    - Děti
    - Základní a střední škola
- Smazat
    - [outline-danger: X]()
    - [outline-danger: X]()

[:Přidat]()

### Form:

- Věk od: 8
- Věk do: 12
- DOI: https://doi.org/10.1000/182
- Pořadí vydání (R): 3
- Poznámka k vydání
- Poznámky k ilustracím: barevné ilustrace, portréty
- Značka: Knižní klub
- Copyright od: 2010
- Copyright do: 2030
- Poznámka pro agenturu (multiLine)
- [primary: Uložit](#/titles/detail)
- [:Zpět](#/titles/detail)
