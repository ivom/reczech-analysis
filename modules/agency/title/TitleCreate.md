## Screen: Vytvořit titul `/titles/create`

### Form:

- Vlastník (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)

### Table: Názvy

- Typ
- Název
- Prefix
- Název bez prefixu
- Číslo části
- Název části
- Podnázev
- Změnit
    - [:Změnit]()
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form: Název

> Formulář zobrazí k editaci jeden řádek z tabulky výše po stisku tl. Změnit, případně nový řádek po stisku tl. Přidat.

- Typ (R select: Hlavní název, Název v jiném jazyce, Název v původním jazyce)
- Název (R)
- Prefix (O)
- Název bez prefixu (O)
- Číslo části (O)
- Název části (O)
- Podnázev (O)
- [:Zavřít]()

> Tl. Zavřít zapíše změny do (nového) řádku v tabulce výše.

### Table: Příspěvky

- Pořadí
- Přispěvatel
- Role
- Jméno
- Příjmení
- Biografická poznámka
- Název organizace
- Nespecifikovaná osoba
- Změnit
    - [:Změnit]()
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form: Příspěvek

> Formulář zobrazí k editaci jeden řádek z tabulky výše po stisku tl. Změnit, případně nový řádek po stisku tl. Přidat.

- Osoba (R select: Adam Robert, Burian Zdeněk, Kutnohorská Eliška, Němcová Božena)
- Jméno
- Příjmení
- Biografická poznámka (multiLine)
- Název organizace
- Nespecifikovaná osoba (select: Anonym, et al, Různí, AI)
- FieldSet: Role
    - FieldSet:
        - Role (R select: Autor, Námět, Spoluautor, Předmluva, Doslov, Ilustrátor, Editor)
        - [:Smazat]()
    - [:Přidat]()
- [:Zavřít]()

> Tl. Zavřít zapíše změny do (nového) řádku v tabulce výše.

### Table: Nakladatelé

- Nakladatel (R select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)
- Role (R select: Nakladatel, Spolunakladatel)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Místa vydání

- Obec (R text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Řady

- Název (R select: Národní obrození)
- Typ (RO)
- Číslo části (O text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Jazyky

- Role (R select: Jazyk textu, Původní jazyk překladu, Jazyk abstrakt, Jazyk titulků)
- Jazyk (R select: Čeština, Slovenština, Angličtina, Němčina)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Témata

- Hlavní téma (O checkbox)
- Schéma (R select: Předmětová kategorie Thema, MDT, Klíčová slova, BISAC téma)
- Kód (O select: FBC, YFA)
- Text (O text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Obsahy

- Typ (R select: Krátký popis, Popis, Obsah, Primární popis na obálce, Doporučení)
- Text (R multiLine)
- Jazyk (O select: Čeština, Slovenština, Angličtina, Němčina)
- Autor textu (O text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Zdroje

- Typ (R select: Přední obálka, Fotografie přispěvatele, Ilustrace, Plný text)
- Druh (R select: Aplikace, Audio, Obrázek, Text)
- Odkaz (R text)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Table: Uživatelská určení

- Typ (R select: Děti, Dospívající, Základní a střední škola, Školka, První stupeň, Druhý stupeň, Střední škola)
- Smazat
    - [outline-danger: X]()

[:Přidat]()

### Form:

- Věk od
- Věk do
- DOI
- Pořadí vydání (R)
- Poznámka k vydání
- Poznámky k ilustracím
- Značka
- Copyright od
- Copyright do
- Poznámka pro agenturu (multiLine)
- [primary: Uložit](#/titles/detail)
- [:Zpět](#/titles)
