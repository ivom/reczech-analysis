## Entity: Nakladatel titulu `TitlePublisher`

### Attributes:

- Titul `title` (1..n:1 `#Title`)
- Nakladatel `publisher` (n:1 `#Publisher`)
- Role (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/45

### Constraints

- Unique: `title`, `publisher`, `role`
