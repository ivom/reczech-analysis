## UC: `TT10` Prohlížet tituly

- Primární aktér: [Pracovník agentury](#Agent), [Garant nakladatele](#PublisherGuarantor), `#Distributor`
- Obrazovka: [Tituly](#/titles)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
    - Název: hledá v polích [Název](#TitleName.name), [Název části](#TitleName.partName)
      a [Podnázev](#TitleName.subtitle)
    - Přispěvatel: hledá v polích [Příjmení](#TitleContribution.lastName) a [Název organizace](#TitleContribution.name)
    - Nakladatel: hledá v [názvu nakladatele](#Publisher.name) propojeného přes [nakladatele titulu](#TitlePublisher)
2. Systém najde záznamy [titulu](#Title), které odpovídají zadaným kritériím, a zobrazí:
    - Hlavní název: [název](#TitleName.name) s typem `01` (hlavní název), pokud existuje, nebo první (jakýkoliv)
    - První přispěvatel: [Příjmení](#TitleContribution.lastName) a [Jméno](#TitleContribution.firstName)
      nebo [Název organizace](#TitleContribution.name) nebo [Nespecifikovaná osoba](#TitleContribution.unnamedPerson)
      příspěvku k titulu s [pořadím](#TitleContribution.sequenceNumber) 1
    - Nakladatel: [název](#Publisher.name) [vlastníka titulu](#Title.owner)
3. Systém seřadí záznamy podle hlavního názvu, prvního přispěvatele, nakladatele.
4. Uživatel vybere záznam.
5. Systém zobrazí detail vybraného [titulu](#Title).
6. Systém najde a zobrazí pro daný titul:
    - všechny jeho [názvy](#TitleName),
    - všechny jeho [příspěvky](#TitleContribution),
    - všechny jeho [nakladatele](#TitlePublisher),
    - všechny jeho [místa vydání](#TitlePublicationPlace),
    - všechny jeho [řady](#TitleCollection),
    - všechny jeho [jazyky](#TitleLanguage),
    - všechny jeho [témata](#TitleSubject),
    - všechny jeho [obsahy](#TitleTextContent),
    - všechny jeho [zdroje](#TitleSupportingResource),
    - všechny jeho [uživatelská určení](#TitleAudience).
7. Systém zobrazí Prvního přispěvatele a Hlavní název (viz krok 2) jako záhlaví.
8. Systém najde a zobrazí pro daný titul všechny jeho [produkty](#Product).

### Extensions:

- 1a. Uživatel má roli Garant nakladatele:
    - 1a1. Systém předvyplní výběrové kritérium na nakladatele přihlášeného uživatele.
- 2a. Uživatel má roli Distributor:
    - 2a1. Systém vynechá tituly, které nemají žádný [veřejný](#Product.accessibility) produkt.
- 2b. Uživatel má roli Garant nakladatele:
    - 2b1. Systém vynechá tituly [vlastněné](#Title.owner) jinými nakladateli, které nemají
      žádný [veřejný](#Product.accessibility) produkt.
