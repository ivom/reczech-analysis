## Entity: Příspěvek k titulu `TitleContribution`

### Attributes:

- Titul `title` (1..n:1 `#Title`)
- Přispěvatel `person` (n:0..1 `#Person`)
- Pořadí `sequenceNumber` (M integer): 1
- Role `roles` (1:1..n `#TitleContributionRole`)
- Jméno `firstName` (C text)
- Příjmení `lastName` (C text)
- Biografická poznámka `biographicalNote` (O text)
- Název organizace `name` (C text)
- Nespecifikovaná osoba `unnamedPerson` (C `#OnixCodetableItem`) - https://onix-codelists.io/codelist/19

### Poznámky

- Musí být vyplněno alespoň jedno z polí Jméno, Příjmení, Název, Nespecifikovaná osoba.
- Buď je vyplněna vazba na Osobu nebo je vyplněno pole Nespecifikovaná osoba (a ne zároveň).
