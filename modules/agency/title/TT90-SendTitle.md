## UC: `TT90` Odeslat titul

- Úroveň: Podfunkce
- Předpoklady
    - [Titul](#Title)

### Main success scenario:

1. Systém odešle zprávu s údaji těchto entit:
    - [titul](#Title), mimo položek:
        - Vlastník
        - Identifikátor vlastníka
        - Poznámka pro agenturu
    - jeho obsažené entity:
        - [názvy](#TitleName),
        - [jazyky](#TitleLanguage),
        - [témata](#TitleSubject),
        - [obsahy](#TitleTextContent),
        - [zdroje](#TitleSupportingResource),
        - [uživatelská určení](#TitleAudience).
    - jeho vazební entity:
        - [nakladatelé](#TitlePublisher), kde nakladatel je identifikován ??? TODO.
        - [příspěvky](#TitleContribution), kde osoba je identifikována [zdrojovým identifikátorem](#Person.sourceId),
            - jejich [rolí přispěvatelů](#TitleContributionRole),
        - [řady](#TitleCollection), kde řada je identifikována ??? TODO (nebo zahrnout atributy řady?),
    - jeho podřízené entity:
        - [produkty](#Product),
            - s položkami:
                - TODO
            - jejich podřízenými entitami:
                - [čísla produktů](#ProductNumber)
                    - s položkami:
                        - TODO

### Poznámky

- Odeslaná zpráva o titulu bude směrována do knihovního systému (nyní Aleph).
- Změny nadřízených entit (nakladatel, řada, osoba) nejsou promítnuty.
