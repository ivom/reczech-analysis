## Screen: Tituly `/titles`

### Form:

- Název
- Přispěvatel
- Nakladatel (select: Albatros Media, Euromedia Group, Grada Publishing, Moravská Bastei MOBA)

### Table:

- Hlavní název
    - [Babička](#/titles/detail)
    - [Čechové na Řípu](#/titles/detail)
    - [Psohlavci](#/titles/detail)
    - [Rok na vsi](#/titles/detail)
- První přispěvatel
    - Němcová Božena
    - Kolínský Adalbert
    - Jirásek Alois
    - Mrštík Vilém
- Nakladatel
    - Albatros Media
    - Albatros Media
    - Euromedia Group
    - Grada Publishing

[:Vytvořit](#/titles/create)
