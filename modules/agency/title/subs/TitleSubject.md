## Entity: Téma titulu `TitleSubject`

### Attributes:

- Hlavní téma `mainSubject` (M boolean)
- Schéma `scheme` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/27
- Kód `code` (C text)
- Text `text` (C text)

### Poznámky

- Hlavní téma musí být nastaveno na nejvýše jednom tématu daného titulu.
- Musí být vyplněno alespoň jedno z polí Kód, Text.
