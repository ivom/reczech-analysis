## Entity: Jazyk titulu `TitleLanguage`

### Attributes:

- Role `role` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/22
- Jazyk `language` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/74
