## Entity: Zdroj titulu `TitleSupportingResource`

### Attributes:

- Typ `type` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/158
- Druh `mode` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/159
- Odkaz `link` (M url)
