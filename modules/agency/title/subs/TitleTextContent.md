## Entity: Obsah titulu `TitleTextContent`

### Attributes:

- Typ `type` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/153
- Text (M text)
- Jazyk `language` (O `#OnixCodetableItem`) - https://onix-codelists.io/codelist/74
- Autor textu `textAuthor` (O text)
