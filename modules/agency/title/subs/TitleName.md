## Entity: Název titulu `TitleName`

### Attributes:

- Typ `type` (M `#OnixCodetableItem`) - https://onix-codelists.io/codelist/15
- Název `name` (M text): Babička
- Prefix `prefix` (O text)
- Název bez prefixu `titleWithoutPrefix` (O text)
- Číslo části `partNumber` (O text)
- Název části `partName` (O text)
- Podnázev `subtitle` (O text): obrazy vesnického života

### Další příklady

- Část
    - Název: Dějiny cisterckého řádu v Čechách
    - Číslo části: Svazek III.
    - Název části: Kláštery na hranicích a za hranicemi Čech

### Poznámky

- Onix nemá Název části.
  Z reálných dat plyne, že se Název části uvádí v poli Podnázev, což není správné.
  Existují ovšem i záznamy, které mají obě pole.
  Z těchto důvodů je potřeba rozšířit zprávy založené na Onix o nové pole Název části.
