## Screen: Titul `/titles/detail`

### Božena Němcová: Babička

### Form:

- Vlastník (RO): Albatros Media

### Table: Názvy

- Typ
    - Hlavní název
    - Název v jiném jazyce
- Název
    - Babička
    - The Grandmother
- Prefix
    - .
    - The
- Název bez prefixu
    - .
    - Grandmother
- Číslo části
- Název části
- Podnázev
    - Obrazy venkovského života

### Table: Příspěvky

- Pořadí
    - 1
    - 2
    - 3
    - 4
- Přispěvatel
    - [Božena Němcová](#/natural-persons/detail)
    - [Eliška Kutnohorská](#/natural-persons/detail)
    -
    - [Nadace Terezy Maxové](#/legal-persons/detail)
- Role
    - Autor
    - Námět, Spoluautor, Předmluva, Doslov
    - Ilustrátor
    - Vydavatel
- Jméno
    - Božena
    - Eliška
- Příjmení
    - Němcová
    - Kutnohorská
- Biografická poznámka
- Název organizace
    - .
    -
    -
    - Nadace Terezy Maxové
- Nespecifikovaná osoba
    - .
    -
    - AI

### Table: Nakladatelé

- Nakladatel
    - [Albatros Media](#/publishers/detail)
- Role
    - Nakladatel

### Table: Místa vydání

- Obec
    - Praha

### Table: Řady

- Typ
    - Edice
- Název
    - [Národní obrození](#/collections/detail)
- Číslo části
    - 1\. svazek

### Table: Jazyky

- Role
    - Jazyk textu
- Jazyk
    - Čeština

### Table: Témata

- Hlavní téma (checkbox)
    - true
- Schéma
    - Předmětová kategorie Thema
    - Předmětová kategorie Thema
    - Klíčová slova
- Kód
    - FBC
    - YFA
- Text
    - Klasická beletrie
    - Beletrie pro děti a mládež: klasická beletrie
    - Česká próza

### Table: Obsahy

- Typ
    - Popis
    - Primární popis na obálce
- Text
    - Babička je nejznámější dílo Boženy Němcové, poprvé vydané roku 1855...
    - Stěžejní dílo Boženy Němcové, které autorka psala od konce roku...
- Jazyk
    - Čeština
    - Čeština
- Autor textu
    - Jaroslav Pospíšil

### Table: Zdroje

- Typ
    - Přední obálka
    - Fotografie přispěvatele
    - Ilustrace
    - Plný text
- Druh
    - Obrázek
    - Obrázek
    - Obrázek
    - Text
- Odkaz
    - https://upload.wikimedia.org/wikipedia/commons/e/e2/Babi%C4%8Dka_%281855%29.jpg
    - https://upload.wikimedia.org/wikipedia/commons/1/16/Jan_Vil%C3%ADmek_-_Bo%C5%BEena_N%C4%9Bmcov%C3%A1.jpg
    - https://upload.wikimedia.org/wikipedia/commons/c/c8/Babicka-1924-A.jpg
    - https://cs.wikisource.org/wiki/Babi%C4%8Dka

### Table: Uživatelská určení

- Typ
    - Děti
    - Základní a střední škola

### Form:

- Věk od (RO): 8
- Věk do (RO): 12
- DOI (RO): https://doi.org/10.1000/182
- Pořadí vydání (RO): 3
- Poznámka k vydání (RO)
- Poznámky k ilustracím (RO): barevné ilustrace, portréty
- Značka (RO): Knižní klub
- Copyright od (RO): 2010
- Copyright do (RO): 2030
- Poznámka pro agenturu (RO)
- Stav (RO): Rozpracovaný
- [:Změnit](#/titles/edit)
- [:Přidat další vydání](#/titles/create)
- [:Zpět](#/titles)

### Table: Produkty

- Primární typ obsahu
    - Text
    - Text
- Forma
    - [Pevná vazba](#/products/detail)
    - [Brožováno](#/products/detail)
- Číslo produktu
    - 978-80-12-12345-1
    - 978-80-12-12346-7
- Datum vydání
    - 20.2.2020
    - 22.2.2020
- Přístupnost
    - Veřejný
    - Soukromý

[:Přidat](#/products/create)
