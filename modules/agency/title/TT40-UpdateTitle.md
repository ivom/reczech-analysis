## UC: `TT40` Změnit titul

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovka: [Změnit titul](#/titles/edit)

### Main success scenario:

1. Uživatel najde a vybere titul k editaci.
2. Systém zobrazí údaje [titulu](#Title).
3. Uživatel změní údaje titulu.
4. Systém provede formální validace dat.
5. Systém zapíše data z formuláře do:
    - [titulu](#Title),
        - jeho [názvů](#TitleName),
        - jeho [příspěvků](#TitleContribution),
            - jejich [rolí přispěvatelů](#TitleContributionRole),
        - jeho [nakladatelů](#TitlePublisher),
        - jeho [míst vydání](#TitlePublicationPlace),
        - jeho [řad](#TitleCollection),
        - jeho [jazyků](#TitleLanguage),
        - jeho [témat](#TitleSubject),
        - jeho [obsahů](#TitleTextContent),
        - jeho [zdrojů](#TitleSupportingResource),
        - jeho [uživatelských určení](#TitleAudience).
6. Systém [odešle titul](#TT90).

### Extensions:

- 2a. Uživatel má pouze roli Garant nakladatele a vybraný titul [nepatří](#Title.owner) nakladateli uživatele:
    - 2a1. Systém zobrazí chybu a případ užití končí.
- 3a. Uživatel má pouze roli Garant nakladatele:
    - 3a1. Systém skryje Vlastníka titulu.
    - 3a1. Systém zakáže změnit nebo smazat záznam [nakladatele titulu](#TitlePublisher) s nakladatelem uživatele
      a [rolí](#TitlePublisher.role) `01` (nakladatel).
- 3b. Uživatel vytvoří nebo smaže příspěvek nebo změní jejich pořadí (drag & drop):
    - 3b1. Systém přegeneruje [pořadí](#TitleContribution.sequenceNumber) příspěvků jako sekvenci počínaje jedničkou.
- 3c. Uživatel zadá text pro výběr řady:
    - 3c1. Systém hledá [edice](#Collection.type) kteréhokoliv z nakladatelů na titulu a
      všechny [série](#Collection.type) bez ohledu na nakladatele podle [hlavního názvu](#Collection.mainName).
    - 3c2. Uživatel vybere řadu.
    - 3c3. Systém zobrazí její [typ](#Collection.type).
- 3d. Uživatel přidá / smaže název / příspěvek / roli přispěvatele / nakladatele / místo vydání / řadu / jazyk / téma /
  obsah / zdroj / uživatelské určení:
    - 3d1. Systém přidá / smaže název / příspěvek / roli přispěvatele / nakladatele / místo vydání / řadu / jazyk /
      téma / obsah / zdroj / uživatelské určení.
- 3e. Uživatel vybral Schéma Tématu `93` (Předmětová kategorie Thema):
    - 3e1. Systém v daném Tématu nabízí kódy předmětových kategorií Thema.
    - 3e2. Systém zakáže pole Text daného Tématu a zobrazuje ho podle vybraného Kódu.
- 3f. Uživatel vybral Schéma Tématu `20` (Klíčová slova):
    - 3f1. Systém zakáže pole Kód a otevře pole Text daného Tématu.
- 3g. Uživatel vybral Schéma Tématu `09` (MDT / UDC):
    - 3g1. Systém na daném Tématu zakáže pole Text a otevře pole Kód jako volný text.
- 3h. Uživatel vybral jiné Schéma Tématu než výše uvedená (např. `10` BISAC):
    - 3h1. Systém na daném Tématu otevře pole Kód i Text jako volná textová pole.
- 3i. Uživatel označí jedno z Témat za Hlavní:
    - 3i1. Systém zruší nastavení Hlavní téma na všech ostatních Tématech.
- 4a. Není vyplněno povinné pole:
    - 4a1. Systém zobrazí chybu.
- 4b. Titul nemá žádný název / jazyk / místo vydání / nakladatele / příspěvek:
    - 4b1. Systém zobrazí chybu.
