## Entity: Titul `Title`

### Attributes:

- Vlastník `owner` (n:1 `#Publisher`)
- Identifikátor vlastníka `ownerId` (O text): 1234 - Identifikátor v systému nakladatele
- Názvy `names` (1:1..n `#TitleName`)
- Pořadí vydání `editionNumber` (O integer): 1
- Poznámka k vydání `editionStatement` (O text)
- Jazyky `languages` (1:1..n `#TitleLanguage`)
- Témata `subjects` (1:n `#TitleSubject`)
- Obsahy `textContents` (1:n `#TitleTextContent`)
- Zdroje `supportingResoures` (1:n `#TitleSupportingResource`)
- Místa vydání `publicationPlaces` (1:1..n `#TitlePublicationPlace`)
- Uživatelská určení `audiences` (1:n `#TitleAudience`)
- Věk od `ageFrom` (O integer): 8
- Věk do `ageTo` (O integer): 12
- DOI (O text): https://doi.org/10.1000/182
- Poznámky k ilustracím `illustrationsNote` (O text): barevné ilustrace, portréty
- Značka `imprint` (O text)
- Copyright od `copyrightFrom` (O year): 2020
- Copyright do `copyrightTo` (O year): 2030
- Poznámka pro agenturu `commentForAgency` (O text)
