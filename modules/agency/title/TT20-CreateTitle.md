## UC: `TT20` Vytvořit titul

- Primární aktér: [Garant nakladatele](#PublisherGuarantor), [Pracovník agentury](#Agent)
- Obrazovka: [Vytvořit titul](#/titles/create)

### Main success scenario:

1. Uživatel zadá údaje titulu a vytvoří ho.
2. Systém provede formální validace dat.
3. Systém zapíše data z formuláře do nových záznamů:
    - [titulu](#Title),
        - jeho [názvů](#TitleName),
        - jeho [příspěvků](#TitleContribution),
            - jejich [rolí přispěvatelů](#TitleContributionRole),
        - jeho [nakladatelů](#TitlePublisher),
        - jeho [míst vydání](#TitlePublicationPlace),
        - jeho [řad](#TitleCollection),
        - jeho [jazyků](#TitleLanguage),
        - jeho [témat](#TitleSubject),
        - jeho [obsahů](#TitleTextContent),
        - jeho [zdrojů](#TitleSupportingResource),
        - jeho [uživatelských určení](#TitleAudience).
4. Systém [odešle titul](#TT90).

### Extensions:

- 1a. Uživatel má pouze roli Garant nakladatele:
    - 1a1. Systém vyplní Vlastníka titulu jako nakladatele přihlášeného uživatele a skryje ho.
    - 1a2. Systém vyplní nakladatele přihlášeného uživatele s [rolí](#TitlePublisher.role) `01` (nakladatel) do prvního
      [nakladatele titulu](#TitlePublisher) a zakáže editaci nebo smazání tohoto záznamu.
- 1b. Uživatel vytvoří nebo smaže příspěvek nebo změní jejich pořadí (drag & drop):
    - 1b1. Systém přegeneruje [pořadí](#TitleContribution.sequenceNumber) příspěvků jako sekvenci počínaje jedničkou.
- 1c. Uživatel zadá text pro výběr řady:
    - 1c1. Systém hledá [edice](#Collection.type) kteréhokoliv z nakladatelů na titulu a
      všechny [série](#Collection.type) bez ohledu na nakladatele podle [hlavního názvu](#Collection.mainName).
    - 1c2. Uživatel vybere řadu.
    - 1c3. Systém zobrazí její [typ](#Collection.type).
- 1d. Uživatel přidá / smaže název / příspěvek / roli přispěvatele / nakladatele / místo vydání / řadu / jazyk / téma /
  obsah / zdroj / uživatelské určení:
    - 1d1. Systém přidá / smaže název / příspěvek / roli přispěvatele / nakladatele / místo vydání / řadu / jazyk /
      téma / obsah / zdroj / uživatelské určení.
- 1e. Uživatel vybral Schéma Tématu `93` (Předmětová kategorie Thema):
    - 1e1. Systém v daném Tématu nabízí kódy předmětových kategorií Thema.
    - 1e2. Systém zakáže pole Text daného Tématu a zobrazuje ho podle vybraného Kódu.
- 1f. Uživatel vybral Schéma Tématu `20` (Klíčová slova):
    - 1f1. Systém zakáže pole Kód a otevře pole Text daného Tématu.
- 1g. Uživatel vybral Schéma Tématu `09` (MDT / UDC):
    - 1g1. Systém na daném Tématu zakáže pole Text a otevře pole Kód jako volný text.
- 1h. Uživatel vybral jiné Schéma Tématu než výše uvedená (např. `10` BISAC):
    - 1h1. Systém na daném Tématu otevře pole Kód i Text jako volná textová pole.
- 1i. Uživatel označí jedno z Témat za Hlavní:
    - 1i1. Systém zruší nastavení Hlavní téma na všech ostatních Tématech.
- 2a. Není vyplněno povinné pole:
    - 2a1. Systém zobrazí chybu.
- 2b. Titul nemá žádný název / jazyk / místo vydání / nakladatele / příspěvek:
    - 2b1. Systém zobrazí chybu.
