## UC: `APB10` Prohlížet nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Nakladatelé](#/publishers)

### Main success scenario:

1. Uživatel zadá kritéria hledání, viz obrazovka.
2. Systém najde a zobrazí záznamy [nakladatele](#Publisher), které odpovídají zadaným kritériím, a seřadí je podle:
    - [Stav](#Publisher.status) v pořadí:
        - Aktivní a V likvidaci
        - Ukončený
    - [Název](#Publisher.name)
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybraného [nakladatele](#Publisher).
5. Systém najde a zobrazí pro daného nakladatele historii prefixů jako seznam
   všech [prefixů nakladatele](#PublisherPrefix), pro které existuje alespoň jedno [číslo produktu](#ProductNumber),
   které nakladateli [patří](#ProductNumber.publisher).
