## Screen: Nakladatel `/publishers/detail`

### Form:

- Název (RO): Albatros Media
- Doplněk k názvu (RO): Rybička
- Kvalifikátor (RO): Olomouc
- Stav (RO): Aktivní
- [:Prefixy nakladatele](#/agent/publisher-prefixes)
- [:Zpět](#/publishers)

### Table: Historie prefixů

- Prefix
    - [80-11](#/publisher-prefixes/detail)
    - [80-1234](#/publisher-prefixes/detail)
    - [80-987](#/publisher-prefixes/detail)
- Nakladatel
    - [Miscellaneous](#/publishers/detail)
    - [Albatros](#/publishers/detail)
    - [Grada](#/publishers/detail)
- Stav
    - Aktivní
    - Zavřený
    - Aktivní
