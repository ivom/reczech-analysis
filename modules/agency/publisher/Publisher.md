## Entity: Nakladatel `Publisher`

### Attributes:

- Zdrojový identifikátor `sourceId` (NK text): 1234
- Název `name` (M text)
- Doplněk k názvu `nameSuffix` (O text)
- Kvalifikátor `qualifier` (O text) - Rozlišení více nakladatelů se stejným názvem.
- Stav `status` (M enum: active, closing, closed)
- Registrační agentura `registrationAgency` (O enum: ISBN, ISMN): ISBN
- Registrační záměr `registrationIntent` (O enum: oneOff, continuous)
- Čas potvrzení údajů `dataConfirmationTime` (M datetime)
