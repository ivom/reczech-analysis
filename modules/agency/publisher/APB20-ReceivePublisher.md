## UC: `APB20` Přijmout nakladatele

- Primární aktér: [Systém](#System)
- Spouštěč: Příchozí zpráva typu Nakladatel
- Položky zprávy Nakladatel se shodují s položkami [entity Nakladatel](#Publisher).

### Main success scenario:

1. Systém přijme zprávu o nakladateli.
2. Systém najde [nakladatele](#Publisher) podle [zdrojového identifikátoru](#Publisher.sourceId).
3. Systém zapíše údaje ze zprávy do [nakladatele](#Publisher).

### Extensions:

- 2a. Nakladatel s daným zdrojovým identifikátorem neexistuje:
    - 2a1. Systém založí nového [nakladatele](#Publisher).
        - 2a1a. Nakladatel má [Registrační záměr](#Publisher.registrationIntent) vydávat soustavně:
            - 2a1a1. Systém vytvoří k nakladateli záznam [členství](#Membership) a nastaví:
                - [Agentura](#Membership.agency) = [registrační agentura nakladatele](#Publisher.registrationAgency)
                - [Čas zahájení](#Membership.startTime) = aktuální datum a čas
