## Screen: Nakladatelé `/publishers`

### Form:

- Název - Hledat podle názvu, doplňku k názvu, kvalifikátoru.
- Stav (select: Aktivní, V likvidaci, Ukončený)

### Table:

- Název
    - [Jan Novák](#/publishers/detail)
    - [Albatros Media](#/publishers/detail)
    - [Euromedia Group](#/publishers/detail)
    - [Moravská Bastei MOBA](#/publishers/detail)
- Doplněk k názvu
    - Rybička
- Kvalifikátor
    - Olomouc
- Stav
    - Aktivní
    - Aktivní
    - Aktivní
    - Aktivní
