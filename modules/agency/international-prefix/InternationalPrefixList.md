## Screen: Mezinárodní prefixy `/international-prefixes`

### Table:

- Agentura
    - [ISBN](#/international-prefixes/detail)
    - [ISBN](#/international-prefixes/detail)
    - [ISMN](#/international-prefixes/detail)
    - [ISMN](#/international-prefixes/detail)
- Formát
    - EAN13
    - ISBN10
    - EAN13
    - ISMN10
- Výchozí (O checkbox)
    - true
    - false
    - true
- Hodnota
    - 978
    -
    - 979-0
    - M

[:Vytvořit](#/international-prefixes/create)
