## UC: `IP10` Prohlížet mezinárodní prefixy

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Mezinárodní prefixy](#/international-prefixes)

### Main success scenario:

1. Systém zobrazí všechny záznamy mezinárodních prefixů řazené podle
    - [agentury](#InternationalPrefix.agency),
    - [formátu](#InternationalPrefix.format),
    - [výchozí](#InternationalPrefix.default) (výchozí hodnota první),
    - [hodnoty](#InternationalPrefix.value).
