## UC: `IP20` Vytvořit mezinárodní prefix

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Vytvořit mezinárodní prefix](#/international-prefixes/create)

### Main success scenario:

1. Uživatel zadá údaje mezinárodního prefixu a vytvoří mezinárodní prefix.
2. Systém vytvoří nový záznam [mezinárodního prefixu](#InternationalPrefix).

### Extensions:

- 2a. Záznam pro danou [agenturu](#InternationalPrefix.agency) a [formát](#InternationalPrefix.format) již existuje:
    - 2a1. Systém zobrazí chybu "Duplicitní záznam."
- 2b. Pro danou [agenturu](#InternationalPrefix.agency) ještě žádný jiný záznam neexistuje:
    - 2b1. Systém nastaví nový záznam jako [výchozí](#InternationalPrefix.default).
