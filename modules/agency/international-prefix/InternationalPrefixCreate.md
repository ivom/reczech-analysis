## Screen: Vytvořit mezinárodní prefix `/international-prefixes/create`

### Form:

- Agentura (R select: ISBN, ISMN)
- Formát(R select: EAN13, ISBN10, ISMN10)
- Hodnota (R)
- [primary: Uložit](#/international-prefixes/detail)
- [:Zpět](#/international-prefixes)
