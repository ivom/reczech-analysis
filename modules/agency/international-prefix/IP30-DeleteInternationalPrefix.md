## UC: `IP30` Smazat mezinárodní prefix

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Mezinárodní prefix](#/international-prefixes/detail)

### Main success scenario:

1. Uživatel vybere mezinárodní prefix a smaže ho.
2. Systém smaže vybraný záznam [mezinárodního prefixu](#InternationalPrefix).
