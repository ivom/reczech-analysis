## UC: `IP40` Nastavit výchozí mezinárodní prefix

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Mezinárodní prefixy](#/international-prefixes)

### Main success scenario:

1. Uživatel vybere mezinárodní prefix a nastaví ho jako výchozí.
2. Systém nastaví vybraný záznam mezinárodního prefixu jako [výchozí](#InternationalPrefix.default).
3. Systém zruší [výchozí](#InternationalPrefix.default) nastavení pro všechny ostatní záznamy
   dané [agentury](#InternationalPrefix.agency).
