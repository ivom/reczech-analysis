## Entity: Mezinárodní prefix `InternationalPrefix`

### Attributes:

- Agentura `agency` (M enum: ISBN, ISMN): ISBN
- Formát `format` (M enum: ISBN10, ISMN10, EAN13): EAN13
- Výchozí `default` (M boolean): ano
- Hodnota `value` (O text): 978

### Constraints

- Unique: `agency`, `format`
- Pro danou agenturu existuje právě jeden výchozí záznam.

### Záznamy

Viz obrazovka [Mezinárodní prefixy](#/international-prefixes).
