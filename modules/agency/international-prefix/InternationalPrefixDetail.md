## Screen: Mezinárodní prefix `/international-prefixes/detail`

### Form:

- Agentura (RO): ISBN
- Formát (RO): EAN13
- Výchozí (RO checkbox): true
- Hodnota (RO): 978
- [warning: Smazat](#/international-prefixes)
- [:Prefixy agentury](#/agency-prefixes)
- [:Zpět](#/international-prefixes)
