## Entity: Uživatel `User`

### Attributes:

- Zdrojový identifikátor `sourceId` (NK text): 12345
- Email (U text): vopicka@albatros.cz
- Jméno `firstName` (M text): Josef
- Příjmení `lastName` (M text): Vopička
- Role `roles` (M json): `["publisher","agent"]`
