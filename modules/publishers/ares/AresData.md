## Entity: Data z ARESu `AresData`

### Attributes:

- IČO `companyNumber` (NK text): 12345678
- Název `name` (M text)
- Právní forma `legalForm` (O enum: legal, natural)
- DIČ `taxNumber` (O text): CZ12345678
- Kód adresního místa `addressCode` (O text): 41405609 - Kód v RÚIAN
- Čas aktualizace `updateTime` (M datetime)
