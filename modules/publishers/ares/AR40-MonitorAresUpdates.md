## UC: `AR40` Hlídat změny údajů v ARESu

- Primární aktér: [Systém](#System)
- Spouštěč: Časovač, viz poznámka

### Main success scenario:

1. Systém najde všechna [data z ARESu](#AresData), jejichž [IČO](#AresData.companyNumber)
   se nerovná hodnotě [IČO](#Publisher.companyNumber) na žádném [nakladateli](Publisher), a smaže je.
2. Systém najde [záznam dat z ARESu](#AresData), který má nejstarší [čas aktualizace](#AresData.updateTime).
3. Systém [načte data z ARESu](#AR60) podle [IČO](#AresData.companyNumber) nalezeného záznamu.
4. Systém nastaví v [záznamu dat z ARESu](#AresData):
    - [Čas aktualizace](#AresData.updateTime) = aktuální datum a čas
5. Systém vyhodnotí, zda došlo ke změně údajů, tj. jsou-li načtené údaje odlišné od údajů uložených
   v [záznamu dat z ARESu](#AresData).
6. Systém zjistí, zda existuje [IČO nakladatele](#Publisher.companyNumber), pro které
   neexistuje [záznam dat z ARESu](#AresData).

### Extensions:

- 2a. Žádný záznam dat z ARESu neexistuje:
    - 2a1. Případ užití pokračuje krokem 5.
- 5a. Údaje v ARESu se změnily:
    - 5a1. Systém zapíše hodnoty načtené z ARESu do [záznamu dat z ARESu](#AresData).
    - 5a2. Systém najde [nakladatele](#Publisher) podle IČO.
        - 5a2a. Nakladatel neexistuje:
            - 5a2a1. Případ užití pokračuje následujícím krokem základního průběhu.
        - 5a2b. Existuje více nakladatelů s daným IČO:
            - 5a2b1. TODO
    - 5a3. Systém zjistí, zda existuje [založená](#AresUpdateRequest.status) žádost o změnu z ARESu pro
      nalezeného [nakladatele](#AresUpdateRequest.publisher).
        - 5a3a. Žádost již existuje:
            - 5a3a1. Případ užití pokračuje následujícím krokem základního průběhu.
    - 5a4. Systém založí [žádost o změnu z ARESu](#AresUpdateRequest) s:
        - [Nakladatel](#AresUpdateRequest.publisher) = nalezený nakladatel
        - [Čas založení](#AresUpdateRequest.createTime) = aktuální datum a čas
        - [Stav](#AresUpdateRequest.status) = Založená
        - [Data](#AresUpdateRequest.data) = záznam dat z ARESu
- 6a. Existuje IČO nakladatele, pro které neexistuje záznam dat z ARESu:
    - 6a1. Systém [načte data z ARESu](#AR60) podle [IČO nakladatele](#Publisher.companyNumber).
    - 6a2. Systém založí nový [záznam dat z ARESu](#AresData) pro dané IČO s hodnotami načtenými z ARESu a s:
        - [Čas aktualizace](#AresData.updateTime) = aktuální datum a čas

### Poznámky

- Systém spouští tento případ užití tak, aby naplnil tyto požadavky současně:
    - Každý záznam dat z ARESu musí být aktualizován nejméně jednou týdně.
    - Systém musí dodržet rate limiting systému ARES.
