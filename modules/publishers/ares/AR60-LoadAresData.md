## UC: `AR60` Načíst data z ARESu

- Úroveň: Podfunkce
- Předpoklady
    - IČO
- Výstupy
    - Název
    - Právní forma (nepovinné)
    - DIČ (nepovinné)
    - Kód adresního místa (nepovinné)

### Main success scenario:

1. Systém načte podle IČO tyto údaje pomocí služeb ARESu:
    - Název: `obchodniJmeno`
    - Právní forma: `pravniForma`, [číselník AA0019](https://apl.czso.cz/iSMS/cisdet.jsp?kodcis=56)
    - DIČ: `dic`
    - Kód adresního místa: `sidlo.kodAdresnihoMista`

### Poznámky

- ARES URL:
    - `https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/` + `IČO`
    - Příklady:
        - Plný záznam: https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/27074358
        - Bez DIČ: https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/09735933
        - Bez adresního místa: https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/00023221
