## Actor: Pracovník agentury `Agent`

### Cíle

- Nakladatel
    - [Prohlížet nakladatele](#PB10)
    - [Vyřídit žádosti nakladatelů o registraci](#PB40)
    - [Založit nakladatele](#PB21)
    - [Potvrdit údaje nakladatele](#PB43)
    - [Změnit nakladatele](#PB45)
    - [Vyřídit žádosti nakladatelů o změnu](#PB47)
    - [Vyřídit žádosti o změnu z ARESu](#PB50)
    - [Ukončit nakladatele](#PB30)
    - [Obnovit nakladatele](#PB32)
    - [Nastavit nakladatele v likvidaci](#PB33)
