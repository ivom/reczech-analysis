## Actor: Garant nakladatele `PublisherGuarantorActor`

### Cíle

- Nakladatel
    - [Zobrazit svého nakladatele](#PB15)
    - [Zaregistrovat se jako nakladatel](#PB20)
    - [Potvrdit údaje svého nakladatele](#PB42)
    - [Změnit svého nakladatele](#PB46)
    - [Ukončit činnost jako nakladatel](#PB31)
