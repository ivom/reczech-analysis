## Entity: Nakladatel `Publisher`

### Attributes:

- Id (PK bigserial)
- Název `name` (M text)
- Doplněk k názvu `nameSuffix` (O text)
- Kvalifikátor `qualifier` (O text) - Rozlišení více nakladatelů se stejným názvem.
- Alternativní názvy `alternativeNames` (1:n `#PublisherAlternativeName`)
- Právní forma `legalForm` (O enum: legal, natural)
- IČO `companyNumber` (O text): 12345678
- DIČ `taxNumber` (O text): CZ12345678
- Datum narození `birthDate` (O date)
- Adresa sídla `mainAddress` (1:1 `#PublisherAddress`)
- Doručovací adresa `mailingAddress` (1:0..1 `#PublisherAddress`)
- Sídelní obec `residentialMunicipality` (M text)
- Sídelní kraj `residentialRegion` (n:1 `#Region`)
- Kontakty `contacts` (1:n `#PublisherContact`)
- Garanti `guarantors` (1:n `#PublisherGuarantor`)
- Poznámka `comment` (O text) - chtějí to nakladatelé?
- Interní poznámka `internalComment` (O text)
- Původ `origin` (M enum: agency, publisher)
- Stav `status` (M enum: request, rejected, active, closing, closed)
- Čas založení `createTime` (M datetime)
- Zadavatel registrace `registrant` (O text) - Jméno a příjmení osoby, která vyplnila registrační formulář.
- Registrační agentura `registrationAgency` (O enum: ISBN, ISMN): ISBN
- Registrační záměr `registrationIntent` (O enum: oneOff, continuous)
- Čas aktivace `activationTime` (O datetime) - První
- Čas potvrzení údajů `dataConfirmationTime` (M datetime)
- Čas ukončení `closeTime` (O datetime) - Poslední
- Původní identifikátor `originalId` (O long) - Sekvenční identifikátor ve starém systému, tzv. `sysno`.

### Poznámky

- Řádově desítky tisíc záznamů (dnes 20 k).
- Původní identifikátor nebude potřeba, pokud nový systém vyloučí duplicity v číslování.

### Stavový diagram

```plant
[*] --> Request: Register (Zaregistrovat se)
[*] --> Active: Create (Založit)
Request: Žádost
Request --> Rejected: Reject (Zamítnout)
Request --> Active: Approve (Schválit)
Rejected: Zamítnutý
Active: Aktivní
Active --> Closing: Closing (V likvidaci)
Active --> Closed: Close (Ukončit)
Closing: V likvidaci
Closing --> Closed: Close (Ukončit)
Closing --> Active: Restore (Obnovit)
Closed: Ukončený
Closed --> Active: Restore (Obnovit)
```
