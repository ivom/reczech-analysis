## UC: `PB40` Vyřídit žádosti nakladatelů o registraci

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Žádosti o registraci nakladatelů](#/publisher-registrations),
  [Žádost o registraci nakladatele](#/publisher-registrations/detail)

### Main success scenario:

1. Systém zobrazí žádosti nakladatelů o registraci
   ([záznamy nakladatele](#Publisher) ve [stavu](#Publisher.status) Žádost)
   seřazené podle [času založení](#Publisher.createTime) od nejstarších.
2. Uživatel vybere žádost.
3. Systém zobrazí detail žádosti.
4. Systém [najde potenciální duplicity nakladatele](#PB70) a zobrazí je.
5. Uživatel ověří a případně upraví registrační údaje nakladatele.
6. Uživatel schválí žádost.
7. Systém provede formální validace dat.
8. Systém znovu [najde potenciální duplicity nakladatele](#PB70).
9. Systém zapíše data z formuláře do:
    - [nakladatele](#Publisher),
    - jeho [alternativních názvů](#Publisher.alternativeNames), včetně jejich vytvoření nebo smazání,
    - jeho [adresy sídla](#Publisher.mainAddress),
    - jeho [kontaktů](#Publisher.contacts),
    - jeho [garantů](#Publisher.guarantors),
    - a jejich [kontaktů](#PublisherGuarantor.contacts),
10. Systém na [nakladateli](#Publisher) nastaví:
    - [Stav](#Publisher.status) = Aktivní
    - [Čas aktivace](#Publisher.activationTime) = aktuální datum a čas
11. Systém [odešle nakladatele](#PB90).
12. Případ užití pokračuje krokem 1.

### Extensions:

- 3a. Doručovací adresa byla / nebyla zadána:
    - 3a1. Systém zobrazí / skryje údaje doručovací adresy.
- 3b. IČO bylo zadáno:
    - 3b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 3c. Datum narození byl zadán:
    - 3c1. Systém vypočte věk žadatele k aktuálnímu dni.
        - 3c1a. Věk žadatele je menší než 18 let:
            - 3c1a1. Systém zobrazí upozornění "Žadatel není plnoletý, věk žadatele k dnešnímu dni je XX."
- 5a. Uživatel zadá / smaže IČO:
    - 5a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
- 5b. Uživatel načte údaje nakladatele podle IČO:
    - 5b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 5c. Nakladatel nemá v ARESu uveden Kód adresního místa
  nebo adresa sídla neodpovídá Kódu adresního místa z ARESu:
    - 5c1. Systém zobrazí upozornění "Pozor: Adresa sídla k danému IČO nepochází z ARESu." a odkaz na záznam nakladatele
      v ARESu podle IČO.
- 5d. Uživatel přidá / smaže alternativní název:
    - 5d1. Systém přidá / smaže alternativní název.
- 5e. Uživatel změní Právní formu:
    - 5e1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
- 5f. Uživatel se rozhodne změnit adresu sídla:
    - 5f1. Uživatel [vybere adresu sídla nakladatele](#PB65).
- 6a. Uživatel zamítne žádost:
    - 6a1. Systém na [nakladateli](#Publisher) nastaví [Stav](#Publisher.status) = Zamítnutý.
    - 6a2. Případ užití pokračuje krokem 1.
- 7a. Právnická osoba nemá IČO ani DIČ:
    - 7a1. Systém zobrazí chybu.
- 7b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 7b1. Systém zobrazí chybu.
- 7c. IČO nemá právě 8 číslic:
    - 7c1. Systém zobrazí chybu.
- 7d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 7d1. Systém zobrazí chybu.
- 7e. Hodnota kontaktu typu E-mail není formálně platnou e-mailovou adresou:
    - 7e1. Systém zobrazí chybu.
- 7f. Hodnota kontaktu typu Web není formálně platným URL:
    - 7f1. Systém zobrazí chybu.
- 7g. Hodnota kontaktu typu Datová schránka má méně než 6 nebo více než 12 znaků:
    - 7g1. Systém zobrazí chybu.
- 7h. Hodnota kontaktu typu Telefon má méně než 9 číslic:
    - 7h1. Systém zobrazí chybu.
- 7i. Existuje jiný záznam se shodnou kombinací polí Název a Kvalifikátor:
    - 7i1. Systém zobrazí chybu.
- 8a. Systém nalezl nové potenciální duplicity, které nebyly nalezeny při předchozím hledání:
    - 8a1. Systém zobrazí aktualizované potenciální duplicity.
    - 8a2. Systém zobrazí varování "Byly nalezeny nové potenciální duplicity."
    - 8a3. Případ užití pokračuje krokem 5.
- 9a. [Kontakt](#PublisherContact) [nakladatele](#Publisher.contacts) nebo [garanta](#PublisherGuarantor.contacts)
  je typu Telefon a obsahuje právě 9 číslic:
    - 9a1. Systém ponechá pouze číslice a doplní dvě mezery do formátu: `123 456 789`.
