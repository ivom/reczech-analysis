## UC: `PB33` Nastavit nakladatele v likvidaci

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Nakladatel](#/publishers/detail)

### Main success scenario:

1. Uživatel najde [aktivního](#Publisher.status) nakladatele a nastaví ho v likvidaci.
2. Systém nastaví na [nakladateli](#Publisher):
    - [Stav](#Publisher.status) = V likvidaci
3. Systém [odešle nakladatele](#PB90).
