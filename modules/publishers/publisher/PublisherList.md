## Screen: Nakladatelé `/publishers`

### Form:

- Hledat - Hledat full-textově podle definovaných položek.
- Název - Hledat podle názvu, doplňku k názvu, kvalifikátoru a všech alternativních názvů.
- IČO / DIČ
- Datum narození od (date)
- Datum narození do (date)
- Stav (select: Aktivní, V likvidaci, Ukončený, Žádost, Zamítnutý)

### Table:

- Název
    - [Albatros Media](#/publishers/detail)
    - [Euromedia Group](#/publishers/detail)
    - [Grada](#/publishers/detail)
    - [Moravská Bastei MOBA](#/publishers/detail)
- Doplněk k názvu
    - Rybička
- Kvalifikátor
    - Olomouc
- Stav
    - Aktivní
    - Aktivní
    - Aktivní

[:Přidat](#/publishers/create)
