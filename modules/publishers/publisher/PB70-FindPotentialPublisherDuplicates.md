## UC: `PB70` Najít potenciální duplicity nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Nakladatel](#Publisher)
- Výstupy
    - Seznam [nakladatelů](#Publisher)

### Main success scenario:

1. Systém jiné záznamy [nakladatele](#Publisher), které se podobají vstupnímu záznamu.

### Poznámky

- Skupiny:
    - IČO, Datum narození, DIČ
    - Název, Doplněk k názvu, Kvalifikátor
    - Telefon (z kontaktů pro veřejnost i ze všech garantů)
    - Email (z kontaktů pro veřejnost i ze všech garantů)
    - Adresa: Obec, Ulice - jak konkrétně?
