## UC: `PB66` Vybrat doručovací adresu nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Nakladatel](#Publisher)

### Main success scenario:

1. Uživatel [vybere adresu](#PB67).
2. Systém zapíše hodnoty adresy do těchto údajů nakladatele:
    - [Doručovací adresa](#Publisher.mailingAddress), tj.:
        - [Kód adresního místa](#PublisherAddress.addressCode) = Kód adresního místa
        - [Ulice](#PublisherAddress.street): Adresní řádek 1
        - [PSČ](#PublisherAddress.postalCode): PSČ
        - [Obec](#PublisherAddress.municipality): Obec
