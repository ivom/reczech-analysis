## UC: `PB50` Vyřídit žádosti o změnu z ARESu

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Žádosti o změnu z ARESu](#/ares-updates),
  [Žádost o změnu z ARESu](#/ares-updates/detail)

### Main success scenario:

1. Systém zobrazí [žádosti o změnu z ARESu](#AresUpdateRequest)
   ve [stavu](#AresUpdateRequest.status) Založená
   seřazené podle [času založení](#AresUpdateRequest.createTime) od nejstarších.
2. Uživatel vybere žádost.
3. Systém zobrazí detail [žádosti o změnu z ARESu](#AresUpdateRequest).
4. Systém zvýrazní hodnoty, u kterých se liší nová hodnota od stávající.
5. Systém [najde potenciální duplicity nakladatele](#PB70) a zobrazí je.
6. Systém načte [alternativní názvy nakladatele](#Publisher.alternativeNames) a seřadí je podle času vytvoření sestupně.
7. Uživatel ověří a případně upraví údaje nakladatele.
8. Systém provede formální validace dat.
9. Systém znovu [najde potenciální duplicity nakladatele](#PB70).
10. Systém zapíše data z formuláře do:
    - [nakladatele](#Publisher),
    - jeho [alternativních názvů](#Publisher.alternativeNames), včetně jejich vytvoření nebo smazání,
    - a jeho [adresy sídla](#Publisher.mainAddress).
11. Systém [odešle nakladatele](#PB90).
12. Systém nastaví na [žádosti o změnu z ARESu](#AresUpdateRequest):
    - [Stav](#AresUpdateRequest.status) = Vyřízená
13. Případ užití pokračuje krokem 1.

### Extensions:

- 4a. IČO nebylo v ARESu nalezeno:
    - 4a1. Systém zobrazí informaci o zrušení IČO v ARESu.
- 7a. Uživatel přidá / smaže alternativní název:
    - 7a1. Systém přidá / smaže alternativní název.
- 7b. Uživatel přidá stávající název jako předchozí:
    - 7b1. Systém vytvoří nový záznam [alternativního názvu nakladatele](#PublisherAlternativeName) s:
        - [Typ](#PublisherAlternativeName.type) = Předchozí,
        - [Název](#PublisherAlternativeName.name) = stávající název,
        - [Doplněk k názvu](#PublisherAlternativeName.nameSuffix) = stávající doplněk k názvu,
        - [Kvalifikátor](#PublisherAlternativeName.qualifier) = stávající kvalifikátor.
- 7c. Uživatel změní Právní formu:
    - 7c1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození. - TODO: doplnit na obr.?
- 7d. Uživatel se rozhodne změnit adresu sídla:
    - 7d1. Uživatel [vybere adresu](#PB67) sídla nakladatele.
- 7e. Uživatel se rozhodne ukončit činnost nakladatele:
    - 7e1. Systém přejde na [ukončení daného nakladatele](#PB30).
- 8a. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 8a1. Systém zobrazí chybu.
- 8b. Existuje jiný záznam se shodnou kombinací polí Název a Kvalifikátor:
    - 8b1. Systém zobrazí chybu.
- 9a. Systém nalezl nové potenciální duplicity, které nebyly nalezeny při předchozím hledání:
    - 9a1. Systém zobrazí aktualizované potenciální duplicity.
    - 9a2. Systém zobrazí varování "Byly nalezeny nové potenciální duplicity."
    - 9a3. Případ užití pokračuje krokem 7.
- 10b. Došlo ke změně adresy sídla nakladatele:
    - 10b1. Systém najde [kraj](#Region) podle načteného názvu.
        - 10b1a. Kraj nebyl nalezen:
            - 10b1a1. Systém založí nový [kraj](#Region).
    - 10b2. Systém nastaví na nakladateli:
        - [Sídelní obec](#Publisher.residentialMunicipality) = Obec
        - [Sídelní kraj](#Publisher.residentialRegion) = nalezený [kraj](#Region)
    - 10b2a. Došlo ke změně sídelního kraje:
        - 10b2a1. Systém odešle upozornění, že došlo ke změně sídelního kraje, na email ? TODO.
        - 10b2a2. Systém vytvoří nový záznam [změny kraje nakladatele](#PublisherRegionUpdate).
          TODO: -> Modul povinné výtisky?
