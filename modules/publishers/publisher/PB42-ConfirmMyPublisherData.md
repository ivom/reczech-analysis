## UC: `PB42` Potvrdit údaje svého nakladatele

- Primární aktér: [Garant nakladatele](#PublisherGuarantorActor)
- Obrazovka: [Můj nakladatel](#/my-publisher)

### Main success scenario:

1. Uživatel potvrdí platnost údajů vlastního nakladatele.
2. Systém na [nakladateli](#Publisher) nastaví:
    - [Čas potvrzení údajů](#Publisher.dataConfirmationTime) = aktuální datum a čas
3. Systém [odešle nakladatele](#PB90).
