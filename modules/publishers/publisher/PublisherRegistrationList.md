## Screen: Žádosti o registraci nakladatelů `/publisher-registrations`

### Form:

- Registrační záměr (select: Jednorázově, Soustavně)

### Table:

- Registrační záměr
    - Jednorázově
    - Soustavně
    - Soustavně
    - Soustavně
- Název
    - [Albatros Media](#/publisher-registrations/detail)
    - [Euromedia Group](#/publisher-registrations/detail)
    - [Grada](#/publisher-registrations/detail)
    - [Moravská Bastei MOBA](#/publisher-registrations/detail)
- Datum registrace
    - 20.2.2022
    - 22.2.2022
    - 24.2.2022
    - 26.2.2022
