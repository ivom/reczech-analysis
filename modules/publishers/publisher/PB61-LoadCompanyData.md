## UC: `PB61` Načíst údaje podle IČO

- Úroveň: Podfunkce
- Předpoklady
    - IČO
- Výstupy
    - Název
    - Právní forma (nepovinné)
    - DIČ (nepovinné)
    - Kód adresního místa (nepovinné)
    - Datová schránka (nepovinné)
    - Kód adresního místa (nepovinné)
    - Adresní řádek 1, tj. Ulice, Číslo popisné, Číslo evidenční (nepovinné)
    - PSČ (nepovinné)
    - Obec (nepovinné)
    - Kraj (nepovinné)

### Main success scenario:

1. Systém [načte data z ARES](#AR60) podle IČO, tj.:
    - Název
    - Právní forma
    - DIČ
    - Kód adresního místa
2. Systém načte podle IČO kód datové schránky ze Systému datových schránek.
3. Systém načte podle kódu adresního místa tyto údaje nakladatele pomocí služeb RÚIAN:
    - Adresní řádek 1, tj. Ulice, Číslo popisné, Číslo evidenční
    - PSČ
    - Obec
    - Kraj (VÚSC)

### Extensions:

- 3a. Nakladatel nemá v ARESu uveden Kód adresního místa:
    - 3a1. Případ užití končí.

### Poznámky

- Systém datových schránek:
    - Formulář: https://www.mojedatovaschranka.cz/sds/
    - Popis webových služeb: https://www.mojedatovaschranka.cz/sds/p/download/sds_webove_sluzby.pdf
- RÚIAN:
    - Adresa podle adresního místa, formulář: https://vdp.cuzk.cz/vdp/ruian/adresnimista/41405609
