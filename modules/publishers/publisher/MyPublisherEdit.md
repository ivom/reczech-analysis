## Screen: Změnit svého nakladatele `/my-publisher/edit`

### Form: Údaje vyžadující schválení změny agenturou

- IČO: 12345678
- [:Načíst dle IČO z rejstříku]()
- Název (R): Grada Publishing
- Název, původní (RO): Grada Publishing

> Pro případ fyzické osoby s upraveným názvem, aby nedošlo k přepsání z registru.

- Doplněk k názvu: Rybička
- Právní forma (R radios: Právnická osoba, Fyzická osoba): Právnická osoba
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- Datum narození (date): 20.2.2022
- FieldSet: Adresa sídla
    - Ulice a číslo (R): Nádražní 13/6b - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R): 12100 - Pět číslic bez mezery, např. 12100.
    - Obec (R): Praha 2
    - Souhlas se zveřejněním (R radios: Ano, Ne): Ano

### Form: Údaje nevyžadující schválení změny agenturou

- Doručovací adresa, je-li odlišná od sídla (checkbox): true
- FieldSet: Doručovací adresa
    - Ulice a číslo (R): Hlavní 32 - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R): 32100 - Pět číslic bez mezery, např. 12100.
    - Obec (R): Brno
    - Souhlas se zveřejněním (R radios: Ano, Ne): Ano
- FieldSet: Kontakty pro veřejnost (telefon, e-mail, web, datová schránka)
  Vyplňte pouze v případě, že si je přejete zveřejnit.
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka): Telefon
        - Hodnota (R): 777 666 555
        - [:Smazat kontakt]()
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka): E-mail
        - Hodnota (R): kontakt@grada.cz
        - [:Smazat kontakt]()
    - [:Přidat další kontakt]()
- FieldSet: Garanti - kontaktní osoby pro agenturu
    - FieldSet: Garant
        - Tituly před jménem
        - Jméno (R): Jan
        - Příjmení (R): Novák
        - Tituly za jménem
        - E-mail (R): novak@grada.cz - E-mailová adresa pro přihlašování do systému.
        - [:Použít i jako kontaktní e-mail]()
        - FieldSet: Kontakty
            - FieldSet: Kontakt
                - Typ (R select: Telefon, E-mail, Web, Datová schránka): Telefon
                - Hodnota (R): 777 888 999
                - [:Smazat kontakt]()
            - [:Přidat další kontakt]()
        - [:Smazat garanta]()
    - [:Přidat garanta]()
- [primary: Odeslat](#/my-publisher)

> TODO vyhodit?! - Při změně kraje zobrazit následující upozornění:

- FieldSet: Změna kraje
  Změnili jste kraj, povinné výtisky...
