## UC: `PB90` Odeslat nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Nakladatel](#Publisher)

### Main success scenario:

1. Systém odešle zprávu s údaji [nakladatele](#Publisher):
    - [Id](#Publisher.id) `id` (povinné)
    - [Název](#Publisher.name) `name` (povinné)
    - [Doplněk k názvu](#Publisher.nameSuffix) `nameSuffix` (nepovinné)
    - [Kvalifikátor](#Publisher.qualifier) `qualifier` (nepovinné)
    - [Stav](#Publisher.status) `status` (povinný výčet: request, rejected, active, closing, closed)
    - [Registrační agentura](#Publisher.registrationAgency) `registrationAgency` (nepovinný výčet: ISBN, ISMN)
    - [Registrační záměr](#Publisher.registrationIntent) `registrationIntent` (nepovinný výčet: oneOff, continuous)
    - [Čas potvrzení údajů](#Publisher.dataConfirmationTime) `dataConfirmationTime` (povinný datetime)
    - [Garanti](#PublisherGuarantor) `guarantors`
        - [Id](#PublisherGuarantor.id) `id` (povinné)
        - [Email](#PublisherGuarantor.email) `email` (povinné)
        - [Jméno](#PublisherGuarantor.firstName) `firstName` (povinné)
        - [Příjmení](#PublisherGuarantor.lastName) `lastName` (povinné)
