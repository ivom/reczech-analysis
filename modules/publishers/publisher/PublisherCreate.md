## Screen: Založení nakladatele `/publishers/create`

### Table: Potenciální duplicity

- IČO / Datum narození / DIČ
    - 12345678
    - 12345678
    - 12345678
    - 20.2.2000
    - 20.2.2000
- Název
    - [UK](#/publishers/detail)
    - UK
    - Jan Novák
    - Jana Nováková
    - Jana Nováková
- Doplněk k názvu
    - 1 . lékařská, klinika adiktologie
    - 1 . lékařská
- Kvalifikátor
    - .
    - .
    - Olomouc
- Telefon
    - 777 888 999
    -
    -
    -
    - 777 888 999
- Email
    - .
    - .
    - .
    - .
    - vopicka@seznam.cz
- Obec
    - .
    - .
    - .
    - .
    - Praha 2
- Ulice
    - .
    - .
    - .
    - .
    - Nádražní 13/6b

### Form:

- IČO: 12345678
- [:Načíst dle IČO z rejstříku]()
- Název (R): Grada
- Doplněk k názvu: Rybička
- Kvalifikátor
- Název, registr (RO): Grada

### Table: Alternativní názvy

- Název (R)
    - .
- Doplněk k názvu (O)
    - .
- Kvalifikátor (O)
    - .
- Typ (O select: Předchozí, Značka, Zkratka, Organizační složka, Cizojazyčná varianta, Jiné)
- Neveřejný (O checkbox)
    - false
- Smazat
    - [warning:Smazat]()

[:Přidat alternativní název]()

### Form:

- Právní forma (radios: Právnická, Fyzická)
- Právní forma, registr (RO radios: Právnická, Fyzická): Právnická
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- DIČ, registr (RO): CZ12345678
- Datum narození (date): 20.2.2000
- FieldSet: Adresa sídla
    - Ulice a číslo: Nádražní 13/6b
    - PSČ: 12100
    - Obec: Praha 2
    - Sídelní obec (RO): Praha
    - Sídelní kraj (RO): Hlavní město Praha
- FieldSet: Kontakty
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka)
        - Hodnota (R)
        - [:Smazat kontakt]()
    - [:Přidat další kontakt]()
- Interní poznámka (multiLine)
- [primary: Uložit](#/publishers/detail)
- [: Zrušit](#/publishers)

> Pokud není Právní forma, systém zobrazí upozornění.
>
> Fyzická osoba nemusí mít ani IČO, ani DIČ ani Datum narození.
