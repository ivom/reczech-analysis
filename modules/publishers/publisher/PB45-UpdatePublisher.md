## UC: `PB45` Změnit nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Změnit nakladatele](#/publishers/edit)

### Main success scenario:

1. Uživatel najde a vybere nakladatele k editaci.
2. Systém zobrazí údaje [nakladatele](#Publisher).
3. Systém [najde potenciální duplicity nakladatele](#PB70) a zobrazí je.
4. Systém načte [alternativní názvy nakladatele](#Publisher.alternativeNames) a seřadí je podle času vytvoření sestupně.
5. Uživatel změní údaje nakladatele.
6. Systém provede formální validace dat.
7. Systém znovu [najde potenciální duplicity nakladatele](#PB70).
8. Systém zapíše data z formuláře do:
    - [nakladatele](#Publisher),
    - jeho [alternativních názvů](#Publisher.alternativeNames), včetně jejich vytvoření nebo smazání,
    - jeho [adresy sídla](#Publisher.mainAddress),
    - jeho [doručovací adresy](#Publisher.mailingAddress), včetně jejího vytvoření nebo smazání,
    - jeho [kontaktů](#Publisher.contacts), včetně jejich vytvoření nebo smazání,
    - jeho [garantů](#Publisher.guarantors), včetně jejich vytvoření nebo smazání,
    - a jejich [kontaktů](#PublisherGuarantor.contacts), včetně jejich vytvoření nebo smazání.
9. Systém [odešle nakladatele](#PB90).

### Extensions:

- 2a. Existuje podaná žádost o změnu nakladatele:
    - 2a1. Systém zobrazí informaci "Žádost o změnu nakladatele čeká na vyřízení."
    - 2a2. Případ užití končí.
- 2b. IČO bylo zadáno:
    - 2b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 5a. Uživatel zadá / smaže IČO:
    - 5a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
- 5b. Uživatel načte údaje nakladatele podle IČO:
    - 5b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 5c. Nakladatel nemá v ARESu uveden Kód adresního místa
  nebo adresa sídla neodpovídá Kódu adresního místa z ARESu:
    - 5c1. Systém zobrazí upozornění "Pozor: Adresa sídla k danému IČO nepochází z ARESu." a odkaz na záznam nakladatele
      v ARESu podle IČO.
- 5d. Uživatel přidá / smaže alternativní název:
    - 5d1. Systém přidá / smaže alternativní název.
- 5e. Uživatel přidá stávající název jako předchozí:
    - 5e1. Systém vytvoří nový záznam [alternativního názvu nakladatele](#PublisherAlternativeName) s:
        - [Typ](#PublisherAlternativeName.type) = Předchozí,
        - [Název](#PublisherAlternativeName.name) = stávající název,
        - [Doplněk k názvu](#PublisherAlternativeName.nameSuffix) = stávající doplněk k názvu,
        - [Kvalifikátor](#PublisherAlternativeName.qualifier) = stávající kvalifikátor.
- 5f. Uživatel změní Právní formu:
    - 5f1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
- 5g. Uživatel zadal fyzickou osobu bez IČO:
    - 5g1. Systém zobrazí Souhlas se zveřejněním Adresy sídla.
- 5h. Uživatel zadal právnickou osobu nebo IČO:
    - 5h1. Systém skryje Souhlas se zveřejněním Adresy sídla.
- 5i. Uživatel se rozhodne změnit adresu sídla:
    - 5i1. Uživatel [vybere adresu](#PB67) sídla nakladatele.
- 5j. Uživatel zvolí, že doručovací adresa je odlišná od sídla:
    - 5j1. Systém otevře zadávání doručovací adresy.
    - 5j2. Uživatel [vybere doručovací adresu nakladatele](#PB66).
- 5k. Uživatel zvolí, že doručovací adresa je shodná s adresou sídla:
    - 5k1. Systém skryje zadávání doručovací adresy.
- 5l. Uživatel přidá / smaže kontakt pro veřejnost / kontakt garanta:
    - 5l1. Systém přidá / smaže kontakt pro veřejnost / kontakt garanta.
- 5m. Uživatel použije email garanta jako jeho kontaktní email a tento garant nemá žádný kontakt typu E-mail:
    - 5m1. Systém přidá garantovi kontakt typu E-mail s hodnotou emailu garanta.
- 5n. Uživatel přidá / smaže garanta:
    - 5n1. Systém přidá / smaže garanta.
- 6a. Právnická osoba nemá IČO ani DIČ:
    - 6a1. Systém zobrazí chybu.
- 6b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 6b1. Systém zobrazí chybu.
- 6c. IČO nemá právě 8 číslic:
    - 6c1. Systém zobrazí chybu.
- 6d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 6d1. Systém zobrazí chybu.
- 6e. Hodnota kontaktu typu E-mail není formálně platnou e-mailovou adresou:
    - 6e1. Systém zobrazí chybu.
- 6f. Hodnota kontaktu typu Web není formálně platným URL:
    - 6f1. Systém zobrazí chybu.
- 6g. Hodnota kontaktu typu Datová schránka má méně než 6 nebo více než 12 znaků:
    - 6g1. Systém zobrazí chybu.
- 6h. Hodnota kontaktu typu Telefon má méně než 9 číslic:
    - 6h1. Systém zobrazí chybu.
- 6i. Žádný garant není uveden: TODO: pouze pro toho, co žádal
    - 6i1. Systém zobrazí chybu.
- 6j. Garant nemá žádný kontakt:
    - 6j1. Systém zobrazí chybu.
- 6k. Existuje jiný záznam se shodnou kombinací polí Název a Kvalifikátor:
    - 6k1. Systém zobrazí chybu.
- 7a. Systém nalezl nové potenciální duplicity, které nebyly nalezeny při předchozím hledání:
    - 7a1. Systém zobrazí aktualizované potenciální duplicity.
    - 7a2. Systém zobrazí varování "Byly nalezeny nové potenciální duplicity."
    - 7a3. Případ užití pokračuje krokem 5.
- 8a. [Kontakt](#PublisherContact) [nakladatele](#Publisher.contacts) nebo [garanta](#PublisherGuarantor.contacts)
  je typu Telefon a obsahuje právě 9 číslic:
    - 8a1. Systém ponechá pouze číslice a doplní dvě mezery do formátu: `123 456 789`.
