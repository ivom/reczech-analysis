## UC: `PB65` Vybrat adresu sídla nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - [Nakladatel](#Publisher)

### Main success scenario:

1. Uživatel [vybere adresu](#PB67).
2. Systém zapíše hodnoty adresy do těchto údajů nakladatele:
    - [Adresa sídla](#Publisher.mainAddress), tj.:
        - [Kód adresního místa](#PublisherAddress.addressCode) = Kód adresního místa
        - [Ulice](#PublisherAddress.street): Adresní řádek 1
        - [PSČ](#PublisherAddress.postalCode): PSČ
        - [Obec](#PublisherAddress.municipality): Obec
    - [Sídelní obec](#Publisher.residentialMunicipality): Obec
    - [Sídelní kraj](#Publisher.residentialRegion): nalezený [kraj](#Region)

### Extensions:

- 2a. Kraj byl načten:
    - 2a1. Systém najde [kraj](#Region) podle načteného názvu.
        - 2a1a. Kraj nebyl nalezen:
            - 2a1a1. Systém založí nový [kraj](#Region).
