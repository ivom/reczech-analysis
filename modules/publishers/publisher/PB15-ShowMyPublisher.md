## UC: `PB15` Zobrazit svého nakladatele

- Primární aktér: [Garant nakladatele](#PublisherGuarantorActor)
- Obrazovka: [Můj nakladatel](#/my-publisher)

### Main success scenario:

1. Uživatel se přihlásí do systému.
2. Systém najde a zobrazí údaje [nakladatele](#Publisher) přihlášeného uživatele.
3. Systém najde a zobrazí pro daného nakladatele:
    - všechny jeho [alternativní názvy](#Publisher.alternativeNames),
    - jeho [adresu sídla](#Publisher.mainAddress),
    - jeho [doručovací adresu](#Publisher.mailingAddress),
    - všechny jeho [kontakty](#Publisher.contacts),
    - všechny jeho [garanty](#Publisher.guarantors),
    - a všechny jejich [kontakty](#PublisherGuarantor.contacts).

### Extensions:

- 2a. [Čas potvrzení údajů](#Publisher.dataConfirmationTime) nakladatele je starší než 1 rok:
    - 2a1. Systém zobrazí upozornění "Zkontrolujete prosím platnost Vašich údajů."
    - 2a2. Uživatel [potvrdí údaje svého nakladatele](#PB42).
