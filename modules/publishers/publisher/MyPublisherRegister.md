## Screen: Registrace nakladatele `/my-publisher/register`

### Form:

- Žádám o (R radios: ISBN - standardní číslo knihy, ISMN - standardní číslo hudebniny)
- Plánuji (R radios: Vydat knihu/hudebninu jednorázově, Vydávat knihy/hudebniny soustavně)
- IČO
- [:Načíst dle IČO z rejstříku]()
- Název (R)
- Doplněk k názvu
- Právní forma (R radios: Právnická osoba, Fyzická osoba)
- DIČ - Včetně kódu státu, např. CZ12345678.
- Datum narození (date)
- FieldSet: Adresa sídla
    - Ulice a číslo (R) - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R) - Pět číslic bez mezery, např. 12100.
    - Obec (R)
    - Souhlas se zveřejněním (R radios: Ano, Ne)
- Doručovací adresa, je-li odlišná od sídla (checkbox)
- FieldSet: Doručovací adresa
    - Ulice a číslo (R) - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R) - Pět číslic bez mezery, např. 12100.
    - Obec (R)
    - Souhlas se zveřejněním (R radios: Ano, Ne)
- FieldSet: Kontakty pro veřejnost (telefon, e-mail, web, datová schránka)
  Vyplňte pouze v případě, že si je přejete zveřejnit.
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka)
        - Hodnota (R)
        - [:Smazat kontakt]()
    - [:Přidat další kontakt]()
- FieldSet: Garant - kontaktní osoba pro agenturu
  Osoba odpovědná za přidělování ISBN nebo ISMN v nakladatelství.
    - Tituly před jménem
    - Jméno (R)
    - Příjmení (R)
    - Tituly za jménem
    - E-mail (R) - E-mailová adresa pro přihlašování do systému.
    - [:Použít i jako kontaktní e-mail]()
    - FieldSet: Kontakty
        - FieldSet: Kontakt
            - Typ (R select: Telefon, E-mail, Web, Datová schránka)
            - Hodnota (R)
            - [:Smazat kontakt]()
        - [:Přidat další kontakt]()
- FieldSet: Údaje o knize / hudebnině
    - Název (R)
    - Podnázev
    - Číslo dílu, svazku
    - Název části
    - FieldSet: Autor
        - Jméno
        - Příjmení
        - [:Smazat autora]()
    - [:Přidat autora]()
    - Datum vydání (R date)
    - Pořadí vydání (R)
    - Vydáno v koedici s - Vyplňte pouze pokud publikaci vydává více subjektů.
    - FieldSet: V jaké podobě chcete publikaci vydat
        - Tištěná publikace (checkboxes: v měkké vazbě, v pevné vazbě)
        - E-kniha / e-hudebnina ve formátu (checkboxes: pdf, epub, mobi)
        - FieldSet: Jiný formát
            - Formát
            - [:Smazat jiný formát]()
        - [:Přidat jiný formát]()
        - URL - URL stránek, kde budete svou e-knihu / e-hudebninu nabízet.
        - Verze na datovém nosiči (checkboxes: CD-ROM, DVD-ROM) - V publikaci musí převažovat psaný text / notový zápis.
        - Audio kniha (checkboxes: na nosiči, bez nosiče)
- Poznámka k registraci (multiLine)

Odesláním formuláře se nakladatel zavazuje...

- Datum (RO): 20.2.2022
- Vyplnil, jméno a příjmení (R)
- [primary: Zaregistrovat se]()

Po odeslání formuláře obdržíte potvrzení o přijetí žádosti o registraci na Vaši e-mailovou adresu pro přihlašování do
systému.
