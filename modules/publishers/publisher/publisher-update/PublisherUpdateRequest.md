## Entity: Žádost o změnu nakladatele `PublisherUpdateRequest`

### Attributes:

- Nakladatel `publisher` (n:1 `#Publisher`)
- Čas podání `submitTime` (M datetime)
- Uživatel `user` (n:1 `#User`)
- Stav `status` (M enum: submitted, resolved)
- Výsledek `result` (O enum: approved, rejected)
- Data (M json)

### Stavový diagram

```plant
[*] --> Submitted: Update my publisher (Změnit vlastního nakladatele)
Submitted: Podaná
Submitted --> Resolved: Resolve publisher update (Vyřídit žádost o změnu)
Resolved: Vyřízená
```
