## UC: `PB10` Prohlížet nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Nakladatelé](#/publishers), [Detail nakladatele](#/publishers/detail)

### Main success scenario:

1. Uživatel zadá kritéria hledání:
    - "Hledat": full-textově
    - Název: podle názvu, doplňku k názvu, kvalifikátoru a všech alternativních názvů
    - IČO / DIČ: podle IČO nebo DIČ
    - Datum narození od / do: podle data narození spadajícího do zadaného intervalu
    - Stav
2. Systém najde a zobrazí odpovídající záznamy [nakladatele](#Publisher) řazené podle:
    - [Stav](#Publisher.status) v pořadí: Aktivní, Žádost, V likvidaci, Ukončený, Zamítnutý
    - [Název](#Publisher.name)
3. Uživatel vybere záznam.
4. Systém zobrazí detail vybraného [nakladatele](#Publisher).
5. Systém najde a zobrazí pro daného nakladatele:
    - všechny jeho [alternativní názvy](#Publisher.alternativeNames),
    - jeho [adresu sídla](#Publisher.mainAddress),
    - jeho [doručovací adresu](#Publisher.mailingAddress),
    - všechny jeho [kontakty](#Publisher.contacts),
    - všechny jeho [garanty](#Publisher.guarantors),
    - a všechny jejich [kontakty](#PublisherGuarantor.contacts).
