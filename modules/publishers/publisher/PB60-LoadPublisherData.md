## UC: `PB60` Načíst údaje nakladatele podle IČO

- Úroveň: Podfunkce
- Předpoklady
    - IČO
    - [Nakladatel](#Publisher)

### Main success scenario:

1. Systém [načte údaje podle IČO](#PB61).
2. Systém zapíše načtené údaje do nakladatele:
    - [Název](#Publisher.name) = Název
    - [Právní forma](#Publisher.legalForm) = Právní forma
    - [DIČ](#Publisher.taxNumber) = DIČ
    - [Kontakt pro veřejnost](#Publisher.contacts) typu Datová schránka = Datová schránka
    - [Kontakt](#PublisherGuarantor.contacts) [garanta](#Publisher.guarantors) typu Datová schránka = Datová schránka
    - [Adresa sídla](#Publisher.mainAddress), tj.:
        - [Kód adresního místa](#PublisherAddress.addressCode) = Kód adresního místa
        - [Ulice](#PublisherAddress.street) = Adresní řádek 1, tj. Ulice, Číslo popisné, Číslo evidenční
        - [PSČ](#PublisherAddress.postalCode) = PSČ
        - [Obec](#PublisherAddress.municipality) = Obec
    - [Sídelní obec](#Publisher.residentialMunicipality) = Obec
    - [Sídelní kraj](#Publisher.residentialRegion) = nalezený [kraj](#Region)

### Extensions:

- 2a. Datová schránka nebo údaje adresy nebyly načteny:
    - 2a1. Systém příslušné položky nevyplní.
- 2b. Kraj byl načten:
    - 2b1. Systém najde [kraj](#Region) podle načteného názvu.
        - 2b1a. Kraj nebyl nalezen:
            - 2b1a1. Systém založí nový [kraj](#Region).
