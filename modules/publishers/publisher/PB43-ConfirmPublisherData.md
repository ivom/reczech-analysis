## UC: `PB43` Potvrdit údaje nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Detail nakladatele](#/publishers/detail)

### Main success scenario:

1. Uživatel najde a vybere nakladatele.
2. Uživatel potvrdí platnost údajů nakladatele.
3. Systém na [nakladateli](#Publisher) nastaví:
    - [Čas potvrzení údajů](#Publisher.dataConfirmationTime) = aktuální datum a čas
4. Systém [odešle nakladatele](#PB90).
