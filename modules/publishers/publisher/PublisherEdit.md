## Screen: Změnit nakladatele `/publishers/edit`

### Table: Potenciální duplicity

> TODO: Zde doplnit tabulku potenciálních duplicit.

### Form:

- IČO: 12345678
- [:Načíst dle IČO z rejstříku]()
- Název, stávající (RO): Grada Publishing
- Doplněk k názvu, stávající (RO): Rybička
- Kvalifikátor, stávající (RO): Olomouc
- Název (R): Grada Publishing
- Doplněk k názvu: Rybička
- Kvalifikátor: Olomouc
- Název, registr (RO): Grada Publishing
- [:Přidat stávající název jako předchozí]()

### Table: Alternativní názvy

- Název (R)
    - Jan Novák
    - Grada
    - Bambook
- Doplněk k názvu (O)
    - Rybička
    - .
    - .
- Kvalifikátor (O)
    - Olomouc
    - .
    - .
- Typ (O select: Předchozí, Značka, Zkratka, Organizační složka, Cizojazyčná varianta, Jiné)
    - Předchozí
    - Zkratka
    - Značka
- Neveřejný (O checkbox)
    - true
    - false
    - false
- Smazat
    - [warning:Smazat]()
    - [warning:Smazat]()
    - [warning:Smazat]()

[:Přidat alternativní název]()

### Form:

- Právní forma (R radios: Právnická osoba, Fyzická osoba): Právnická osoba
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- Datum narození (date): 20.2.2022
- FieldSet: Adresa sídla
    - Ulice a číslo (R): Nádražní 13/6b - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R): 12100 - Pět číslic bez mezery, např. 12100.
    - Obec (R): Praha 2
    - Sídelní obec (RO): Praha
    - Sídelní kraj (RO): Hlavní město Praha
    - Souhlas se zveřejněním (R radios: Ano, Ne): Ano
- Doručovací adresa, je-li odlišná od sídla (checkbox): true
- FieldSet: Doručovací adresa
    - Ulice a číslo (R): Hlavní 32 - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ (R): 32100 - Pět číslic bez mezery, např. 12100.
    - Obec (R): Brno
    - Souhlas se zveřejněním (R radios: Ano, Ne): Ano
- FieldSet: Kontakty pro veřejnost (telefon, e-mail, web, datová schránka)
  Vyplňte pouze v případě, že si je přejete zveřejnit.
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka): Telefon
        - Hodnota (R): 777 666 555
        - [:Smazat kontakt]()
    - FieldSet: Kontakt
        - Typ (R select: Telefon, E-mail, Web, Datová schránka): E-mail
        - Hodnota (R): kontakt@grada.cz
        - [:Smazat kontakt]()
    - [:Přidat další kontakt]()
- FieldSet: Garanti - kontaktní osoby pro agenturu
    - FieldSet: Garant
        - Tituly před jménem
        - Jméno (R): Jan
        - Příjmení (R): Novák
        - Tituly za jménem
        - E-mail (R): novak@grada.cz - E-mailová adresa pro přihlašování do systému.
        - [:Použít i jako kontaktní e-mail]()
        - FieldSet: Kontakty
            - FieldSet: Kontakt
                - Typ (R select: Telefon, E-mail, Web, Datová schránka): Telefon
                - Hodnota (R): 777 888 999
                - [:Smazat kontakt]()
            - [:Přidat další kontakt]()
        - [:Smazat garanta]()
    - [:Přidat garanta]()
- Interní poznámka (multiLine): Stávající obsah.
- [primary: Uložit](#/publishers/detail)
- [:Zpět](#/publishers/detail)
