## UC: `PB67` Vybrat adresu

- Úroveň: Podfunkce
- Výstupy
    - Adresní řádek 1
    - PSČ
    - Obec
    - Kraj

### Main success scenario:

1. Uživatel zadává údaje adresy.
2. Systém napovídá datové položky adresy podle existujících adresních míst v RÚIAN.
3. Uživatel vybere platné adresní místo z RÚIAN.
4. Systém vrátí hodnoty adresního místa:
    - Kód adresního místa
    - Adresní řádek 1: Adresní řádek 1, tj. Ulice, Číslo popisné, Číslo evidenční
    - PSČ: PSČ
    - Obec: Obec
    - Kraj: Kraj (VÚSC)

### Poznámky

- RÚIAN: https://vdp.cuzk.cz/vdp/ruian/
