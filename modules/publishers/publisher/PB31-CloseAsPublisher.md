## UC: `PB31` Ukončit činnost jako nakladatel

- Primární aktér: [Garant nakladatele](#PublisherGuarantorActor)
- Obrazovka: [Můj nakladatel](#/my-publisher)

### Main success scenario:

1. Uživatel ukončí činnost vlastního nakladatele, a to buď [aktivního](#Publisher.status)
   nebo [v likvidaci](#Publisher.status).
2. Uživatel potvrdí ukončení.
3. Systém na [nakladateli](#Publisher) nastaví:
    - [Stav](#Publisher.status) = Ukončený
    - [Čas ukončení](#Publisher.closeTime) = aktuální datum a čas
4. Systém [odešle nakladatele](#PB90).
