## Entity: Alternativní název nakladatele `PublisherAlternativeName`

### Attributes:

- Název `name` (M text)
- Doplněk k názvu `nameSuffix` (O text)
- Kvalifikátor `qualifier` (O text)
- Typ `type` (O enum: previous, imprint, abbreviation, organizationalUnit, foreignLanguage)
- Neveřejný `nonPublic` (M boolean) - Nakladatele lze podle názvu vyhledat, ale název nebude vidět.

### Typ

- `previous`: Předchozí
- `imprint`: Značka
- `abbreviation`: Zkratka
- `organizationalUnit`: Organizační složka
- `foreignLanguage`: Cizojazyčná varianta

### Poznámky

- Řadit od nejnovějšího.
- V současném systému také řazeno od nejnovějšího. Při migraci zachovat řazení.
