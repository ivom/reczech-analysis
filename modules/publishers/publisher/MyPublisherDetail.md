## Screen: Můj nakladatel `/my-publisher`

### Form:

- Název (RO): Grada Publishing
- Doplněk k názvu (RO): Rybička

### Table: Alternativní názvy

- Název
    - Grada
    - Grada
- Doplněk k názvu
    - Rybička
- Kvalifikátor
    - Brno
- Typ
    - Předchozí
- Neveřejný (checkbox)
    - true
    - false

### Form:

- Právní forma (RO): Právnická osoba
- IČO (RO): 12345678
- DIČ (RO): CZ12345678
- Datum narození (RO): 20. 2. 2000
- FieldSet: Adresa sídla
    - Ulice (RO): Nádražní 13/6b
    - PSČ (RO): 123 45
    - Obec (RO): Praha 2
- FieldSet: Doručovací adresa
    - Ulice (RO): Hlavní 123
    - PSČ (RO): 123 45
    - Obec (RO): Praha 4
- Sídelní obec (RO): Praha
- Sídelní kraj (RO): Hlavní město Praha

### Table: Kontakty pro veřejnost

- Typ
    - Telefon
    - E-mail
- Hodnota
    - 777 666 555
    - kontakt@grada.cz

### Table: Garanti

- Jméno
    - Ing. Mgr. Jan Novák, CSc.
    - Jana Nová
- Kontakty
    - Telefon: 777 666 555, E-mail: novak@grada.cz
    - Telefon: 777 555 444
- Uživatel
    - Jan Novák
    - Jana Nová
- Agentury
    - ISBN, ISMN
    - ISBN

### Form:

- Stav (RO): Aktivní
- Čas registrace (RO): 20. 2. 2020
- Čas aktivace (RO): 22. 2. 2020
- Čas ukončení (RO)

### Povinné výtisky

Knihy (neperiodika):

- NK: 2x
- MZK: 1x
- VKOL: 1x
- Regionální: 1x

Periodika:

- NK: 2x
- MZK: 1x
- VKOL: 1x
- Regionální: 1x

> Seznam adres knihoven, kam má nakladatel posílat povinné výtisky, včetně počtu.
>
> Regionální bude vyplněno podle sídelního kraje nakladatele.

### Form:

[:Potvrdit platnost údajů]()

[:Změnit](#/my-publisher/edit)

[:Ohlásit ukončení nakladatelské činnosti](#/my-publisher/close)
