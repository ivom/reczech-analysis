## Screen: Žádost o změnu nakladatele `/publisher-updates/detail`

### Table: Potenciální duplicity

> TODO: Zde doplnit tabulku potenciálních duplicit.

### Form:

- IČO, stávající (RO): 12345678
- IČO: 12345678
- [:Načíst dle IČO z rejstříku]()
- Název, stávající (RO): Grada Publishing
- Doplněk k názvu, stávající (RO): Rybička
- Kvalifikátor, stávající (RO): Olomouc
- Název (R): Nová Grada
- Doplněk k názvu: Rybička
- Kvalifikátor: Olomouc
- Název, registr (RO): Grada Publishing
- [:Přidat stávající název jako předchozí]()

### Table: Alternativní názvy

- Název (R)
    - Jan Novák
    - Grada
    - Bambook
- Doplněk k názvu (O)
    - Rybička
    - .
    - .
- Kvalifikátor (O)
    - Olomouc
    - .
    - .
- Typ (O select: Předchozí, Značka, Zkratka, Organizační složka, Cizojazyčná varianta, Jiné)
    - Předchozí
    - Zkratka
    - Značka
- Neveřejný (O checkbox)
    - true
    - false
    - false
- Smazat
    - [warning:Smazat]()
    - [warning:Smazat]()
    - [warning:Smazat]()

[:Přidat alternativní název]()

### Form:

- Právní forma, stávající (RO radios: Právnická osoba, Fyzická osoba): Právnická osoba
- Právní forma (R radios: Právnická osoba, Fyzická osoba): Právnická osoba
- Právní forma, registr (RO radios: Právnická osoba, Fyzická osoba): Právnická osoba
- DIČ, stávající (RO): CZ12345678
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- DIČ, registr (RO): CZ12345678
- Datum narození, stávající (RO): 20.2.2022
- Datum narození (date): 20.2.2022
- FieldSet: Adresa sídla
    - Ulice a číslo, stávající (RO): Nádražní 13/6b
    - Ulice a číslo (R): Nádražní 13/6b - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - PSČ, stávající (RO): 12100
    - PSČ (R): 12100 - Pět číslic bez mezery, např. 12100.
    - Obec, stávající (RO): Praha 2
    - Obec (R): Praha 2
    - Sídelní obec, stávající (RO): Praha
    - Sídelní obec (RO): Praha
    - Sídelní kraj, stávající (RO): Hlavní město Praha
    - Sídelní kraj (RO): Hlavní město Praha
- Interní poznámka (multiLine): Stávající obsah.
- [warning: Schválit](#/publisher-updates)
- [warning: Zamítnout](#/publisher-updates)
