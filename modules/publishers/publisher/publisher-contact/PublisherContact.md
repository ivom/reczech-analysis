## Entity: Kontakt nakladatele `PublisherContact`

### Attributes:

- Typ `type` (M enum: phone, email, web, dataBox)
- Hodnota `value` (M text)

### Hodnoty

- `phone` Telefon
- `email` E-mail
- `web` Web
- `dataBox` Datová schránka
