## Screen: Ukončení nakladatelské činnosti `/my-publisher/close`

### Form: Potvrďte ukončení nakladatelské činnosti

[danger:Potvrdit ukončení nakladatelské činnosti](#/my-publisher)

[:Zpět](#/my-publisher)
