## UC: `PB32` Obnovit nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Nakladatel](#/publishers/detail)

### Main success scenario:

1. Uživatel najde nakladatele [ukončeného](#Publisher.status) nebo [v likvidaci](#Publisher.status) a obnoví ho.
2. Systém nastaví na [nakladateli](#Publisher):
    - [Stav](#Publisher.status) = Aktivní
3. Uživatel [změní nakladatele](#PB45).
4. Systém [odešle nakladatele](#PB90).
