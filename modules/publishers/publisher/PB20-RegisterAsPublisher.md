## UC: `PB20` Zaregistrovat se jako nakladatel

- Primární aktér: [Garant nakladatele](#PublisherGuarantorActor) jako nepřihlášený (anonymní) uživatel
- Obrazovka: [Registrace nakladatele](#/my-publisher/register)

### Main success scenario:

1. Uživatel zvolí typ identifikátoru, o který žádá.
2. Systém následně použije termín "kniha" nebo "hudebnina" podle zvoleného typu identifikátoru.
3. Uživatel zvolí, zda hodlá vydat titul jednorázově, nebo bude vydávat soustavně.
4. Systém zobrazí (jednorázově) nebo skryje (soustavně) Údaje o titulu podle záměru nakladatele.
5. Uživatel zadá IČO a [načte údaje nakladatele podle IČO](#PB60).
6. Uživatel zadá ostatní údaje nakladatele a případně titulu a zaregistruje se jako nakladatel.
7. Systém provede formální validace dat.
8. Systém zapíše data z formuláře do nových záznamů:
    - [nakladatele](#Publisher),
    - jeho [adresy sídla](#Publisher.mainAddress),
    - jeho [doručovací adresy](#Publisher.mailingAddress),
    - jeho [kontaktů](#Publisher.contacts),
    - jeho [garantů](#Publisher.guarantors),
    - a jejich [kontaktů](#PublisherGuarantor.contacts).
    - TODO: uložit údaje o titulu
9. Systém na [nakladateli](#Publisher) nastaví:
    - [Původ](#Publisher.origin) = Nakladatel
    - [Stav](#Publisher.status) = Žádost
    - [Čas založení](#Publisher.createTime) = aktuální datum a čas
    - [Čas potvrzení údajů](#Publisher.dataConfirmationTime) = aktuální datum a čas
    - [Interní poznámka nakladatele](#Publisher.internalComment) = Poznámka k registraci
10. Systém odešle e-mailovou zprávu na přihlašovací email registrovaného garanta nakladatele (bcc `isbn@nkp.cz`) s
    potvrzením o přijetí žádosti o registraci včetně údajů registrace.
11. Systém [odešle nakladatele](#PB90).

### Extensions:

- 5a. Uživatel zadá / smaže IČO:
    - 5a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
    - 5a2. Systém zakáže (s IČO) / povolí (bez IČO) vyplnění těchto údajů nakladatele ručně:
      Název, Právní forma, DIČ, Datum narození.
- 5b. Nakladatel nemá IČO:
    - 5b1. Uživatel zadá základní údaje nakladatele (Název, Právní forma, DIČ, Datum narození) ručně.
        - 5b1a. Uživatel zvolí Právní formu:
            - 5b1a1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
    - 5b2. Uživatel [vybere adresu sídla nakladatele](#PB65).
- 5c. Nakladatel má IČO, ale nemá v ARESu uveden Kód adresního místa:
    - 5c1. Uživatel [vybere adresu sídla nakladatele](#PB65).
- 5d. Systém načetl z ARESu platný Kód adresního místa:
    - 5d1. Systém zakáže ruční editaci adresy sídla (Ulice a číslo, PSČ, Obec).
- 6a. Uživatel zadal fyzickou osobu bez IČO:
    - 6a1. Systém zobrazí Souhlas se zveřejněním Adresy sídla.
- 6b. Uživatel zadal právnickou osobu nebo IČO:
    - 6b1. Systém skryje Souhlas se zveřejněním Adresy sídla.
- 6c. Uživatel zvolí, že doručovací adresa je odlišná od sídla:
    - 6c1. Systém otevře zadávání doručovací adresy.
    - 6c2. Uživatel [vybere doručovací adresu nakladatele](#PB66).
- 6d. Uživatel zvolí, že doručovací adresa je shodná s adresou sídla:
    - 6d1. Systém skryje zadávání doručovací adresy.
- 6e. Uživatel přidá / smaže kontakt pro veřejnost / kontakt garanta:
    - 6e1. Systém přidá / smaže kontakt pro veřejnost / kontakt garanta.
- 6f. Uživatel použije email garanta jako jeho kontaktní email a tento garant nemá žádný kontakt typu E-mail:
    - 6f1. Systém přidá garantovi kontakt typu E-mail s hodnotou emailu garanta.
- 6g. Uživatel přidá / smaže autora:
    - 6g1. Systém přidá / smaže autora.
- 6h. Uživatel zaškrtne nebo vyplní / smaže elektronický formát:
    - 6h1. Systém povolí / zakáže pole URL.
- 6i. Uživatel přidá / smaže jiný formát:
    - 6i1. Systém přidá / smaže jiný formát.
- 7a. Právnická osoba nemá IČO ani DIČ:
    - 7a1. Systém zobrazí chybu.
- 7b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 7b1. Systém zobrazí chybu.
- 7c. IČO nemá právě 8 číslic:
    - 7c1. Systém zobrazí chybu.
- 7d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 7d1. Systém zobrazí chybu.
- 7e. Hodnota kontaktu typu E-mail není formálně platnou e-mailovou adresou:
    - 7e1. Systém zobrazí chybu.
- 7f. Hodnota kontaktu typu Web není formálně platným URL:
    - 7f1. Systém zobrazí chybu.
- 7g. Hodnota kontaktu typu Datová schránka má méně než 6 nebo více než 12 znaků:
    - 7g1. Systém zobrazí chybu.
- 7h. Hodnota kontaktu typu Telefon má méně než 9 číslic:
    - 7h1. Systém zobrazí chybu.
- 7i. Garant nemá žádný kontakt:
    - 7i1. Systém zobrazí chybu.
- 7j. Titul nemá žádného autora:
    - 7j1. Systém zobrazí chybu.
- 8a. [Kontakt](#PublisherContact) [nakladatele](#Publisher.contacts) nebo [garanta](#PublisherGuarantor.contacts)
  je typu Telefon a obsahuje právě 9 číslic:
    - 8a1. Systém ponechá pouze číslice a doplní dvě mezery do formátu: `123 456 789`.
