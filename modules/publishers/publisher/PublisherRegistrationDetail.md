## Screen: Žádost o registraci nakladatele `/publisher-registrations/detail`

### Table: Potenciální duplicity

- IČO / Datum narození / DIČ
    - 12345678
    - 12345678
    - 12345678
    - 20.2.2000
    - 20.2.2000
- Název
    - [UK](#/publishers/detail)
    - UK
    - Jan Novák
    - Jana Nováková
    - Jana Nováková
- Doplněk k názvu
    - 1 . lékařská, klinika adiktologie
    - 1 . lékařská
- Kvalifikátor
    - .
    - .
    - Olomouc
- Telefon
    - 777 888 999
    -
    -
    -
    - 777 888 999
- Email
    - .
    - .
    - .
    - .
    - vopicka@seznam.cz
- Obec
    - .
    - .
    - .
    - .
    - Praha 2
- Ulice
    - .
    - .
    - .
    - .
    - Nádražní 13/6b

### Form:

- Žádám o (RO radios: ISBN, ISMN): ISBN
- Plánuji (RO radios: Vydat jednorázově, Vydávat soustavně): Vydávat soustavně
- IČO: 12345678
- [:Načíst dle IČO z rejstříku]()
- Název (R): Grada
- Doplněk k názvu: Rybička
- Kvalifikátor
- Název, registr (RO): Grada

### Table: Alternativní názvy

- Název (R)
    - .
- Doplněk k názvu (O)
    - .
- Kvalifikátor (O)
    - .
- Typ (O select: Předchozí, Značka, Zkratka, Organizační složka, Cizojazyčná varianta, Jiné)
- Neveřejný (O checkbox)
    - false
- Smazat
    - [warning:Smazat]()

[:Přidat alternativní název]()

### Form:

- Právní forma (R radios: Právnická, Fyzická): Právnická
- Právní forma, registr (RO radios: Právnická, Fyzická): Právnická
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- DIČ, registr (RO): CZ12345678
- Datum narození (date): 20.2.2000
- FieldSet: Adresa sídla
    - Ulice a číslo (R): Nádražní 13/6b
    - PSČ (R): 12100
    - Obec (R): Praha 2
    - Sídelní obec (RO): Praha
    - Sídelní kraj (RO): Hlavní město Praha
- FieldSet: Doručovací adresa
    - Ulice a číslo (RO): Hlavní 32
    - PSČ (RO): 13100
    - Obec (RO): Praha 3
- FieldSet: Kontakty pro veřejnost (telefon, e-mail, web, datová schránka)
  Vyplňte pouze v případě, že si je přejete zveřejnit.
    - FieldSet: Kontakt
        - Typ (RO): Telefon
        - Hodnota (R): 777 888 999
    - FieldSet: Kontakt
        - Typ (RO): E-mail
        - Hodnota (R): vopicka@seznam.cz
- FieldSet: Garant
    - Tituly před jménem: Ing.
    - Jméno (R): Jan
    - Příjmení (R): Novák
    - Tituly za jménem: Ph.D.
    - E-mail (R): novak@seznam.cz - E-mailová adresa pro přihlašování do systému.
    - FieldSet: Kontakty
        - FieldSet: Kontakt
            - Typ (RO): Telefon
            - Hodnota (R): 777 666 555
        - FieldSet: Kontakt
            - Typ (RO): E-mail
            - Hodnota (R): novak@seznam.cz
- Interní poznámka (multiLine)
- Datum registrace (RO): 20.2.2022
- Vyplnil, jméno a příjmení (RO): Jan Novák
- [warning: Schválit](#/publisher-registrations)
- [warning: Zamítnout](#/publisher-registrations)
