## UC: `PB21` Založit nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Založení nakladatele](#/publishers/create)

### Main success scenario:

1. Uživatel zadá IČO a [načte údaje nakladatele podle IČO](#PB60).
2. Uživatel zadá ostatní údaje nakladatele a uloží je.
3. Systém provede formální validace dat.
4. Systém [najde potenciální duplicity nakladatele](#PB70).
5. Systém zapíše data z formuláře do nových záznamů:
    - [nakladatele](#Publisher),
    - jeho [alternativních názvů](#Publisher.alternativeNames),
    - jeho [adresy sídla](#Publisher.mainAddress),
    - a jeho [kontaktů](#Publisher.contacts).
6. Systém na [nakladateli](#Publisher) nastaví:
    - [Původ](#Publisher.origin) = Agentura
    - [Stav](#Publisher.status) = Aktivní
    - [Čas založení](#Publisher.createTime) = aktuální datum a čas
    - [Čas potvrzení údajů](#Publisher.dataConfirmationTime) = aktuální datum a čas
7. Systém [odešle nakladatele](#PB90).

### Extensions:

- 1a. Uživatel zadá / smaže IČO:
    - 1a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
- 1b. Nakladatel nemá IČO:
    - 1b1. Uživatel zadá údaje nakladatele ručně.
- 2a. Uživatel přidá / smaže alternativní název:
    - 2a1. Systém přidá / smaže alternativní název.
- 2b. Uživatel zvolí Právní formu:
    - 2b1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
- 2c. Uživatel se rozhodne zadat adresu sídla ručně:
    - 2c1. Uživatel [vybere adresu sídla nakladatele](#PB65).
- 2d. Uživatel přidá / smaže kontakt:
    - 2d1. Systém přidá / smaže kontakt.
- 3a. Právnická osoba nemá IČO ani DIČ:
    - 3a1. Systém zobrazí chybu.
- 3b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 3b1. Systém zobrazí chybu.
- 3c. IČO nemá právě 8 číslic:
    - 3c1. Systém zobrazí chybu.
- 3d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 3d1. Systém zobrazí chybu.
- 3e. Hodnota kontaktu typu E-mail není formálně platnou e-mailovou adresou:
    - 3e1. Systém zobrazí chybu.
- 3f. Hodnota kontaktu typu Web není formálně platným URL:
    - 3f1. Systém zobrazí chybu.
- 3g. Hodnota kontaktu typu Datová schránka má méně než 6 nebo více než 12 znaků:
    - 3g1. Systém zobrazí chybu.
- 3h. Hodnota kontaktu typu Telefon má méně než 9 číslic:
    - 3h1. Systém zobrazí chybu.
- 3i. Existuje jiný záznam se shodnou kombinací polí Název a Kvalifikátor:
    - 3i1. Systém zobrazí chybu.
- 4a. Systém nalezl potenciální duplicity:
    - 4a1. Systém zobrazí potenciální duplicity.
    - 4a2. Systém zobrazí varování "Byly nalezeny potenciální duplicity."
    - 4a3. Případ užití pokračuje krokem 1.
- 5a. [Kontakt](#PublisherContact) [nakladatele](#Publisher.contacts) nebo [garanta](#PublisherGuarantor.contacts)
  je typu Telefon a obsahuje právě 9 číslic:
    - 5a1. Systém ponechá pouze číslice a doplní dvě mezery do formátu: `123 456 789`.
