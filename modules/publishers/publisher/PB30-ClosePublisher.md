## UC: `PB30` Ukončit nakladatele

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Ukončit nakladatele](#/publishers/close)

### Main success scenario:

1. Uživatel najde nakladatele [aktivního](#Publisher.status) nebo [v likvidaci](#Publisher.status) a ukončí ho.
2. Systém na [nakladateli](#Publisher) nastaví data z formuláře a:
    - [Stav](#Publisher.status) = Ukončený
    - [Čas ukončení](#Publisher.closeTime) = aktuální datum a čas
3. Systém [odešle nakladatele](#PB90).
