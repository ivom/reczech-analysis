## Entity: Adresa nakladatele `PublisherAddress`

### Attributes:

- Kód adresního místa `addressCode` (M text): 41405609 - Kód v RÚIAN
- Ulice `street` (M text): Nádražní 13/6b - Ulice, číslo popisné, číslo orientační.
- PSČ `postalCode` (M text): 123 45
- Obec `municipality` (M text)
