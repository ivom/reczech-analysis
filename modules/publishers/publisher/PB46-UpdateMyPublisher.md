## UC: `PB46` Změnit svého nakladatele

- Primární aktér: [Garant nakladatele](#PublisherGuarantorActor)
- Obrazovka: [Změnit svého nakladatele](#/my-publisher/edit)

### Main success scenario:

1. Systém najde [žádosti o změnu](#PublisherUpdateRequest) pro nakladatele přihlášeného uživatele
   ve [stavu](#PublisherUpdateRequest.status) Podaná.
2. Systém zobrazí údaje [nakladatele](#Publisher) přihlášeného uživatele.
3. Uživatel změní údaje nakladatele.
4. Uživatel odešle změny.
5. Systém provede formální validace dat.
6. Systém uloží změny neschvalovaných položek do:
    - [nakladatele](#Publisher),
    - jeho [doručovací adresy](#Publisher.mailingAddress), včetně jejího vytvoření nebo smazání,
    - jeho [kontaktů](#Publisher.contacts), včetně jejich vytvoření nebo smazání,
    - jeho [garantů](#Publisher.guarantors), včetně jejich vytvoření nebo smazání,
    - a jejich [kontaktů](#PublisherGuarantor.contacts), včetně jejich vytvoření nebo smazání.
7. Systém [odešle nakladatele](#PB90).

### Extensions:

- 1a. Existuje podaná žádost o změnu nakladatele přihlášeného uživatele:
    - 1a1. Systém zobrazí informaci "Žádost o změnu nakladatele čeká na vyřízení."
    - 1a2. Případ užití končí.
- 3a. Nakladatel má / nemá IČO:
    - 3a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
    - 3a2. Systém zakáže (s IČO) / povolí (bez IČO) vyplnění těchto údajů nakladatele ručně:
      Název, Právní forma, DIČ, Datum narození.
- 3b. Nakladatel nemá IČO:
    - 3b1. Uživatel zadá základní údaje nakladatele (Název, Právní forma, DIČ, Datum narození) ručně.
        - 3b1a. Uživatel zvolí Právní formu:
            - 3b1a1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
    - 3b2. Uživatel [vybere adresu](#PB67) sídla nakladatele.
- 3c. Nakladatel má IČO, ale nemá v ARESu uveden Kód adresního místa:
    - 3c1. Uživatel [vybere adresu](#PB67) sídla nakladatele.
- 3d. Systém načetl z ARESu platný Kód adresního místa:
    - 3d1. Systém zakáže ruční editaci adresy sídla (Ulice a číslo, PSČ, Obec).
- 3e. Uživatel zadal fyzickou osobu bez IČO:
    - 3e1. Systém zobrazí Souhlas se zveřejněním Adresy sídla.
- 3f. Uživatel zadal právnickou osobu nebo IČO:
    - 3f1. Systém skryje Souhlas se zveřejněním Adresy sídla.
- 3g. Uživatel zvolí, že doručovací adresa je odlišná od sídla:
    - 3g1. Systém otevře zadávání doručovací adresy.
    - 3g2. Uživatel [vybere doručovací adresu nakladatele](#PB66).
- 3h. Uživatel zvolí, že doručovací adresa je shodná s adresou sídla:
    - 3h1. Systém skryje zadávání doručovací adresy.
- 3i. Uživatel přidá / smaže kontakt pro veřejnost / kontakt garanta:
    - 3i1. Systém přidá / smaže kontakt pro veřejnost / kontakt garanta.
- 3j. Uživatel použije email garanta jako jeho kontaktní email a tento garant nemá žádný kontakt typu E-mail:
    - 3j1. Systém přidá garantovi kontakt typu E-mail s hodnotou emailu garanta.
- 3k. Uživatel přidá / smaže garanta:
    - 3k1. Systém přidá / smaže garanta.
- 3l. Došlo ke změně emailu garanta:
    - 3l1. Systém zobrazí varování:
      "Změnili jste email garanta. Pokud jde o jinou osobu, smažte prosím záznam garanta a vytvořte nový."
- 5a. Právnická osoba nemá IČO ani DIČ:
    - 5a1. Systém zobrazí chybu.
- 5b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 5b1. Systém zobrazí chybu.
- 5c. IČO nemá právě 8 číslic:
    - 5c1. Systém zobrazí chybu.
- 5d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 5d1. Systém zobrazí chybu.
- 5e. Hodnota kontaktu typu E-mail není formálně platnou e-mailovou adresou:
    - 5e1. Systém zobrazí chybu.
- 5f. Hodnota kontaktu typu Web není formálně platným URL:
    - 5f1. Systém zobrazí chybu.
- 5g. Hodnota kontaktu typu Datová schránka má méně než 6 nebo více než 12 znaků:
    - 5g1. Systém zobrazí chybu.
- 5h. Hodnota kontaktu typu Telefon má méně než 9 číslic:
    - 5h1. Systém zobrazí chybu.
- 5i. Žádný garant není uveden:
    - 5i1. Systém zobrazí chybu.
- 5j. Garant nemá žádný kontakt:
    - 5j1. Systém zobrazí chybu.
- 6a. [Kontakt](#PublisherContact) [nakladatele](#Publisher.contacts) nebo [garanta](#PublisherGuarantor.contacts)
  je typu Telefon a obsahuje právě 9 číslic:
    - 6a1. Systém ponechá pouze číslice a doplní dvě mezery do formátu: `123 456 789`.
- 6b. Alespoň jedna položka vyžadující schválení byla změněna:
    - 6b1. Systém navíc zobrazí upozornění "Změnili jste schvalované údaje, vyčkejte na schválení..."
    - 6b2. Systém vytvoří novou [žádost o změnu nakladatele](#PublisherUpdateRequest) a nastaví:
        - [Nakladatel](#PublisherUpdateRequest.publisher) = nakladatel přihlášeného uživatele
        - [Čas podání](#PublisherUpdateRequest.submitTime) = aktuální čas
        - [Uživatel](#PublisherUpdateRequest.user) = přihlášený uživatel
        - [Stav](#PublisherUpdateRequest.status) = Podaná
        - [Data](#PublisherUpdateRequest.data) = data z formuláře vyžadující schválení, viz níže
    - 6b3. Systém odešle e-mailovou zprávu na `isbn@nkp.cz` s informací o nové žádosti o změnu nakladatele ke schválení.

### Data ke schválení

Struktura dat vyžadující schválení:

- IČO `companyNumber` (nepovinné)
- Název `name`
- Právní forma `legalForm`
- DIČ `taxNumber` (nepovinné)
- Datum narození `birthDate` (nepovinné)
- Adresa sídla `mainAddress`
    - Kód adresního místa `addressCode`
    - Ulice `street`
    - PSČ `postalCode`
    - Obec `municipality`
