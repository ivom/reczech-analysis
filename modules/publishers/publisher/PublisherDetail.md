## Screen: Nakladatel `/publishers/detail`

### Form:

- Název (RO): Grada Publishing
- Doplněk k názvu (RO): Rybička
- Kvalifikátor (RO): Olomouc

### Table: Alternativní názvy

- Název
    - Grada
    - Grada
- Doplněk k názvu
    - Rybička
- Kvalifikátor
    - Brno
- Typ
    - Předchozí
- Neveřejný (checkbox)
    - true
    - false

### Form:

- Právní forma (RO): Právnická osoba
- IČO (RO): 12345678
- DIČ (RO): CZ12345678
- Datum narození (RO): 20. 2. 2000
- FieldSet: Adresa sídla
    - Ulice (RO): Nádražní 13/6b
    - PSČ (RO): 123 45
    - Obec (RO): Praha 2
- FieldSet: Doručovací adresa
    - Ulice (RO): Hlavní 123
    - PSČ (RO): 123 45
    - Obec (RO): Praha 4
- Sídelní obec (RO): Praha
- Sídelní kraj (RO): Hlavní město Praha

### Table: Kontakty pro veřejnost

- Typ
    - Telefon
    - E-mail
- Hodnota
    - 777 666 555
    - kontakt@grada.cz

### Table: Garanti

- Jméno
    - Ing. Mgr. Jan Novák, CSc.
    - Jana Nová
- Kontakty
    - Telefon: 777 666 555, E-mail: novak@grada.cz
    - Telefon: 777 555 444
- Uživatel
    - Jan Novák
    - Jana Nová
- Agentury
    - ISBN, ISMN
    - ISBN

### Form:

- Interní poznámka (RO): Pozor na...
- Stav (RO): Aktivní
- Čas registrace (RO): 20. 2. 2020
- Čas aktivace (RO): 22. 2. 2020
- Čas potvrzení údajů (RO): 20. 2. 2022 (před 2 roky)
- Čas ukončení (RO)
- [:Potvrdit platnost údajů]()
- [:Změnit](#/publishers/edit)
- [:V likvidaci]()
- [:Ukončit](#/publishers/close)
- [:Obnovit](#/publishers/edit)
- [:Zpět](#/publishers)
