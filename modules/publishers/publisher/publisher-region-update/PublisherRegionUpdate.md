## Entity: Změna kraje nakladatele `PublisherRegionUpdate`

### Attributes:

- Nakladatel `publisher` (n:1 `#Publisher`)
- Čas `submitTime` (M datetime)
- Původný kraj `originalRegion` (n:0..1 `#Region`)
- Cílový kraj `targetRegion` (n:0..1 `#Region`)
