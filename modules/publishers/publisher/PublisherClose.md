## Screen: Ukončit nakladatele `/publishers/close`

### Form:

- Název (RO): Grada Publishing
- Interní poznámka (multiLine): Stávající obsah.
- [primary: Uložit](#/publishers/detail)
- [:Zpět](#/publishers/detail)
