## Entity: Garant nakladatele `PublisherGuarantor`

### Attributes:

- Id (PK bigserial)
- Email (U text): vopicka@albatros.cz
- Tituly před jménem `titlesBefore` (O text)
- Jméno `firstName` (M text)
- Příjmení `lastName` (M text)
- Tituly za jménem `titlesAfter` (O text)
- Kontakty `contacts` (1:n `#PublisherContact`)
- Uživatel `user` (0..1:1 `#User`)
- Agentury `agencies` (1:n `#PublisherGuarantorAgency`)
