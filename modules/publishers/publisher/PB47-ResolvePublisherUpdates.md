## UC: `PB47` Vyřídit žádosti nakladatelů o změnu

- Primární aktér: [Pracovník agentury](#Agent)
- Obrazovka: [Žádosti o změnu nakladatelů](#/publisher-updates),
  [Žádost o změnu nakladatele](#/publisher-updates/detail)

### Main success scenario:

1. Systém zobrazí [žádosti o změnu nakladatele](#PublisherUpdateRequest)
   ve [stavu](#PublisherUpdateRequest.status) Podaná
   seřazené podle [času podání](#PublisherUpdateRequest.submitTime) od nejstarších.
2. Uživatel vybere žádost.
3. Systém zobrazí detail [žádosti o změnu nakladatele](#PublisherUpdateRequest).
4. Systém zvýrazní hodnoty, u kterých se liší nová hodnota od stávající.
5. Systém [najde potenciální duplicity nakladatele](#PB70) a zobrazí je.
6. Systém načte [alternativní názvy nakladatele](#Publisher.alternativeNames) a seřadí je podle času vytvoření sestupně.
7. Uživatel ověří a případně upraví údaje nakladatele.
8. Uživatel schválí žádost.
9. Systém provede formální validace dat.
10. Systém znovu [najde potenciální duplicity nakladatele](#PB70).
11. Systém zapíše data z formuláře do:
    - [nakladatele](#Publisher),
    - jeho [alternativních názvů](#Publisher.alternativeNames), včetně jejich vytvoření nebo smazání,
    - a jeho [adresy sídla](#Publisher.mainAddress).
12. Systém [odešle nakladatele](#PB90).
13. Systém nastaví na [žádosti o změnu nakladatele](#PublisherUpdateRequest):
    - [Stav](#PublisherUpdateRequest.status) = Vyřízená
    - [Výsledek](#PublisherUpdateRequest.result) = Schválená
14. Případ užití pokračuje krokem 1.

### Extensions:

- 3b. V [datech žádosti](#PublisherUpdateRequest.data) bylo zadáno IČO:
    - 3b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 7a. Uživatel zadá / smaže IČO:
    - 7a1. Systém povolí (s IČO) / zakáže (bez IČO) načítání údajů podle IČO.
- 7b. Uživatel načte údaje nakladatele podle IČO:
    - 7b1. Systém [načte údaje podle IČO](#PB61) a zobrazí je v položkách označených "registr".
- 7c. Nakladatel nemá v ARESu uveden Kód adresního místa
  nebo adresa sídla neodpovídá Kódu adresního místa z ARESu:
    - 7c1. Systém zobrazí upozornění "Pozor: Adresa sídla k danému IČO nepochází z ARESu." a odkaz na záznam nakladatele
      v ARESu podle IČO.
- 7d. Uživatel přidá / smaže alternativní název:
    - 7d1. Systém přidá / smaže alternativní název.
- 7e. Uživatel přidá stávající název jako předchozí:
    - 7e1. Systém vytvoří nový záznam [alternativního názvu nakladatele](#PublisherAlternativeName) s:
        - [Typ](#PublisherAlternativeName.type) = Předchozí,
        - [Název](#PublisherAlternativeName.name) = stávající název,
        - [Doplněk k názvu](#PublisherAlternativeName.nameSuffix) = stávající doplněk k názvu,
        - [Kvalifikátor](#PublisherAlternativeName.qualifier) = stávající kvalifikátor.
- 7f. Uživatel změní Právní formu:
    - 7f1. Systém zakáže (Právnická osoba) nebo povolí (Fyzická osoba) pole Datum narození.
- 7g. Uživatel se rozhodne změnit adresu sídla:
    - 7g1. Uživatel [vybere adresu](#PB67) sídla nakladatele.
- 8a. Uživatel zamítne žádost:
    - 8a1. Systém nastaví na [žádosti o změnu nakladatele](#PublisherUpdateRequest):
        - [Stav](#PublisherUpdateRequest.status) = Vyřízená
        - [Výsledek](#PublisherUpdateRequest.result) = Zamítnutá
    - 8a2. Případ užití pokračuje krokem 1.
- 9a. Právnická osoba nemá IČO ani DIČ:
    - 9a1. Systém zobrazí chybu.
- 9b. Fyzická osoba nemá IČO, DIČ ani Datum narození:
    - 9b1. Systém zobrazí chybu.
- 9c. IČO nemá právě 8 číslic:
    - 9c1. Systém zobrazí chybu.
- 9d. DIČ nemá 2 písmena následovaná 2 až 13 znaky:
    - 9d1. Systém zobrazí chybu.
- 9e. Existuje jiný záznam se shodnou kombinací polí Název a Kvalifikátor:
    - 9e1. Systém zobrazí chybu.
- 10a. Systém nalezl nové potenciální duplicity, které nebyly nalezeny při předchozím hledání:
    - 10a1. Systém zobrazí aktualizované potenciální duplicity.
    - 10a2. Systém zobrazí varování "Byly nalezeny nové potenciální duplicity."
    - 10a3. Případ užití pokračuje krokem 5.
- 11a. Došlo ke změně adresy sídla nakladatele:
    - 11a1. Systém najde [kraj](#Region) podle načteného názvu.
        - 11a1a. Kraj nebyl nalezen:
            - 11a1a1. Systém založí nový [kraj](#Region).
    - 11a2. Systém nastaví na nakladateli:
        - [Sídelní obec](#Publisher.residentialMunicipality) = Obec
        - [Sídelní kraj](#Publisher.residentialRegion) = nalezený [kraj](#Region)
    - 11a2a. Došlo ke změně sídelního kraje:
        - 11a2a1. Systém odešle upozornění, že došlo ke změně sídelního kraje, na email ? TODO.
        - 11a2a2. Systém vytvoří nový záznam [změny kraje nakladatele](#PublisherRegionUpdate).
          TODO: -> Modul povinné výtisky?
