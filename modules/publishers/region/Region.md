## Entity: Kraj `Region`

### Attributes:

- Název `name` (U text)

### Poznámky

- České kraje, tj. Středočeský, atd.
- Správa v aplikaci se nepředpokládá, tabulka bude naplněna při zprovoznění systému. Případné budoucí změny v krajích
  jsou řešeny automatickým doplňováním při úpravách nakladatelů nebo přímou změnou v databázi (přidání nových záznamů,
  příp. přejmenování záznamů).
