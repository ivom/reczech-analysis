## Screen: Žádost o změnu z ARESu `/ares-updates/detail`

### Table: Potenciální duplicity

> TODO: Zde doplnit tabulku potenciálních duplicit.

### Form:

- IČO (RO): 12345678
- Název (R): Grada Publishing
- Doplněk k názvu: Rybička
- Kvalifikátor: Olomouc
- Název, registr (RO): Nová Grada
- [:Přidat stávající název jako předchozí]()

### Table: Alternativní názvy

- Název (R)
    - Jan Novák
    - Grada
    - Bambook
- Doplněk k názvu (O)
    - Rybička
    - .
    - .
- Kvalifikátor (O)
    - Olomouc
    - .
    - .
- Typ (O select: Předchozí, Značka, Zkratka, Organizační složka, Cizojazyčná varianta, Jiné)
    - Předchozí
    - Zkratka
    - Značka
- Neveřejný (O checkbox)
    - true
    - false
    - false
- Smazat
    - [warning:Smazat]()
    - [warning:Smazat]()
    - [warning:Smazat]()

[:Přidat alternativní název]()

### Form:

- Právní forma (R radios: Právnická osoba, Fyzická osoba): Právnická osoba
- Právní forma, registr (RO radios: Právnická osoba, Fyzická osoba): Právnická osoba
- DIČ: CZ12345678 - Včetně kódu státu, např. CZ12345678.
- DIČ, registr (RO): CZ12345678
- Datum narození
- FieldSet: Adresa sídla
    - Ulice a číslo (R): Nádražní 13/6b - Ulice, číslo popisné i číslo orientační, např. Nádražní 13/6b.
    - Ulice a číslo, registr (RO): Nádražní 13/6b
    - PSČ (R): 12100 - Pět číslic bez mezery, např. 12100.
    - PSČ, registr (RO): 12100
    - Obec (R): Praha 2
    - Obec, registr (RO): Praha 2
    - Sídelní obec (RO): Praha
    - Sídelní obec, registr (RO): Praha
    - Sídelní kraj (RO): Hlavní město Praha
    - Sídelní kraj, registr (RO): Hlavní město Praha
- Interní poznámka (multiLine): Stávající obsah.
- [primary: Uložit](#/ares-updates)
- [:Ukončit](#/publishers/close)
- [:Najít IČO v ARESu]()
- [:Zrušit](#/ares-updates)
