## Entity: Žádost o změnu z ARESu `AresUpdateRequest`

### Attributes:

- Nakladatel `publisher` (n:1 `#Publisher`)
- Čas založení `createTime` (M datetime)
- Stav `status` (M enum: created, resolved)
- Data (n:1 `#AresData`)

### Stavový diagram

```plant
[*] --> Created: Monitor ARES updates (Hlídat změny údajů v ARES)
Created: Založená
Created --> Resolved: Resolve ARES updates (Vyřídit žádost o změnu z ARES)
Resolved: Vyřízená
```
