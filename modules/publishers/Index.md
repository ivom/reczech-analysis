# ReČeK FSD, modul Nakladatelé

FSD systému ReČeK, včetně přehledu všech modulů, [zde](reczech-fsd.html).

## Obrazovky

- Pracovník agentury
    - [Nakladatelé](#/publishers)
    - [Žádosti o registraci](#/publisher-registrations)
    - [Žádosti o změnu](#/publisher-updates)
    - [Žádosti o změnu z ARESu](#/ares-updates)
- Nakladatel
    - [Registrace nakladatele](#/my-publisher/register)
    - [Můj nakladatel](#/my-publisher)

## Aktéři

- [Pracovník agentury](#Agent)
- [Garant nakladatele](#PublisherGuarantorActor)
- [Systém](#System)

## Datový model

- [Kraj](#Region)
- [Nakladatel](#Publisher)
    - [Alternativní název](#PublisherAlternativeName)
    - [Adresa nakladatele](#PublisherAddress)
    - [Garant nakladatele](#PublisherGuarantor)
    - [Kontakt nakladatele](#PublisherContact)
    - Požadavky na změnu:
        - [Žádost o změnu nakladatele](#PublisherUpdateRequest)
        - [Žádost o změnu z ARESu](#AresUpdateRequest)
- [Data z ARESu](#AresData)
- [Uživatel](#User)

## Klíčové požadavky

Specifikace modulu Nakladatelé vychází z těchto klíčových požadavků:

1. Vést evidenci nakladatelů v ČR.

## Generické funkčnosti

Modul musí implementovat následující generické funkčnosti, které jsou popsány
v [modulu Uživatelé](reczech-users-fsd.html):

- Jednotné přihlášení přes všechny moduly (SSO)
- Vedení auditního logu všech změn v systému a jeho prohlížení

## TODO

- Anonymizace
- Hlídání změn v RÚIAN, datových schránkách
- Nakladatel: Alternativní názvy, hledání
- Přesunout do modulu Povinné výtisky:
    - Oslovit regionální knihovny, jak chtějí dostávat údaje o změnách nakladatelů v jejich kraji
    - Přidat obrazovky pro regionální knihovny (bez přihlášení):
        - Aktuální seznam všech nakladatelů podle kraje
        - Změny (přibyl, skončil) v nakladatelích podle kraje, řazené sestupně podle data, možnost skrýt zaznamy starší
          než
    - Při změně Sídelního kraje nakladatele upozornit regionální knihovnu na změnu.
    - Zobrazit historii sídelních krajů nakladatele
- Doplnit na seznam nakladatelů prefix nakladatele a hledání podle něj
- Reporty:
    - Mezinárodní agentura
    - Výroční zpráva
    - Nakladatelé v likvidaci
    - Nově registrovaní nakladatelé dle kraje
    - SČKN ?
    - Nipos ?
- Všechno (kontakty, adresa sídla) na nakladateli, který nežádal, nejsou veřejné
- Nakladatel: sloučení, převzetí (= ukončení?)
