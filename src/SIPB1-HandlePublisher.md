## UC: `SIPB1` Zpracovat nakladatele

- Úroveň: Podfunkce
- Předpoklady
    - Zpráva s údaji nakladatele:
        - Id `id` (povinné)
        - Název `name` (povinné)
        - Doplněk k názvu `nameSuffix` (nepovinné)
        - Kvalifikátor `qualifier` (nepovinné)
        - Stav `status` (povinný výčet: request, rejected, active, closing, closed)
        - Registrační agentura `registrationAgency` (nepovinný výčet: ISBN, ISMN)
        - Registrační záměr `registrationIntent` (nepovinný výčet: oneOff, continuous)
        - Čas potvrzení údajů `dataConfirmationTime` (povinný datetime)
        - Garanti `guarantors`
            - Id `id` (povinné)
            - Email `email` (povinné)
            - Jméno `firstName` (povinné)
            - Příjmení `lastName` (povinné)

### Main success scenario:

1. Systém [přijme zprávu s údaji nakladatele](reczech-publishers-fsd.html#PB90) z modulu Nakladatelé.
2. Systém [odešle zprávu do modulu Agentura](reczech-agency-fsd.html#APB20) a mapuje položky takto:
    - Zdrojový identifikátor: Id
3. Systém [odešle zprávu do modulu Uživatelé](reczech-users-fsd.html#OR50) a mapuje / vyplní položky takto:
    - Zdroj: `publishers`
    - Zdrojový identifikátor organizace: Id
    - Jméno: Název
    - Archivováno: Stav není `active` ani `closing`
    - Uživatelé: Garanti
        - Archivováno: `false`
        - Role: `PublisherAdmin`, `PublisherGuarantor`

### Extensions:

- 2a. Stav nakladatele je `request` nebo `rejected`:
    - 2a1. Systém zprávu do modulu Agentura neposílá.
- 3a. Stav nakladatele je `request` nebo `rejected`:
    - 3a1. Systém zprávu do modulu Uživatelé neposílá.
