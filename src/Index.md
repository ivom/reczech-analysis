# ReČeK FSD

## Moduly

- [Uživatelé](reczech-users-fsd.html)
- [Nakladatelé](reczech-publishers-fsd.html)
- [Agentura](reczech-agency-fsd.html)

## Technické požadavky

Z funkční analýzy vyplynuly tyto technické požadavky na implementaci, týkající se obecně všech modulů:

- Systém musí odesílat emaily přes službu k tomu určenou (např. SendGrid), aby se předešlo tomu, že zprávy budou
  vyhodnoceny jako spam.
- Systém ignoruje mezery na začátku a na konci v údajích zadávaných uživatelem na formulářích (trim).
- Při hledání podle textových položek systém automaticky hledá hodnoty,
  které začínají zadaným textem (tj. ekvivalent SQL like 'xxx%').
- Všechna data se ukládají a na všech rozhraních přenášejí v kódování UTF-8.
- Backend běží v UTC, všechny časové údaje jsou v DB ukládány v UTC, a to buď explicitně (včetně zóny) nebo implicitně.
- Systém implementuje optimistické zamykání, tzn. při souběžné editaci stejných dat "první vyhrává". Cílem je zabránit
  případům, kdy si klienti (více uživatelů nebo např. více záložek prohlížeče jednoho uživatele) navzájem přepisují
  data.

## Systémová integrace

Integraci modulů popisují případy užití:

- [Zpracovat nakladatele](#SIPB1)
- [Přihlásit se do externího systému](reczech-users-fsd.html#SE25)

## TODO

### Úkoly

- Ve všech formulářích mazat vícenásobné mezery uprostřed. (??)
